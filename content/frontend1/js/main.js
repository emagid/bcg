$(document).ready(function() {
    var $window = $(this);
    $('#promotional_top_ticker').slick();
    $(".our_picks_slider").slick();
    
    var scrollTop = $window.scrollTop();
    position_topbar(scrollTop);
    $window.scroll(function(e){
        var scrollTop = $(this).scrollTop();
        position_topbar(scrollTop);
    });

    function position_topbar(scrollTop){
        var $global_header = $(".global_header");
        var $below_header = $("#primary_page_controller_wrapper");
        if(scrollTop <= 33){
            $global_header.css({"position":"relative"});
            $below_header.css({"margin-top":"0"});
        } else{
            $global_header.css({"position":"fixed","top":"0px"});
            $below_header.css({"margin-top":"124px"});
        }
    }

    $("a.change_our_picks_category").click(function(event){
    	event.stopPropagation();
    	$("div.our_picks_categories").show();
    });
    var $our_picks_categories_box = $("div.our_picks_categories");
    if($our_picks_categories_box.length > 0){
    	$("body").click(function(){
    		$our_picks_categories_box.hide();
    	});
    	$our_picks_categories_box.click(function(event){
    		event.stopPropagation();
    	});
    }

    $(".show_mobile_menu").click(function(event){
    	$(this).hide();
    	$(".modal_overlay").addClass("modal_overlay_visible");
    	$(".hide_mobile_menu").show();
    	$(".mobile_menu_wrapper").addClass("mobile_menu_show");
    });

    $(".modal_overlay_visible.modal_overlay").click(function(){
    	$(this).removeClass("modal_overlay_visible");
    });


    $(".hide_mobile_menu").click(function(event){
    	$(this).hide();
    	$(".show_mobile_menu").show();
    	$(".modal_overlay").removeClass("modal_overlay_visible");
    	$(".mobile_menu_wrapper").removeClass("mobile_menu_show");
    });
    $(".collections_tab").click(function(event){
    	$(".collections_tab").removeClass("active_collections_tab");
    	$(this).addClass("active_collections_tab");
    });


    $(".category_view_product_cell").mouseover(function(event){
    	var $rollover_card_object = $(this);
    	var product_cell_left = $(this).offset().left;
    	var product_cell_top = $(this).offset().top;
    	var product_cell_width = $(this).width();
    	var product_cell_height = $(this).height();
    	var product_rollover_image = $(this).attr("data-rollover_product_image");
    	var product_rollover_name = $(this).attr("data-rollover_product_name");
    	var product_rollover_link = $(this).attr("data-rollover_product_link");
        var product_rollover_price = $(this).attr("data-rollover_product_price");
    	var products_container_left = $(".category_page_content_wrapper").offset().left;

    	var product_distance_from_container_left = product_cell_left -products_container_left;
    	if(product_distance_from_container_left >= 480){
    		var product_rollover_direction = "left";
    	} else{
    		var product_rollover_direction = "right";
    	}

    	show_product_rollover_card($rollover_card_object,product_cell_height,product_cell_width,product_cell_left,product_cell_top,product_rollover_direction,product_rollover_link,product_rollover_price,product_rollover_name,product_rollover_image);
    });

	// $(".product_rollover_card").mouseover(function(event){
 //        event.stopImmediatePropagation();
	// 	$(this).show().delay(150).queue(function(finish_queue2){
 //            $(this).addClass("active_rollover_card");
 //            finish_queue2();
 //        });
	// });

    // $(window).mouseover(function(e){
    //     var $rollover_card = $(".product_rollover_card");
    //     var $product_cell = $(".category_view_product_cell");

    //     if($rollover_card.is(":visible")){
    //         if((!$rollover_card.is(e.target))||(!$product_cell.is(e.target))){
    //             $rollover_card.removeClass("active_rollover_card").delay(350).queue(function(finish_queue2){
    //                 $rollover_card.hide();
    //                 finish_queue2();
    //             });
    //         }
    //     }
    // });


    function show_product_rollover_card($rollover_card_object, product_cell_height,product_cell_width,product_cell_left,product_cell_top,product_rollover_direction,product_rollover_link,product_rollover_price,product_rollover_name,product_rollover_image){
		var $rollover_card = $(".product_rollover_card");
		var product_cell_left_positioner = product_cell_left - 8;
        $rollover_card.find(".product_rollover_image").css({"background-image":"url('"+product_rollover_image+"')"});
        $rollover_card.find(".product_rollover_title").text(product_rollover_name);
        $rollover_card.find(".product_rollover_price").text("$" + product_rollover_price);
        $rollover_card.find(".product_rollover_link").each(function(){
            $(this).attr("href",product_rollover_link);
        });
		var height_adjustor = product_cell_height + 36;
		
		var product_cell_top_positioner = product_cell_top - 18;
        var rollover_card_width = product_cell_width + 240;
        var product_direction_left_left_positioner = product_cell_left + product_cell_width - rollover_card_width + 8;
		console.log("triggered");
		if(product_rollover_direction == "right"){
            $rollover_card.find(".product_rollover_image").css({"float":"left","left":"4px","right":"auto","width":product_cell_width});
            $rollover_card.find(".product_rollover_buy_section").css({"float":"right"});
			$rollover_card.css({"left":product_cell_left_positioner,"top":product_cell_top_positioner,"height":height_adjustor,"width":rollover_card_width});
		} else{
            $rollover_card.find(".product_rollover_image").css({"float":"right","right":"4px","left":"auto","width":product_cell_width});
            $rollover_card.find(".product_rollover_buy_section").css({"float":"left"});
			$rollover_card.css({"left":product_direction_left_left_positioner,"top":product_cell_top_positioner,"height":height_adjustor,"width":rollover_card_width});			
		}
		$rollover_card.addClass("active_rollover_card");


        $rollover_card.one("mouseleave",function(){
            hide_product_rollover_card();
        });

	}

    function hide_product_rollover_card(){
    	var $rollover_card = $(".product_rollover_card");
        $rollover_card.removeClass("active_rollover_card");
        $rollover_card.css({"width":"0","height":"0"});
        $rollover_card.find(".product_rollover_title").empty();
        $rollover_card.find(".product_rollover_price").empty();
        $rollover_card.find(".product_rollover_link").each(function(){
            $(this).attr("href","");
        });
        $rollover_card.find(".product_rollover_image").css({"background-image":""});
    }

    $("#show_shopping_bag_dropdown").hover(function(event){
        event.stopPropagation();
        var top = $(this).offset().top;
        var left = $(this).offset().left;
        var width = $(this).width();
        var width_half = width/2;
        var type = "mouseover";
        left = left + width_half;
        show_header_dark_overlay();
        show_bag_menu(top,left,type);
    });

    $("body").delegate( ".shopping_bag_menu_wrapper.mouse_event", "mouseleave", function( event ) {
      event.preventDefault();
      hide_bag_menu();
      hide_header_dark_overlay();
    });

    $(".hide_shopping_bag_inner_btn").click(function(){
        hide_bag_menu();
        hide_header_dark_overlay();
    });

    $("#full_page_header_dark_overlay").click(function(){
        hide_bag_menu();
        hide_header_dark_overlay();
    });

    $(".purchase_btn").click(function(){
        var $shopping_bag_placer = $("#show_shopping_bag_dropdown");
        event.stopPropagation();
        var top = $shopping_bag_placer.offset().top;
        var left = $shopping_bag_placer.offset().left;
        var width = $shopping_bag_placer.width();
        var width_half = width/2;
        var type = "click";
        left = left + width_half;
        show_header_dark_overlay();
        show_bag_menu(top,left,type);        
    });

    function show_header_dark_overlay(){
        $("#full_page_header_dark_overlay").show().delay(5).queue(function(fadeInOverlay){
            $("#full_page_header_dark_overlay").addClass("opacity_one");
            fadeInOverlay();
        });
    }

    function hide_header_dark_overlay(){
        $("#full_page_header_dark_overlay").removeClass("opacity_one").delay(270).queue(function(fadeOutOverlay){
            $("#full_page_header_dark_overlay").hide();
            fadeOutOverlay();
        });
    }

    function show_bag_menu(top,left,type){
        var $bag_wrapper = $(".shopping_bag_menu_wrapper");
        if(type === "mouseover"){
            $bag_wrapper.addClass("mouse_event");
        } else if(type === "click"){
            $bag_wrapper.addClass("mouse_click");
        }
        var new_top = top - 22;
        var new_left = left - 200;
        $bag_wrapper.css({"top":new_top,"left":new_left});
        $bag_wrapper.show().delay(5).queue(function(fadeInBag){
           $bag_wrapper.addClass("opacity_one");
            fadeInBag();
        });
        $("#show_shopping_bag_dropdown").addClass("bag_link_active");
    }

    function hide_bag_menu(){
        var $bag_wrapper = $(".shopping_bag_menu_wrapper");
        $bag_wrapper.removeClass("mouse_event mouse_click");
        $bag_wrapper.removeClass("opacity_one");
        $bag_wrapper.hide();
        $("#show_shopping_bag_dropdown").removeClass("bag_link_active");
    }

});