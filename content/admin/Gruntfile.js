module.exports = function(grunt) {
  require('time-grunt')(grunt);
  require('jit-grunt')(grunt);
  require('load-grunt-tasks')(grunt);
  var mozjpeg = require('imagemin-mozjpeg');
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    concat: {
      dist: {
        src: [
        'js/bootstrap/transition.js',
        'js/bootstrap/alert.js',
        'js/bootstrap/button.js',
        'js/bootstrap/carousel.js',
        'js/bootstrap/collapse.js',
        'js/bootstrap/dropdown.js',
        'js/bootstrap/modal.js',
        'js/bootstrap/tooltip.js',
        'js/bootstrap/popover.js',
        'js/bootstrap/scrollspy.js',
        'js/bootstrap/tab.js',
        'js/bootstrap/affix.js',
        'js/plugins/jquery.simplePagination.js',
        'js/plugins/dropzone.js',
        'js/plugins/jquery-sortable.js',
         'js/plugins/numericInput.min.js',
		//'js/plugins/jquery.paginate.js',
        'js/plugins/jquery-validation/dist/jquery.validate.js',
        'js/plugins/slimScroll/jquery.slimscroll.js',
        'js/plugins/datatables/jquery.dataTables.js',
        'js/plugins/datatables/dataTables.bootstrap.js',
        'js/plugins/datepicker/bootstrap-datepicker.js',
        'js/plugins/input-mask/jquery.inputmask.js',
        'js/plugins/input-mask/jquery.inputmask.date.extensions.js',
        'js/plugins/input-mask/jquery.inputmask.extensions.js',
        'js/plugins/daterangepicker/daterangepicker.js',
        'js/plugins/timepicker/bootstrap-timepicker.js',
        'js/plugins/bootstrap-multiselect.js',
        'js/plugins/autoNumeric.js',
        'js/plugins/mult_image.js',
        
        'js/plugins/app-custom.js',

        ],
        dest: 'js/main.js',
      }
    },
    uglify: {
      dist: {
        files: {
          'js/main.min.js': ['js/main.js']
        }
      }
    },
    less: {
      compileCore: {
        options: {
          strictMath: true,
          sourceMap: true,
          outputSourceFiles: true,
          sourceMapURL: 'main.css.map',
          sourceMapFilename: 'css/main.css.map'
        },
        src: 'less/AdminLTE.less',
        dest: 'css/main.css'
      }
    },
    cssmin: {
      options: {
        compatibility: 'ie8',
        keepSpecialComments: '*',
        noAdvanced: true
      },
      minifyCore: {
        src: 'css/main.css',
        dest: 'css/main.min.css'
      }
    },
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        eqnull: true,
        browser: true,
        globals: {
          jQuery: true
        },
        reporter: require('jshint-stylish')
      },
      target: [
      'js/main.js'
      ]
    },
  imagemin: {                          // Task
    static: {                          // Target
      options: {                       // Target options
        optimizationLevel: 3,
        svgoPlugins: [{ removeViewBox: false }],
        use: [mozjpeg()]
      }
    },
    dynamic: {                         // Another target
      files: [{
        expand: true,                  // Enable dynamic expansion
        cwd: 'opt-images/',                   // Src matches are relative to this path
        src: ['**/*.{png,jpg,gif,svg}'],   // Actual patterns to match
        dest: 'images/'                  // Destination path prefix
      }]
    }
  }
  /*svgmin: {
    options: {
      plugins: [
      {
        removeViewBox: false
      }, {
        removeUselessStrokeAndFill: false
      }
      ]
    },
    dist: {
      expand: true,
      cwd: 'icons/',
      src: ['*.svg'],
      dest: 'icons/optimized/'
    }
  },
  grunticon: {
    myIcons: {
      files: [{
        expand: true,
        cwd: 'icons/optimized/',
        src: ['*.svg', '*.png'],
        dest: "icons/dist/"
      }],
      options: {
        loadersnippet: "grunticon.loader.js",
        defaultWidth: "32px",
        defaultHeight: "32px",
        customselectors: {
          "*": [".icon-$1:before"]
        },
        cssprefix: ".icon-",
        colors: {
          white: "#FFF"
        },
        dynamicColorOnly: true
      }
    }
  }*/
});

grunt.registerTask('s', ['concat', 'less', 'cssmin']);

grunt.registerTask('c', ['concat', 'uglify', 'less', 'cssmin', 'imagemin', /*'svgmin', 'grunticon:myIcons'*/]);
};