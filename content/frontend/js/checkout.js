$(document).ready(function() {
	var $credit_card_icons = $(".credit_card_options > g");
	var $credit_card_icon_visa = $(".credit_card_options > #visa");
	var $credit_card_icon_amex = $(".credit_card_options > #amex");
	var $credit_card_icon_mc = $(".credit_card_options > #mc");
	var $credit_card_icon_disc = $(".credit_card_options > #disc");

	$('#credit_card_number').keyup(function() {
	    var cc_input = this.value;
	    var cc_input_first = cc_input.substr(0, 1);
	    var cc_input_first_two = cc_input.substr(0, 2);
	    if((cc_input_first === "4") && (cc_input.length <= 16)){
	    	$credit_card_icons.attr("class","");
	    	$credit_card_icon_visa.attr("class","active_cc_icon");
	    } else if((cc_input_first === "3") && (cc_input.length <= 15)){
	    	if((cc_input_first_two === "34") || (cc_input_first_two === "37")){
	    		$credit_card_icons.attr("class","");
	    		$credit_card_icon_amex.attr("class","active_cc_icon");
	    	}
	    	else{
	    		$credit_card_icons.attr("class","");
	    	}
	    } else if((cc_input_first === "6") && (cc_input.length <= 16)){
	    	if((cc_input_first_two === "60") || (cc_input_first_two === "65") || (cc_input_first_two === "62")){
	    		$credit_card_icons.attr("class","");
	    		$credit_card_icon_disc.attr("class","active_cc_icon");
	    	}
	    	else{
	    		$credit_card_icons.attr("class","");
	    	}
	    } else if((cc_input_first === "5") && (cc_input.length <= 16)){
	    	if((cc_input_first_two === "51") || (cc_input_first_two === "52") || (cc_input_first_two === "53") || (cc_input_first_two === "54") || (cc_input_first_two === "55")){
	    		$credit_card_icons.attr("class","");
	    		$credit_card_icon_mc.attr("class","active_cc_icon");
	    	}
	    	else{
	    		$credit_card_icons.attr("class","");
	    	}
	    } else{
	    	$credit_card_icons.attr("class","");
	    }

	});
});