<?php

namespace Model;

class Admin_Roles extends \Emagid\Core\Model {
  
    static $tablename = "admin_roles";
    public static $fields = ['admin_id','role_id'];

}
