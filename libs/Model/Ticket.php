<?php

namespace Model;

class Ticket extends \Emagid\Core\Model {
    static $tablename = "public.tickets";

    public static $fields  =  [
        'ticket_type_id',
        'name',
        'price',
        'quantity',
        'sales_start_date',
        'total_capacity',
    ];
}