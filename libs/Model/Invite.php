<?php

namespace Model;

class Invite extends \Emagid\Core\Model {
    static $tablename = "public.invites";

    public static $fields  =  [
        'name',
        'email',
        'phone',
        'status',
        'event_id',
    ];

    static $status = ['Invited','Accepted'];
}