<?php
namespace Model;

class Day extends \Emagid\Core\Model {
	static $tablename = "days";
	public static $fields = [
  		'date'
  	];
}