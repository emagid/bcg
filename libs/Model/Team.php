<?php
namespace Model;

class Team extends \Emagid\Core\Model {
	static $tablename = "team";
	public static $fields = [
		'name',
		'position',
		'image',
		'description',
	];
}