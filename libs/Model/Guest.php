<?php

namespace Model;

class Guest extends \Emagid\Core\Model {
    static $tablename = "public.guests";

    public static $fields  =  [
        'first_name',
        'last_name',
        'email' => ['required' => true, 'type' => 'email'],
        'phone',
        'address',
        'city',
        'state',
        'country',
        'zip',
        'event_id',
        'no_of_guests',
        'confirmation_no',
        'status',
        'qrcode',
        'company',
        'volunteer',
        'check_in_time',
        'type',
    ];

    static $status = ['Registered','Checked-In'];

    static $type = ['Guest','Pre-Registered Guest'];

    public static function generateToken()
    {
        $long_token = md5(uniqid(rand(), true));
        $short_token = substr($long_token,0,7);
        return $short_token;
    }

    function full_name() {
        return $this->first_name.' '.$this->last_name;
    }

    function get_qr_image() {
        return UPLOAD_URL . 'qrcodes/' . $this->image;
    }

    public static function search($keywords, $limit = 20)
    {
        $sql = "SELECT *  FROM guests WHERE active = 1 AND (";
        if (is_numeric($keywords)) {
            $sql .= "id = " . $keywords . " OR ";
        }
        foreach (explode(' ',urldecode($keywords)) as $keyword){
            if($keyword == '') continue;
            $sql .= " LOWER(first_name) LIKE LOWER('$keyword%')   OR ";
            $sql .= " LOWER(last_name)  LIKE LOWER('$keyword%')   OR ";
            $sql .= " LOWER(email)      LIKE LOWER('%$keyword%')   OR ";
        }
        $sql .= " LOWER(first_name) || ' ' || LOWER(last_name) like '" . strtolower(urldecode($keywords)) . "%') limit " . $limit;
        return self::getList(['sql' => $sql]);
    }
}