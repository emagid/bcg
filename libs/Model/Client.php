<?php

namespace Model;

class Client extends \Emagid\Core\Model {
    static $tablename = "public.clients";

    public static $fields  =  [
        'name',
        'email',
        'phone',
        'logo',
    ];
}