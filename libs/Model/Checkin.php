<?php

namespace Model;

class Contact extends \Emagid\Core\Model {
    static $tablename = "public.checkin";

    public static $fields  =  [
        'pin' => ['type' => 'numeric'],
        'first_name',
        'subject_id',
        'last_name',
        'email',
        'phone',
    ];

    public static $forms = [
        1=>'Share Picture',
        2=>'Share Gif',
        3=>'Donate'
    ];

    public function get_signatures(){
        return Signature::getList(['where'=>"checkin_id = '$this->id'"]);
    }

    public function lastSignin(){
        $signin = $this->get_signatures();
        if(!isset($signin[0])) return '';
        $signin = $signin[0];
        $insert_time = new \DateTime($signin->insert_time);
        $insert_time = $insert_time->format('m-d-Y H:s');
        return $insert_time;
    }

}
























