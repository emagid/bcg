<?php

namespace Model;

class Ticket_Type extends \Emagid\Core\Model {
    static $tablename = "public.ticket_type";

    public static $fields  =  [
        'type',
    ];
}