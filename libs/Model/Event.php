<?php

namespace Model;

class Event extends \Emagid\Core\Model {
    static $tablename = "public.events";

    public static $fields  =  [
        'title',
        'description',
        'venue',
        'address',
        'city',
        'state',
        'country',
        'zip',
        'start_date',
        'end_date',
        'ticket_id',
        'client_id',
        'url',
        'background_image',
        'logo',
        'registration_status',
        'theme_color',
        'featured_image1',
        'featured_image2',
        'featured_image3',
        'featured_image4',
        'insta_url',
    ];

    static $reg_status = ['Not Started','Open','Closed'];
}