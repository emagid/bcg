<?php

namespace Model;

class Signature extends \Emagid\Core\Model {
    static $tablename = "signature";

    public static $fields  =  [
        'contact_id',
        'signature',
        'insert_time',
        'type'
    ];

    static $type = [1=>'checkin',2=>'registration'];

    public function toDate(){
        $insert_time = new \DateTime($this->insert_time);
        $insert_time = $insert_time->format('m-d-Y H:s');
        return $insert_time;
    }
}
























