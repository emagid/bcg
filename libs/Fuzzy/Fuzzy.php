<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 3/24/16
 * Time: 11:25 AM
 */

namespace Fuzzy;
class Fuzzy {
    /**
     * Perform a fuzzy string searching.
     * Searching rows should be
     * [
     *     [ 'fields' => ['abc'. 'ddd'], 'object' => \Model\Product],
     *     [ 'fileds' => ['ddd', 'xxx'], 'object' => \Model\WeddingBand]
     * ]
     * @param array $rows
     * @param string $query
     * @param integer $threshold
     * @param integer $limit
     * @return array
     */
    public function search(array $rows, $query, $threshold = 3, $limit = 10)
    {
        $matched = [];
        $query = strtolower($query);
        foreach($rows as $row)
        {
            foreach($row['fields'] as $field){
                $field = strtolower($field);
                $distance = $this->calculateDistance($query, $field);
                if ($threshold >= $distance && count($matched) < $limit)
                {
                    $matched[] = $row['object'];
                    break; // break for current foreach, loop to next row
                }
            }
        }

        $returnData = [];

        // $match will be product, ring, weddingband and jewelry model
        foreach($matched as $match){
            $returnData[] = $match->generateSearchResult();
        }

        return $returnData;
    }

    /**
     * Calculate the Levenshtein distance between two strings.
     *
     * @param string $one
     * @param string $two
     * @return int
     */
    protected function calculateDistance($one, $two)
    {
        return levenshtein($one, $two);
    }
}