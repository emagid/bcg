# EmagidPHP #

A light weight PHP framework. 

This framework will allow you to create controllers, models and views with ease through its variety of functions, it has an ORM layer, and utilities to help you write better code faster.

### Installation ###
The framework should be installed as a submodule, we recommend under '**lib/Emagid**' directory. 
To install as a submodule use bash (or gitbash on windows):
```
$ git submodule add git@bitbucket.org:emagid/emagidphp.git lib/Emagid
```

If the site is new, you can copy the content of '**lib/Emagid/_includes/start_here**' to the root folder of the site. 

basic structure of project folders using the framework to get started:
```
->root folder
	->controller
	->lib
		->Emagid
		->Model
	->templates
	->views
	->.htaccess
	->index.php
	->config.php
```

initializing the framework:
in your index.php file on the project folder make sure to include the file emagid.php.
	
to initialize the framework,first make sure you are redirecting correctly in your .htaccess file. have your rewrite rule to be
```
RewriteRule ^(.+)$ index.php/$1 [L]
```

the above will make sure to redirect to your index.php in your project directory.

###Initiating the framework###
To start the module, you need to create the object inside the index.php file, and load the framework:
```php
		$emagid = new \Emagid\Emagid();
```

You can use parameters to configure the ORM, MVC, template, etc... 
```php
$emagid = new \Emagid\Emagid([
	'debug'=>true,      		// When debug is enabled, Kint debugging plugin is enabled. 
								//  you can use d($my_var) , dd($my_var) , Kint::Trace() , etc... 
								//  documentation available here : http://raveren.github.io/kint/
	'root'=>'/', 
	'template'=>'default',  	// template must be found in /temlpates/<template_name>/<template_name>.php 
								// so in this example we will have /templates/default/default.php 
								// please open the template file to see how the view is being rendered .

	'connection_string'=> array(
				'driver'   => 'pgsql', 
                'db_name'  => 'db_name',
                'username' => 'user',
                'password' => 'pwd',
                'host'     => 'localhost'
        )
]);
```

###Loading MVC###
Once the emagid framework has been instantiated, you may load the MVC framework  :
```php
$emagid->loadMvc();
```
The Emagid MVC framework supports configuring custom and complex routes, please read documentation under MVC folder. 


###Local debugging, scaffolding and advanced features###
The framework include few elevated debugging features while working locally.
To activate these features, you should add the following to your *php.ini* file : 
```
[eMagid]
em_is_local = 1
```
