<?php 

define('DB_DRIVER', 'pgsql');
define('DB_HOST', 'localhost');
define('DB_NAME', '<my db>');
define('DB_USER', 'postgres');
define('DB_PWD', '123456');


$emagid_conf = array (
		'debug'=>true,          // When debug is enabled, Kint debugging plugin is enabled. 
								//  you can use d($my_var) , dd($my_var) , Kint::Trace() , etc... 
								//  documentation available here : http://raveren.github.io/kint/
		'template'=>'default',  // template must be found in /temlpates/<template_name>/<template_name>.php 
								// so in this example we will have /templates/default/default.php 
								// please open the template file to see how the view is being rendered .

		'connection_string'=> array(
				'driver'   => DB_DRIVER, 
                'db_name'  => DB_NAME,
                'username' => DB_USER,
                'password' => DB_PWD,
                'host'     => DB_HOST
        )
); 