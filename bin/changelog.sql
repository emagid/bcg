-- Events DB

-- May 23, 2018
CREATE TABLE public.events
(
    id SERIAL primary key,
    active SMALLINT NOT NULL DEFAULT 1,
    insert_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    name CHARACTER VARYING COLLATE pg_catalog."default",
    description CHARACTER VARYING COLLATE pg_catalog."default",
    venue CHARACTER VARYING COLLATE pg_catalog."default",
    city CHARACTER VARYING COLLATE pg_catalog."default",
    state CHARACTER VARYING COLLATE pg_catalog."default",
    country CHARACTER VARYING COLLATE pg_catalog."default",
    zip CHARACTER VARYING COLLATE pg_catalog."default",
    type CHARACTER VARYING COLLATE pg_catalog."default",
    start_date CHARACTER VARYING,
    end_date CHARACTER VARYING,
    client_id INTEGER,
    ticket_id INTEGER
)

CREATE TABLE public.clients
(
    id SERIAL primary key,
    active SMALLINT NOT NULL DEFAULT 1,
    insert_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    name CHARACTER VARYING COLLATE pg_catalog."default",
    email CHARACTER VARYING COLLATE pg_catalog."default",
    phone CHARACTER VARYING COLLATE pg_catalog."default"
)

-- May 25, 2018
CREATE TABLE public.tickets
(
    id SERIAL primary key,
    active SMALLINT NOT NULL DEFAULT 1,
    insert_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    name CHARACTER VARYING COLLATE pg_catalog."default",
    price CHARACTER VARYING COLLATE pg_catalog."default",
    quantity CHARACTER VARYING COLLATE pg_catalog."default",
    sales_start_date CHARACTER VARYING,
    total_capacity CHARACTER VARYING,
    ticket_type_id INTEGER
)

CREATE TABLE public.ticket_type
(
    id SERIAL primary key,
    active SMALLINT NOT NULL DEFAULT 1,
    insert_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    type CHARACTER VARYING COLLATE pg_catalog."default",
    CONSTRAINT ticket_type_pkey PRIMARY KEY (id)
)

-- May 31, 2018

ALTER TABLE events RENAME name TO title;
ALTER TABLE events ADD COLUMN address character varying;

-- Aug 6, 2018
CREATE TABLE public.guests
(
    id SERIAL primary key,
    active SMALLINT NOT NULL DEFAULT 1,
    insert_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    first_name CHARACTER VARYING COLLATE pg_catalog."default",
    last_name CHARACTER VARYING COLLATE pg_catalog."default",
    email CHARACTER VARYING COLLATE pg_catalog."default",
    phone CHARACTER VARYING COLLATE pg_catalog."default",
    event_id Integer,
    address CHARACTER VARYING COLLATE pg_catalog."default",
    city CHARACTER VARYING COLLATE pg_catalog."default",
    state CHARACTER VARYING COLLATE pg_catalog."default",
    zip CHARACTER VARYING COLLATE pg_catalog."default"
)

ALTER TABLE guests ADD COLUMN country CHARACTER VARYING;

-- Aug 13, 2018

ALTER TABLE events ADD COLUMN url CHARACTER VARYING;
ALTER TABLE guests ADD COLUMN no_of_guests CHARACTER VARYING;
-- ALTER TABLE events ALTER COLUMN start_date TYPE timestamp;
-- ALTER TABLE events ALTER COLUMN end_date TYPE timestamp;

--August 14, 2018

ALTER TABLE guests ADD COLUMN confirmation_no CHARACTER VARYING;

--August 15, 2018

ALTER TABLE events ADD COLUMN background_image CHARACTER VARYING COLLATE pg_catalog."default";
ALTER TABLE events ADD COLUMN logo CHARACTER VARYING COLLATE pg_catalog."default";
ALTER TABLE clients ADD COLUMN logo CHARACTER VARYING COLLATE pg_catalog."default";
ALTER TABLE guests ADD COLUMN status CHARACTER VARYING COLLATE pg_catalog."default";

--August 16, 2018

ALTER TABLE events ADD COLUMN registration_status CHARACTER VARYING COLLATE pg_catalog."default";

--August 17, 2018

ALTER TABLE guests ADD COLUMN qrcode CHARACTER VARYING COLLATE pg_catalog."default";

--Augist 28, 2018

ALTER TABLE events ADD COLUMN theme_color CHARACTER VARYING COLLATE pg_catalog."default";

ALTER TABLE events ADD COLUMN featured_image1 CHARACTER VARYING COLLATE pg_catalog."default";
ALTER TABLE events ADD COLUMN featured_image2 CHARACTER VARYING COLLATE pg_catalog."default";
ALTER TABLE events ADD COLUMN featured_image3 CHARACTER VARYING COLLATE pg_catalog."default";
ALTER TABLE events ADD COLUMN featured_image4 CHARACTER VARYING COLLATE pg_catalog."default";

-- September 7,2018

ALTER TABLE guests ADD COLUMN company CHARACTER VARYING COLLATE pg_catalog."default";
ALTER TABLE guests ADD COLUMN volunteer INTEGER DEFAULT 0;

-- September 11,2018

CREATE TABLE public.invites
(
    id SERIAL primary key,
    active SMALLINT NOT NULL DEFAULT 1,
    insert_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    name CHARACTER VARYING COLLATE pg_catalog."default",
    email CHARACTER VARYING COLLATE pg_catalog."default",
    phone CHARACTER VARYING COLLATE pg_catalog."default",
    status CHARACTER VARYING COLLATE pg_catalog."default",
    event_id Integer
)

-- September 12, 2018

ALTER TABLE guests ADD COLUMN check_in_time TIMESTAMP WITHOUT TIME ZONE;

-- September 19, 2018

ALTER TABLE guests ADD COLUMN type CHARACTER VARYING COLLATE pg_catalog."default";
ALTER TABLE events ADD COLUMN insta_url CHARACTER VARYING COLLATE pg_catalog."default";