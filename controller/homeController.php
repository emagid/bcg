<?php

class homeController extends siteController {
        function __construct(){
        parent::__construct();
    }    
    public function index(Array $params = []){

        $url = $params['url'];
        $this->viewData->event = \Model\Event::getItem(null,['where'=>"url = '{$url}'"]);
        
        $this->viewData->banners = \Model\Banner::getList(['where'=>"active='1'"]);
        $this->viewData->mainBanner = \Model\Banner::getList(['where'=>"active = 1 and featured_id = 0", 'orderBy'=>"banner_order asc"]);
        
        
        $this->loadView($this->viewData);
    }
}