<?php

use Twilio\Rest\Client;

class check_inController extends siteController
{

    public function index(Array $params = [])
    {
        $event = $params['event'];
        $this->viewData->event = \Model\Event::getItem(null,['where'=>"url = '{$event}'"]);
        $this->configs['Meta Title'] = "Check In";
        
        $this->loadView($this->viewData);
    }

    public function find_contact_post(){
        $response = ['status'=>false];
        if(isset($_POST['email']) && $_POST['email'] != ''){
            $contact = \Model\Contact::getItem(null,['where'=>"LOWER(email) = LOWER('{$_POST['email']}') OR LOWER(phone) = LOWER('{$_POST['email']}') "]);
            if($contact != null){
                $response['status'] = true;
                $response['contact_id'] = $contact->id;
                $response['name'] = $contact->first_name.' '.$contact->last_name;
            }
        }
        $this->toJson($response);
    }

    public function signin_post(){
        $response = ['status'=>false];
        if(isset($_POST['contact_id']) && $_POST['contact_id'] != ''){
            $sid = TWILIO_SID;
            $token = TWILIO_TOKEN;
            $client = new Client($sid, $token);
            $contact = \Model\Contact::getItem($_POST['contact_id']);
            if($contact->has_balance){
                $client->messages
                    ->create(
                        TWILIO_NOTIFY_NUMBER,
                        array(
                            "from" => TWILIO_NUMBER,
                            "body" => "$contact->first_name $contact->last_name, $contact->email has checked in",
                        )
                    );
            }

            $signin = new \Model\Signature();
            $signin->type = 1;
            $signin->contact_id = $_POST['contact_id'];
            $signin->signature = $_POST['signature'];
            if($signin->save()){
                $response['status'] = true;
            }
        }
        $this->toJson($response);
    }

    public function findFace_post() {
        $r = ['status'=>false];
        if(!isset($_POST['image'])){
            $r['msg']='no image added';
            $this->toJson($r);
        }

        $data_string = array(
            'image'  =>$_POST['image'],
            'gallery_name'=>'bbc'
        );
        $data_string = json_encode($data_string);
        $ch = curl_init('https://api.kairos.com/recognize');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string),
            'app_id:'.KAIROS_APP_ID,
            'app_key:'.KAIROS_API_KEY
        ));
        $result = curl_exec($ch);
        if($result !== FALSE){
            if(isset($result['errors'])){
                return false;
            } else {
                $r['status']=true;
                $candidates = json_decode($result, true)['images'][0]['candidates'];
                $matches = [];
                foreach ($candidates as $candidate){
                    $match = \Model\Contact::findBySubjectId($candidate['subject_id']);
                    if($match){
                        $matches[] = ['name'=>$match->first_name." ".$match->last_name,'id'=>$match->id,'subject_id'=>$candidate['subject_id']];
                    }
                }
                if(count($matches) == 0 ) {
                    $r['status'] = false;
                }
                $r['candidates'] = $matches;
                $this->toJson($r);
            }
        }
    }

    public function validate_post(){
        if(isset($_POST['candidates']) && isset($_POST['pin'])){
            foreach (json_decode($_POST['candidates'], true) as $candidate){
                $user = \Model\Contact::getItem($candidate);
                if($user->pin == $_POST['pin']){
                    $this->toJson(['status'=>true,'user'=>$user->id,'msg'=>"Welcome $user->first_name $user->last_name"]);
                }
            }
            $this->toJson(['status'=>false]);
        }
    }
}