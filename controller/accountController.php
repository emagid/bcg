<?php

class accountController extends siteController
{
    function __construct()
    {
        parent::__construct();
    }

    public function index(Array $params = [])
    {
        $this->configs['Meta Title'] = "BBy diamond Wholesale";
        if(!$this->viewData->user) {
            $n = new \Notification\ErrorHandler('You have to login.');
            $_SESSION["notification"] = serialize($n);
            redirect('/');
        }
//        $this->viewData->wholesale_items = \Model\Wholesale_Item::getList(['where'=>"wholesale_id = {$this->viewData->user->wholesale_id}"]);
        $this->viewData->wholesale_items = $this->viewData->user->wholesale_id;
        $this->loadView($this->viewData);

    }

    public function activate(Array $params = []){
        if(isset($params['hash'])){
            if($user = \Model\User::getCount(['where' => ['hash' => $params['hash'],'setup'=>1]])) {
                $n = new \Notification\ErrorHandler('This account has already been verified, login please');
                $_SESSION["notification"] = serialize($n);
                print_r(\Model\Wholesale::getItem((integer)$params['id']));
                redirect('/home');
            } elseif(!($user = \Model\User::getItem(null, ['where' => ['hash' => $params['hash'],'setup'=>0]]))) {
                $n = new \Notification\ErrorHandler('This account does not exist');
                $_SESSION["notification"] = serialize($n);
                print_r(\Model\Wholesale::getItem((integer)$params['id']));
                redirect('/home');
            } else {
                $wholesale = \Model\Wholesale::getItem($user->wholesale_id);
                if($this->viewData->user = $user && $wholesale->status == 1) {
                    $n = new \Notification\MessageHandler('Welcome, set up your wholesaler account');
                    $_SESSION["notification"] = serialize($n);
                    $this->loadView($this->viewData);
                } else {
                    $n = new \Notification\ErrorHandler('Your Vendor Account has not been approved');
                    $_SESSION["notification"] = serialize($n);
                    redirect('/home');
                }
            }
        } else {
            redirect('/home');
        }

    }

    public function activate_post(){
        $user = \Model\User::loadFromPost();
        $hash = \Emagid\Core\Membership::hash($user->password);
        $user->password = $hash['password'];
        $user->hash = $hash['salt'];
        $user->setup = 1;
        if($user->save()){
            $this->toJson(['status'=>'success','data'=>$_POST,'user'=>get_object_vars($user)]);
        } else {
            $this->toJson(['status'=>'FAILURE','data'=>$_POST,'user'=>get_object_vars($user)]);
        }
    }

    public function shipping_post()
    {
        $address = \Model\Address::loadFromPost();
        $address->save();
        redirect('/account');
    }

    public function billing_post()
    {
        $payment_profile = \Model\Payment_Profile::loadFromPost();
        $payment_profile->save();
        redirect('/account');
    }

    public function password_post(){

        $user = \Model\User::loadFromPost();
        $hash = \Emagid\Core\Membership::hash($user->password);
        $user->password = $hash['password'];
        $user->hash = $hash['salt'];
//        dd($user);
        $user->save();
        redirect('/account');
    }

    public function newsletter_post(){
        $email = $this->viewData->user->email;
        $userId = $this->viewData->user->id;
//        dd($userId);
        foreach($_POST['category'] as $name => $value){
            if($newsLetter = \Model\Newsletter::getItem(null, ['where' => ['category' =>$name, 'user_id' => $userId]])){
                $newsLetter->is_subscribe = ($value == 'on')?1:0;
                $newsLetter->save();
            } else {
                $newsLetter = new \Model\Newsletter();
                $newsLetter->email = $email;
                $newsLetter->is_subscribe = ($value == 'on')?1:0;
                $newsLetter->category = $name;
                $newsLetter->user_id = $userId;
                $newsLetter->save();
            }
        }
        redirect('/account');
    }
}