<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 11/12/15
 * Time: 6:17 PM
 */

class statusController extends siteController {
    function __construct()
    {
        parent::__construct();
    }
    public function index(Array $params = [])
    {
        if(!isset($params['slug'])){
            $n = new \Notification\ErrorHandler('Invalid order number');
            $_SESSION["notification"] = serialize($n);
            redirect('/');
        }

        if(!($order = \Model\Order::getItem(null, ['where'=>['ref_num' => $params['slug']]]))){
            $n = new \Notification\ErrorHandler('Invalid order number');
            $_SESSION["notification"] = serialize($n);
            redirect('/');
        }

        $this->viewData->order = $order;
        $this->loadView($this->viewData);
    }
}