<?php

use Twilio\Rest\Client;

class registerController extends siteController
{
    function __construct()
    {
        parent::__construct();

    }

    public function index(Array $params = []){

        $url = $params['event'];
        $this->viewData->event = \Model\Event::getItem(null,['where'=>"url = '{$url}'"]);
        
        $this->viewData->banners = \Model\Banner::getList(['where'=>"active='1'"]);
        $this->viewData->mainBanner = \Model\Banner::getList(['where'=>"active = 1 and featured_id = 0", 'orderBy'=>"banner_order asc"]);
        $this->loadView($this->viewData);
    }

    public function guest_registration_post(){
        $resp = ['status'=>false];
        $event_id = $_POST['event_id'];
        $email_id = $_POST['email'];
        // $phone = $_POST['phone'];
        // $sid = TWILIO_SID;
        // $token = TWILIO_TOKEN;
        // $client = new Client($sid, $token);

        $check_guest = \Model\Guest::getItem(null,['where'=>"event_id=".$event_id." and email = '".$email_id."'"]);
        if($check_guest){
            $resp['status'] = false;
            $resp['msg'] = "Already Registered for this Event";
        } else {
            $guest = \Model\Guest::loadFromPost();
            $guest->status = \Model\Guest::$status[0];
            // $guest_phone = $guest->phone;
            // Checking if the phone number entered is correct or not.
            // try{
                // $phone_number = $client->lookups->v1->phoneNumbers($guest_phone)
                //     ->fetch(array(
                //             'addOns' => "whitepages_pro_caller_id"
                //         )
                //     );

                // if($phone_number->addOns['status'] == 'successful'){
            if($guest->save()){
                $guest->type = \Model\Guest::$type[1];
                $guest->confirmation_no = $guest->generateToken();
                $guest->status = 'Checked_in';
                $guest->save();
                $event = \Model\Event::getItem($guest->event_id);
                $tempDir = UPLOAD_PATH . 'qrcodes/';
                $codeContents = $guest->confirmation_no;
                $fileName = $guest->id.'_'.md5($codeContents).'.png';
                $pngAbsoluteFilePath = $tempDir.$fileName;
                QRcode::png($codeContents, $pngAbsoluteFilePath); 
                $guest->qrcode = $fileName;

                $guest->save();
                $invite_list = \Model\Invite::getItem(null,['where'=>"email = '".$guest->email."'"]);
                if($invite_list != null){
                    $invite_list->status = \Model\Invite::$status[1];
                    $invite_list->save();
                }

                $qrImage = 'https://'.$_SERVER['SERVER_NAME'].UPLOAD_URL.'qrcodes/'.$guest->qrcode;

                // if(isset($_POST['sendtext']) && $_POST['sendtext'] == 'byText'){
                //     if($phone != ''){
                //         $client->messages
                //             ->create(
                //                 $phone,
                //                 array(
                //                     "from" => TWILIO_NUMBER,
                //                     "body" => "Thank you for registering for ".$event->title.". Here's your QR code and confirmation Number - ".$guest->confirmation_no,
                //                     "mediaUrl" => $qrImage
                //                 )
                //             );
                //         $resp['status'] = true;
                //     }
                // }

                // if(isset($_POST['sendemail']) && $_POST['sendemail'] == 'byEmail'){
                // $email = new \Email\MailMaster();
                // $mergeFields = [
                //     'CONFIRMATION_NUMBER'=>$guest->confirmation_no,
                //     'QRCODE'=>$qrImage
                // ];
                // $email->setTo(['email' => $guest->email, 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('bcg-event');
                // $email->setTo(['email' => $guest->email, 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('events-confirmation');


                // try {
                //     $emailresp = $email->send();
                //     $resp['status'] = true;
                // } catch(Mandrill_Error $e){
                //     $n = new \Notification\ErrorHandler('Email not sent');
                // }

                $resp['emailResp'] = $emailresp;
                // }
                $resp['conf_code'] = $guest->confirmation_no;
                $resp['qrcode'] = $guest->qrcode;
                $resp['guest'] = $guest->no_of_guests;
            }
                // }
            // } catch(Exception $e){
            //     if($e->getStatusCode() == 404) {
            //         $resp['status'] = false;
            //         $resp['msg'] = "invalid phone number";
            //     }
            //     else{
            //         $resp['status'] = false;
            //         $resp['msg'] = "invalid phone number";
            //     }
            // }        
        }  
        $this->toJson($resp);  
    }

    public function guest_checkin_post(){
        $resp = ['status'=>false];
        $event = $_POST['event_id'];
        $email = $_POST['email'];
        $f_name = $_POST['f_name'];
        $l_name = $_POST['l_name'];

        $total_check_in = 0;
        $check_in_count = \Model\Guest::getCount(['where'=>"status = 'Checked-In'"]); 
        // if(isset($_POST['confirmation_no']) && $_POST['confirmation_no'] != ''){
        if(true){
            // $conf_no = $_POST['confirmation_no'];
            $guest = \Model\Guest::getItem(null,['where'=>"event_id = ".$event." and lower(last_name) = lower('".$l_name."') and lower(first_name) = lower('".$f_name."')"]);
            if($guest &&$guest->status != 'Checked-In'){
                $guest->status = \Model\Guest::$status[1];
                date_default_timezone_set('US/Eastern');
                $date = new DateTime();
                $guest->check_in_time = $date->format('Y-m-d H:i:s');
                if($guest->save()){
                    $volunteer = false;
                    if($guest->volunteer == 1){
                        $volunteer = true;
                    }
                    $total_check_in = $check_in_count + 1; 
                    $resp = ['status'=>true,'gname'=>$guest->full_name(),'gemail'=>$guest->email,'total_guest'=>$guest->no_of_guests,'company'=>$guest->company,'volunteer'=>$volunteer,'check_in_count'=>$total_check_in];
                } else {
                    $resp = ['status'=>false];
                }
            } else if($guest && $guest->status == 'Checked-In'){
                $resp = ['status'=>false];
                $resp = ['msg'=>"checked-in"];
            } else {
                $resp = ['status'=>false];
                $resp = ['msg'=>$l_name];
            } 
        }

        if(isset($_POST['confirmation_email']) && $_POST['confirmation_email'] != ''){
            $email = $_POST['confirmation_email'];
            $guest = \Model\Guest::getItem(null,['where'=>"email = '$email' and event_id = ".$event]);
            if($guest){
                $guest->status = \Model\Guest::$status[1];
                date_default_timezone_set('US/Eastern');
                $date = new DateTime();
                $guest->check_in_time = $date->format('Y-m-d H:i:s');
                if($guest->save()){
                    $volunteer = false;
                    if($guest->volunteer == 1){
                        $volunteer = true;
                    }
                    $total_check_in = $check_in_count + 1; 
                    $resp = ['status'=>true,'gname'=>$guest->full_name(),'gemail'=>$guest->email,'total_guest'=>$guest->no_of_guests,'company'=>$guest->company,'volunteer'=>$volunteer,'check_in_count'=>$total_check_in];
                } else {
                    $resp = ['status'=>false];
                }
            } else {
                $resp = ['status'=>false];
                $resp = ['msg'=>"failed"];
            }
            // else if($guest && $guest->status == 'Checked-In'){
            //     $resp = ['status'=>false];
            //     $resp = ['msg'=>"already checked-in"];
            // }
        }
        $this->toJson($resp);
    }

    public function scanQR(){
        $resp = ['status'=>false];
        if(isset($_POST['data']) && $_POST['data'] != ''){
            $data = $_POST['data'];
            $event = $_POST['event'];
            $checkedin_guest = \Model\Guest::getItem(null,['where'=>"confirmation_no = '".strtolower($data)."' AND event_id = ".$event]);
            $total_check_in = 0;
            $check_in_count = \Model\Guest::getCount(['where'=>"status = 'Checked-In'"]);
            if($checkedin_guest){
                $checkedin_guest->status = \Model\Guest::$status[1];
                date_default_timezone_set('US/Eastern');
                $date = new DateTime();
                $checkedin_guest->check_in_time = $date->format('Y-m-d H:i:s');
                if($checkedin_guest->save()){
                    $volunteer = false;
                    if($checkedin_guest->volunteer == 1){
                        $volunteer = true;
                    }
                    $total_check_in = $check_in_count + 1;
                    $resp = ['status'=>true,'gname'=>$checkedin_guest->full_name(),'gemail'=>$checkedin_guest->email,'total_guest'=>$checkedin_guest->no_of_guests,'company'=>$checkedin_guest->company,'volunteer'=>$volunteer,'check_in_count'=>$total_check_in];
                } else {
                    $resp = ['status'=>false];
                }
            } else {
                $resp = ['status'=>false];
                $resp = ['msg'=>"failed"];
            }
            // else if($guest && $guest->status == 'Checked-In'){
            //     $resp = ['status'=>false];
            //     $resp = ['msg'=>"already checked-in"];
            // }
        }
        $this->toJson($resp);
    }
}