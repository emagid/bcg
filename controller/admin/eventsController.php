<?php

use Twilio\Rest\Client;

class eventsController extends adminController {
	
	function __construct(){
		parent::__construct("Event","events");
	}
	
	function index(Array $params = []){
		
		$this->_viewData->hasCreateBtn = true;		
		parent::index($params);
	}

	function update(Array $arr = []){
		$event = isset($arr['id']) ? $arr['id'] : '';
		$this->_viewData->organizers = \Model\Client::getList(['where'=>"active = 1"]);
		$this->_viewData->guests = \Model\Guest::getList(['where'=>"event_id = ".$event]);
		$this->_viewData->volunteers = \Model\Guest::getList(['where'=>"event_id = ".$event." AND volunteer=1"]);
        $preregister = \Model\Guest::$type[1];
        $this->_viewData->preregistered = \Model\Guest::getList(['where'=>"event_id = ".$event." AND type = '".$preregister."'"]);
		parent::update($arr);
	}

	function update_post() {
		$events = \Model\Event::loadFromPost();
		$tickets = \Model\Ticket::loadFromPost();
		$tickets->save();

		parent::update_post();
	}

	function sendInvites(){
		$resp = ['status'=>false];
		$event_id = $_POST['event'];
		$event = \Model\Event::getItem($event_id);
		$reg_url = SITE_DOMAIN.SITE_URL.'home/'.$event->url;
		$client = \Model\Client::getItem($event->client_id);
		if($_FILES['file']['error'] > 0){
            $n = new \Notification\ErrorHandler('File is invalid');
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL."events/update/".$event_id);
        } else {
        	$mFile = fopen($_FILES["file"]["tmp_name"],'r');            
            $header = fgetcsv($mFile);

            while($row = fgetcsv($mFile)){
            	$_invite = array_combine($header, $row);
            	$invite = new Model\Invite($_invite);
            	// foreach($header as $field){
             //        $invite->$field = $_invite[$field];
             //    }
            	$invite->status = \Model\Invite::$status[0];
            	$invite->event_id = $event_id;

            	if($invite->save()){
            		$invitee_email = $invite->email;
            		$email = new \Email\MailMaster();
	                $mergeFields = [
	                    'INVITE_LINK'=>"https://".$_SERVER['SERVER_NAME']."/home/".$event->url
	                ];
	                $email->setTo(['email' => $invitee_email, 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('events-invitation');

	                try {
	                    $emailresp = $email->send();
	                    $resp['status'] = true;
	                } catch(Mandrill_Error $e){
	                    $n = new \Notification\ErrorHandler('Email not sent');
	                }
            	}
            }
        }
        $this->toJson($resp);
	}

	public function registerVolunteers(){
		$resp = ['status'=>false];
		$event_id = $_POST['event'];
		$event = \Model\Event::getItem($event_id);
		if($_FILES['file']['error'] > 0){
            $n = new \Notification\ErrorHandler('File is invalid');
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL."events/update/".$event_id);
        } else {
        	$mFile = fopen($_FILES["file"]["tmp_name"],'r');            
            $header = fgetcsv($mFile);
            $linkHtml = '';
            while($row = fgetcsv($mFile)){
            	$_volunteer = array_combine($header, $row);
                $email_field = $_volunteer['email'];
                $where = "event_id = ".$event_id." AND email = '$email_field'";
                $volunteer = \Model\Guest::getItem(null,['where'=>$where]);
                if($volunteer){
                    foreach ($header as $field) {
                        $volunteer->$field = $_volunteer[$field];
                    }
                    $volunteer->save();
                    $resp = ['status'=>true];
                } else {
                    $volunteer = new Model\Guest($_volunteer);
                    $volunteer->volunteer = 1;
                    $volunteer->event_id = $event_id;
                    $volunteer->no_of_guests = 0;
                    $volunteer->status = \Model\Guest::$status[0];
                    
                    if($volunteer->save()){
                        $volunteer->confirmation_no = $volunteer->generateToken();
                        $volunteer->save();
                        $tempDir = UPLOAD_PATH.'qrcodes/';
                        $codeContents = $volunteer->confirmation_no;
                        $fileName = $volunteer->id.'_'.md5($codeContents).'.png';
                        $pngAbsoluteFilePath = $tempDir.$fileName;
                        QRcode::png($codeContents,$pngAbsoluteFilePath);
                        $volunteer->qrcode = $fileName;
                        $volunteer->save();
                    }

                    $qrImage = 'https://'.$_SERVER['SERVER_NAME'].UPLOAD_URL.'qrcodes/'.$volunteer->qrcode;
                    // $phone = $volunteer->phone;
                    // Sending text to VOlunteer with confirmation number and QRcode
                    // $sid = TWILIO_SID;
                    // $token = TWILIO_TOKEN;
                    // $client = new Client($sid, $token);
                    // if($phone != ''){
                    //     $client->messages
                    //         ->create(
                    //             $phone,
                    //             array(
                    //                 "from" => TWILIO_NUMBER,
                    //                 "body" => "You have been registered as a Volunteer for ".$event->title.". Your QR code and confirmation no. is ".$volunteer->confirmation_no,
                    //                 "mediaUrl" => $qrImage
                    //             )
                    //         );

                    // }

                    // Sending Confirmation Number and QR code to Volunteers
                    $email = new \Email\MailMaster();
                    $mergeFields = [
                        'CONFIRMATION_NUMBER'=>$volunteer->confirmation_no,
                        'QRCODE'=>$qrImage
                    ];
                    $email->setTo(['email'=>$volunteer->email, 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('events-confirmation');

                    try {
                        $emailresp = $email->send();
                        $resp['status'] = true;
                    } catch(Mandrill_Error $e){
                        $n = new \Notification\ErrorHandler('Email not sent');
                    }
                }
            }
        }
        $this->toJson($resp);
	}

    public function importInviteTemplate(){
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=guestInviteTemplate.csv');
        $this->template = false;
        $output = fopen('php://output', 'w');
        $t = ['name','email'];
        fputcsv($output, $t);
    }

    public function importVolunteersTemplate(){
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=volunteersRegistrationTemplate.csv');
        $this->template = false;
        $output = fopen('php://output', 'w');
        $t = ['first_name','last_name','email','company'];
        fputcsv($output, $t);
    }

    public function preregisterGuestTemplate(){
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=preregisterGuestTemplate.csv');
        $this->template = false;
        $output = fopen('php://output', 'w');
        $t = ['first_name','last_name','company','email','no_of_guests'];
        fputcsv($output, $t);
    }

    public function preregisterGuests(){
        $resp = [
            'status'=>false,
            'successes'=>[],
            'failures'=>[],
            'emails'=>[],
            'qrcodes'=>[],
        ];
        
        $event_id = $_POST['event'];
        $event = \Model\Event::getItem($event_id);
        if($_FILES['file']['error'] > 0){
            $n = new \Notification\ErrorHandler('File is invalid');
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL."events/update/".$event_id);
        } else {
            $mFile = fopen($_FILES["file"]["tmp_name"],'r');            
            $header = fgetcsv($mFile);
            while($row = fgetcsv($mFile)){
                $_preregister = array_combine($header, $row);
                $email_field = $_preregister['email'];
                $where = "event_id = ".$event_id." AND email = '$email_field'";

                $preregister = \Model\Guest::getItem(null,['where'=>$where]);
                if($preregister){
                    foreach ($header as $field) {
                        $preregister->$field = $_preregister[$field];
                    }
                    if($preregister->save()){
                        $resp['successes'][$email_field] = $preregister->id;
                    } else {
                        $resp['failures'][$email_field] = $preregister->errors;
                    }
                } else {
                    // $preregister = new Model\Guest($_preregister);
                    if($email_field != ''){
                        $preregister = new Model\Guest();
                        foreach ($header as $field) {
                            $preregister->$field = mb_convert_encoding($_preregister[$field], 'UTF-8');
                        }
                        $preregister->event_id = $event_id;
                        if($preregister->no_of_guests == '' || !is_numeric($preregister->no_of_guests) || intval($preregister->no_of_guests) == 0 ){
                            $preregister->no_of_guests = 0;
                        } else {
                            $preregister->no_of_guests = 1; 
                        }
                        $preregister->status = \Model\Guest::$status[0];
                        $preregister->type = \Model\Guest::$type[1];
                        
                        if($preregister->save()){
                            $preregister->confirmation_no = $preregister->generateToken();
                            $preregister->save();
                            $tempDir = UPLOAD_PATH.'qrcodes/';
                            $codeContents = $preregister->confirmation_no;
                            $fileName = $preregister->id.'_'.md5($codeContents).'.png';
                            $pngAbsoluteFilePath = $tempDir.$fileName;
                            QRcode::png($codeContents,$pngAbsoluteFilePath);
                            $preregister->qrcode = $fileName;
                            $preregister->save();
                            $resp['successes'][$email_field] = $preregister->id;
                        } else {
                            $resp['failures'][$email_field] = $preregister->errors;
                        }

                        $qrImage = 'https://'.$_SERVER['SERVER_NAME'].UPLOAD_URL.'qrcodes/'.$preregister->qrcode;
                        $phone = $preregister->phone;
                        // Sending text to VOlunteer with confirmation number and QRcode
                        $sid = TWILIO_SID;
                        $token = TWILIO_TOKEN;
                        $client = new Client($sid, $token);
                        if($phone != ''){
                            $client->messages
                            ->create(
                                $phone,
                                array(
                                    "from" => TWILIO_NUMBER,
                                    "body" => "You have been registered as a Volunteer for ".$event->title.". Your QR code and confirmation no. is ".$preregister->confirmation_no,
                                    "mediaUrl" => $qrImage
                                )
                            );
                        }

                        $tickets_text = "Below you will find your ticket to the BCG Holiday Party on <span style='margin-bottom: 15px; color: #197a56; font-weight: bold;'>12/15 at Cipriani 42nd Street.</span> <span style='display:block; margin-top: 15px;'>Please do not delete this email, as the QR code below will grant you entry to the event. See you there!</span>";
                        if($preregister->no_of_guests){
                            $tickets_text = "Below you will find you and your guest’s ticket to the BCG Holiday Party on <span style='margin-bottom: 15px; color: #197a56; font-weight: bold;'>12/15 at Cipriani 42nd Street.</span> <span style='display:block; margin-top: 15px;'>Please do not delete this email, as the QR code below will grant you and your guest entry to the event. See you there!</span>";
                        }

                        // Sending Confirmation Number and QR code to Volunteers
                        $email = new \Email\MailMaster();
                        $mergeFields = [
                            'TICKETS_TEXT'=>$tickets_text,
                            'CONFIRMATION_NUMBER'=>$preregister->confirmation_no,
                            'QRCODE'=>$qrImage
                        ];
                        $email->setTo([
                            'email'=>$preregister->email,
                             'type' => 'to'])->setMergeTags($mergeFields)
                             ->setTemplate('bcg-event');

                        try {
                            $emailresp = $email->send();
                        } catch(Mandrill_Error $e){
                            $n = new \Notification\ErrorHandler('Email not sent');
                        }
                        $resp['emails'][$email_field] = $emailresp;
                    }
                }
            }
            if(count($resp['successes']) > 0){
                $resp['status'] = true;
            }
        }
        $this->toJson($resp);
    }

}