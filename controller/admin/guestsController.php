<?php

class guestsController extends adminController {
	
	function __construct(){
		parent::__construct("Guest","guests");
	}
	
	function index(Array $params = []){
		
		// $this->_viewData->hasCreateBtn = true;
		$eventSql ='';
		$statusSql = '';
		if(isset($_GET['event']) && $_GET['event'] != ''){
            $eventSql = " AND event_id = {$_GET['event']}";
        }
        if(isset($_GET['status']) && $_GET['status'] != ''){
        	// $status = $_GETp
            $statusSql = " AND status = '{$_GET['status']}'";
        }
        $params['queryOptions']=['where'=>"active = 1$eventSql$statusSql"];
		parent::index($params);
	}

	function update(Array $arr = []){
		$this->_viewData->events = \Model\Event::getList();
		parent::update($arr);
	}

	function update_post() {
		
		parent::update_post();
	}

	public function search()
    {
        $guests = \Model\Guest::search($_GET['keywords']);

        echo '[';
        foreach ($guests as $key => $guest) {
        	$event = \Model\Event::getItem($guest->event_id);
        	$type = $guest->volunteer == 1 ? 'Volunteer':'Guest';
            $check_in_time = $guest->check_in_time != '' ? date('M d, Y h:ia',strtotime($guest->check_in_time)) : '-';
            echo '{ "id": "' . $guest->id . '", "first_name": "' . $guest->first_name. '", "last_name": "'.$guest->last_name . '", "email": "' . $guest->email . '", "company":"' . $guest->company . '", "check_in_time":"' . $check_in_time . '", "event":"'.$event->title.'", "type":"'.$type.'", "status":"'.$guest->status.'" }';
            if ($key < (count($guests) - 1)) {
                echo ",";
            }
        }
        echo ']';
    }

    public function sendReminder(){
    	if(isset($_GET['event']) && $_GET['event'] != ''){
    		$event_id = $_GET['event'];
    		$registered = \Model\Guest::$status[0];
    		$guests = \Model\Guest::getList(['where'=>"event_id = ".$event_id." and status = '".$registered."'"]);
    		if(count($guests) > 0){
    			foreach($guests as $guest) {
    				$email = new \Email\MailMaster();
                    $qrImage = 'https://'.$_SERVER['SERVER_NAME'].UPLOAD_URL.'qrcodes/'.$guest->qrcode;
    				$mergeFields = [
    					'CONFIRMATION_NUMBER'=>$guest->confirmation_no,
    					'QRCODE'=>$qrImage
    				];
    				$email->setTo(['email'=>$guest->email, 'type'=>'to'])->setMergeTags($mergeFields)->setTemplate('events-confirmation');

    				try{
    					$emailresp = $email->send();
    					$n = new \Notification\MessageHandler('Sent Reminder Successfully');
            			$_SESSION["notification"] = serialize($n);
            			// redirect('/admin/guests');
    				}catch(Mandrill_Error $e){
    					$n = new \Notification\ErrorHandler('Email not sent');
    					$_SESSION["notification"] = serialize($n);
    					// redirect('/admin/guests');
    				}
    			}
                redirect('/admin/guests');
    		}
    	} else {
    		$n = new \Notification\ErrorHandler('Select Event to send Reminders');
    		$_SESSION["notification"] = serialize($n);
    		redirect('/admin/guests');
    	}
    }

    public function exportGuestLists(Array $params = []){
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=guests_complete_email_list.csv');
        
        $sql="SELECT DISTINCT ON (email) * FROM guests WHERE active=1";

        $guests = \Model\Guest::getList(['sql'=>$sql]);
        $output = fopen('php://output', 'w');
        $t=array("No.",'Email');
        fputcsv($output, $t);
        $row ='';

        foreach($guests as $key=>$guest) {

            $row = array($key+1,$guest->email);
            fputcsv($output, $row);  
        }
    }
  
}