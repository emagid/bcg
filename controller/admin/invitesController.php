<?php

class invitesController extends adminController {
	
	function __construct(){
		parent::__construct("Invite","invites");
	}
	
	function index(Array $params = []){
		
		// $this->_viewData->hasCreateBtn = true;
		parent::index($params);
	}

	function update(Array $arr = []){
		$this->_viewData->events = \Model\Event::getList();
		parent::update($arr);
	}

	function update_post() {
		
		parent::update_post();
	}
  
}