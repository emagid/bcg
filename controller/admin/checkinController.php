<?php

class checkinController extends adminController {
	
	function __construct(){
		parent::__construct("Signature","signins");
	}
	
	function index(Array $params = []){
		
		$this->_viewData->start = '';
        $this->_viewData->end = '';
        if(isset($_GET['t'])){
            $get = explode(',',urldecode($_GET['t']));
            $start = date('Y-m-d H:i:s',$get[0]); $end = date('Y-m-d H:i:s',$get[1]);
            $this->_viewData->start = $start;
            $this->_viewData->end = $end;
            if(isset($params['queryOptions']['where'])){
                $params['queryOptions']['where'][] = "insert_time between '$start' and '$end'";
            } else {
                $params['queryOptions']['where'][] = "insert_time between '$start' and '$end'";
            }
        }
        if(isset($params['queryOptions']['where'])){
            $params['queryOptions']['where'] = implode(' and ',$params['queryOptions']['where']);
        }
		parent::index($params);
	}

	public function search(){

		$contacts = \Model\Contact::Search($_GET['keywords']);

		$a = Array();
		echo '[';
		foreach ($contacts as $contact) {
			
			$checkins = \Model\Signature::getList(['where'=>"contact_id = $contact->id"]);

			foreach ($checkins as $checkin) {
				$a[] =  '{ "first_name": "'. $contact->first_name . '",
				"last_name": "'. $contact->last_name . '",
				"email": "'. $contact->email . '",
				"phone": "'. $contact->phone . '",
				"insert_time": "'. $checkin->toDate() . '"}';
			}
		}
		echo implode(',', $a);
		echo ']';
	}
  
}