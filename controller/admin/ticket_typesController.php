<?php

class ticket_typesController extends adminController {
	
	function __construct(){
		parent::__construct("Ticket_Type","ticket_types");
	}
	
	function index(Array $params = []){
		

		$this->_viewData->hasCreateBtn = true;
		parent::index($params);
	}

	function update(Array $arr = []){

		parent::update($arr);
	}
  
}