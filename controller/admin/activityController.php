<?php

class activityController extends adminController {
	
	function __construct(){
		parent::__construct("Signature","Signins");
	}
	
	function index(Array $params = []){
		parent::index($params);
	}

	function update(Array $arr = []){
		$cat = new $this->_model(isset($arr['id'])?$arr['id']:null);
		$this->_viewData->credits = \Model\Credit::getList(['where'=>" active = 1"]);
		
		parent::update($arr);
	}

	function get_sig_post(){
	    $response = ['status'=>true];
	    $signature = \Model\Signature::getItem($_POST['id']);
	    if($signature != null){
	        $response['status'] = true;
	        $response['signature'] = $signature->signature;
        }
        $this->toJson($response);
    }
  
}