<?php

class blogsController extends adminController {
	
	function __construct(){
		parent::__construct("Blog");
	}
  	
	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;    

        parent::index($params);
    }
  	
}