<?php

class contactsController extends adminController {
	
	function __construct(){
		parent::__construct("Contact", "contacts");
	}
	
	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;		
		$this->_viewData->hasLoadBtn = true;

	

		parent::index($params);
	}

	function update(Array $arr = []){
		$cat = new $this->_model(isset($arr['id'])?$arr['id']:null);
		$this->_viewData->credits = \Model\Credit::getList(['where'=>" active = 1"]);
		
		parent::update($arr);
	}

	function readCSV_post(){
        parent::readCSV_post();

        $imported = 0;
        $updated = 0;
        if (! function_exists('array_column')) {
            function array_column(array $input, $columnKey, $indexKey = null) {
                $array = array();
                foreach ($input as $value) {
                    if ( !array_key_exists($columnKey, $value)) {
                        trigger_error("Key \"$columnKey\" does not exist in array");
                        return false;
                    }
                    if (is_null($indexKey)) {
                        $array[] = $value[$columnKey];
                    }
                    else {
                        if ( !array_key_exists($indexKey, $value)) {
                            trigger_error("Key \"$indexKey\" does not exist in array");
                            return false;
                        }
                        if ( ! is_scalar($value[$indexKey])) {
                            trigger_error("Key \"$indexKey\" does not contain scalar value");
                            return false;
                        }
                        $array[$value[$indexKey]] = $value[$columnKey];
                    }
                }
                return $array;
            }
        }
        $mFile = fopen($_FILES["contacts"]["tmp_name"],'r');
        $header = fgetcsv($mFile);
        while($row = fgetcsv($mFile)){
            $import = false;
            $_contact = array_combine($header, $row);
            $contact = \Model\Contact::getItem(null,['where'=>"email = '{$_contact['email']}'"]);
            if(!$contact) {
                $import = true;
                $contact = new \Model\Contact();
            }
            foreach ($_contact as $key => $value){
                $_key = strtolower($key);
                $contact->$_key = $value;
            }
            if(isset($_contact['﻿first_name'])){
                $contact->first_name = $_contact['﻿first_name'];
            }
            if($contact->save()){
                if($import){
                    $imported++;
                } else {
                    $updated++;
                }
            }
        }
        fclose($mFile);
        $n = new \Notification\MessageHandler("$imported $this->_content imported <br />$updated $this->_content updated ");
        $_SESSION["notification"] = serialize($n);
        redirect(ADMIN_URL . $this->_content);

    }

    public function search()
    {
        $contacts = \Model\Contact::search($_GET['keywords']);

        echo '[';
        foreach ($contacts as $key => $contact) {
            echo '{ "id": "' . $contact->id . '", "name": "' . $contact->first_name.' '.$contact->last_name . '", "email": "' . $contact->email . '", "phone":"' . $contact->phone . '", "last_sign_in":"' . $contact->lastSignin() . '" }';
            if ($key < (count($contacts) - 1)) {
                echo ",";
            }
        }
        echo ']';
    }

	function get_sig_post(){
	    $response = ['status'=>true];
	    $signature = \Model\Signature::getItem($_POST['id']);
	    if($signature != null){
	        $response['status'] = true;
	        $response['signature'] = $signature->signature;
        }
        $this->toJson($response);
    }
  
}