<?php

class clientsController extends adminController {
	
	function __construct(){
		parent::__construct("Client","clients");
	}
	
	function index(Array $params = []){
		

		$this->_viewData->hasCreateBtn = true;
		parent::index($params);
	}

	function update(Array $arr = []){
		$client_id = $arr['id'];
		if($client_id != '' && $client_id != 0){
			$this->_viewData->organizers_event = \Model\Event::getList(['where'=>"client_id = $client_id"]);
		}
		parent::update($arr);
	}

	function update_post(){
		// $client_id = $arr['id'];
		// $this->_viewData->organizers_event = \Model\Event::getList(['where'=>"client_id = $client_id"]);
		parent::update_post();
	}
  
}