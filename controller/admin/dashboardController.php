<?php

use Emagid\Core\Membership,
Emagid\Html\Form,
Model\Admin,
Model\Banner; 

class dashboardController extends adminController {
	
	function __construct(){
		parent::__construct('Dashboard');
	}

	public function index(Array $params = []){
		$this->_viewData->page_title = 'Dashboard';
		$this->_viewData->total_registration_count = \Model\Guest::getCount(['where'=>"status='Registered'"]);
		$this->_viewData->check_in_count = \Model\Guest::getCount(['where'=>"status='Checked-In'"]);
		$this->_viewData->total_registration_count += $this->_viewData->check_in_count;
		$this->_viewData->events = \Model\Event::getList();

		$this->loadView($this->_viewData);
	}

}