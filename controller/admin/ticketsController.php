<?php

class ticketsController extends adminController {
	
	function __construct(){
		parent::__construct("Ticket","tickets");
	}
	
	function index(Array $params = []){
		

		$this->_viewData->hasCreateBtn = true;
		parent::index($params);
	}

	function update(Array $arr = []){

		parent::update($arr);
	}
  
}