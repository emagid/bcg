<?php

class aboutController extends siteController
{

    public function index(Array $params = [])
    {

        $this->configs['Meta Title'] = "About Us";
        $this->loadView($this->viewData);
    }

}