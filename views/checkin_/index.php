<header>
  <a href='/'><img src="<?=FRONT_ASSETS?>img/winebow_logo.png"></a>
</header>
<div class='dark_overlay'>
    <div class='blue_overlay'>
        <section class='checkin'>
          <div class='step_one'>
            <h1>STEP ONE</h1>
            <p>Enter your email or phone number</p>
            <input id="email-phone" type="text" />
               <div class='back_arrow'>
                <i class="fas fa-arrow-left"></i>
                <p>BACK</p>
              </div>
              <button class='button'>LOGIN</button>
              <a href="/register/">Register new account</a>
          </div>

          <div class='verify overlay'>
              <div class='x'><i class="fas fa-times-circle"></i></div>
                <h2 id="verify_name"></h2>
                <a class="button" id="verify">YES</a>
           </div>


          <div class='waiver_holder step_two'>
              <form id="checkin_form">
                  <input id="contact_id" type='hidden' name="contact_id">
                  <input id="drawnSig" type='hidden' name="signature">
                  <div class='fixed_waiver'>
                      <img src="<?=FRONT_ASSETS?>img/waiver.png">
                  </div>
                  <div class='white_back'></div>
                  <p class='sign_here'>Sign here</p>
                  <canvas id='canvas'></canvas>
                 <div class='back_arrow arrow1'>
                    <i class="fas fa-arrow-left"></i>
                    <p>BACK</p>
                  </div>
                  <input type="submit" class='button' value="COMPLETE CHECKIN"/>
              </form>
              <!-- <p class='fail f_waiver'>Please sign the waiver</p> -->
            </div>


          <div class='overlay success'>
            <h1>SUCCESS</h1>
            <p>You have checked in!</p>
          </div>

        </section>
  </div>
</div>

<style type="text/css">
  .waiver_holder.step_two #checkin_form .button {
    right: -202px;
  }
</style>


<script type="text/javascript">

    $('.back_arrow').click(function(){
      $('.checkin').fadeOut(1000);
      $(this).children('svg').css('transform', 'translateX(-10px)');
      setTimeout(function(){
        window.location.href = '/';
      }, 1000);
    });

    $('.back_arrow.arrow1').click(function(){
      $('.checkin').fadeOut(1000);
      $(this).children('svg').css('transform', 'translateX(-10px)');
      setTimeout(function(){
        location.reload();
      }, 1000);
    });

    function checkNumberFieldLength(elem){
        if (elem.value.length > 4) {
            elem.value = elem.value.slice(0,4); 
        }
      }

    window.addEventListener('imageCaptured',function(i){
        $.post('/checkin/findFace',{image:i.detail},function (data) {
            if(data.status){
                $('#face_name').text(data.candidates[0]['name']);
                let ids = [];
                for(var i = 0; i < data.candidates.length; i++){
                    ids.push(data.candidates[i]['id']);
                    $('#candidates').val("["+ids.join(',')+"]");
                }
            } else {
                alert ('NO MATCHES FOUND');
                window.location.href = '/';
            }
            console.log(data)
        });

        $('#checkPin').click(function(e){
            e.preventDefault();
            $.post('/checkin/validate',$('#pin_form').serialize(),function (data) {
                if(data.status){
                    $('.check').fadeIn();
                    setTimeout(function(){
                        $('.overlay').slideDown();
                        $('.overlay').css('display', 'flex');
                    }, 6000);

                    setTimeout(function(){
                        window.location.href = '/';
                    }, 10000);

                } else {
                    alert('Incorrect Pin, Try Again');
                }
            });
        });

    });
    var signaturePad;
    $(function(){

        $('#verify').click(function(){
            $('.step_one').hide();
            $('.overlay').fadeOut();
            $('.waiver_holder').fadeIn();
        });
        $('#email-phone').change(function(){
           var email = this.value;
           $.post('/checkin/find_contact',{email:email},function(data){
             if(data.status == true) {
                $('.verify').fadeIn();
                $('.verify').addClass('correct');
                $('.verify').css('display' , 'flex');
                $('.step_one a:nth-child(6)').fadeOut();
                $('#verify_name').text('Are you '+data.name+'?');
                $('#contact_id').val(data.contact_id);
             } else {
                $('.verify').fadeIn();
                $('.verify').addClass('wrong');
                $('.verify').css('display' , 'flex');
                $('.overlay.wrong a').hide();
                $('#verify_name').text('No match found, please register');
                $('.step_one a:nth-child(6)').css('top', '85px');
                $('#contact_id').val(data.contact_id);
             }
           });
        });


        $('.verify .x').click(function(){
            location.reload();
        })


        $('#checkin_form').submit(function(e){
            e.preventDefault();
            if(signaturePad.isEmpty()){
                alert('Sign the waiver first');
                return true;
            } else {
                $.post('/checkin/signin',$('#checkin_form').serialize(),function(data){
                    if(data.status == true){
                        $('.overlay.success').fadeIn();
                        $('.overlay.success').css('display', 'flex');
                        setTimeout(function(){
                              window.location.href = '/';
                        }, 2000);
                    }
                })
            }
        });

        // SIGNATURE
        signaturePad = new SignaturePad(canvas,{
            onEnd: function(){
                $('#drawnSig').val(JSON.stringify(signaturePad.toData()));
            }
        });
        
        function resizeCanvas() {
            var canvas = document.querySelector("canvas");
            var ratio =  Math.max(window.devicePixelRatio || 1, 1);
            canvas.width = 500 * ratio;
            canvas.height = 100 * ratio;
            canvas.getContext("2d").scale(ratio, ratio);
            signaturePad.clear(); // otherwise isEmpty() might return incorrect value
        }
        
          window.addEventListener("resize", resizeCanvas);
          resizeCanvas()


          $('#verify').click(function(){
          $('.fixed_waiver').animate({
                        scrollTop: $(".fixed_waiver").offset().top + 300
            }, 30000);
          });


          $('img').mousedown(function(){
            jQuery('.fixed_waiver').stop(true);
          });

          $('#canvas').mousedown(function(){
            alert('heelp')
            $('.sign_here').fadeOut();
          });


    });
</script>

