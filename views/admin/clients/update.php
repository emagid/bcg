<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#general-tab" aria-controls="general"  role="tab" data-toggle="tab">General</a></li>
        <li role="presentation"><a href="#events-tab" aria-controls="events"  role="tab" data-toggle="tab">Events</a></li>
 
    </ul>
    
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="general-tab">
            <input type="hidden" name="id" value="<?php echo $model->client->id; ?>" />
            <input name="token" type="hidden" value="<?php echo get_token();?>" />
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Client Details</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Name</label>
                                    <?php echo $model->form->editorFor("name"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email</label>
                                    <?php echo $model->form->editorFor("email"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <?php echo $model->form->editorFor("phone"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Logo</label>
                                    <p><small>(ideal profile photo size is 850 x 850)</small></p>
                                    <p><input type="file" name="logo" class='image' /></p>
                                    <div style="display:inline-block">
                                        <?php 
                                            $img_path = "";
                                            if($model->client->logo != "" && file_exists(UPLOAD_PATH.'clients'.DS.$model->client->logo)){ 
                                                $img_path = UPLOAD_URL . 'clients/' . $model->client->logo;
                                        ?>
                                                <div class="well well-sm pull-left" style="max-width:106.25px;max-height:106.25px;">
                                                    <img src="<?php echo $img_path; ?>" />
                                                    <br />
                                                    <a href="<?= ADMIN_URL.'clients/delete_image/'.$model->client->id;?>?logo=1" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>
                                                    <input type="hidden" name="logo" value="<?=$model->client->logo?>" />
                                                </div>
                                        <?php } ?>
                                        <div class='preview-container' style="max-width:106.25px;max-height:106.25px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div role="tabpanel" class="tab-pane" id="events-tab">
            <input type="hidden" name="id" value="<?php echo $model->client->id; ?>" />
            <input name="token" type="hidden" value="<?php echo get_token();?>" />
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>All Event List</h4>
                        <table id="data-list" class="table">
                            <thead>
                                <tr>
                                    <th>Event Title</th>
                                    <th>Venue</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($model->organizers_event as $obj) { ?>
                                    <tr class="originalProducts">
                                        <td><a href=""><?php echo $obj->title; ?></a></td>
                                        <td><a href=""><?php echo $obj->location; ?></a></td>
                                        <td><a href=""><?php echo $obj->start_date; ?></a></td>
                                    </tr> 
                                <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
 
<script type="text/javascript">
    $(function(){
        $("input.image").change(function(){
          readURL(this);
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var img = $("<img />");
            reader.onload = function (e) {
                img.attr('src',e.target.result);
                img.attr('alt','Uploaded Image');
                img.attr("width",'100%');
                img.attr('height','100%');
            };
            $(input).parent().parent().find('.preview-container').html(img);
            $(input).parent().parent().find('input[type="hidden"]').remove();

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>