
<?php
 if(count($model->clients)>0){ ?>

  <div class="box box-table">
    <table class="table" id="data-list">
      <thead>
        <tr>
            <th width="20%">Logo</th>
            <th width="10%">Name</th>
            <th width="10%">Email</th>
            <th width="10%">Phone</th>
            <th width="5%">Events</th>
            <th width="5%" class="text-center">Edit</th>
            <th width="5%" class="text-center">Delete</th>
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->clients as $obj){
        $event = \Model\Event::getCount(['where'=>"client_id = $obj->id"]);
          ?>
        <tr class="allCheckins">
            <td>
              <a href="<?php echo ADMIN_URL; ?>clients/update/<?php echo $obj->id; ?>">
              <img src= "<?php echo UPLOAD_URL; ?>clients/<?=$obj->logo;?>" height="60px"/> 
              </a>
            </td>
            <td><a href="<?php echo ADMIN_URL; ?>clients/update/<?= $obj->id ?>"><?php echo $obj->name; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>clients/update/<?= $obj->id ?>"><?php echo $obj->email; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>clients/update/<?= $obj->id ?>"><?php echo $obj->phone; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>clients/update/<?= $obj->id ?>"><?php echo $event; ?></a></td>
            <td class="text-center">
              <a class="btn-actions" href="<?= ADMIN_URL ?>clients/update/<?= $obj->id ?>">
              <i class="icon-pencil"></i> 
              </a>
            </td>
            <td class="text-center">
              <a class="btn-actions" href="<?= ADMIN_URL ?>clients/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
                <i class="icon-cancel-circled"></i> 
              </a>
            </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php } ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'clients';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
