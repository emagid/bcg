<?php if(count($model->pages)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
          <th width="40%">Page</th> 
          <th width="30%">URL</th> 
          <th width="15%" class="text-center">Edit</th>	
          <th width="15%" class="text-center">Delete</th>	
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->pages as $page): ?>
        <tr>
         <td><a href="<?php echo ADMIN_URL; ?>pages/update/<?php echo $page->id; ?>"><?php echo $page->title; ?></a></td>
         <td><?php echo $page->slug; ?></td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>pages/update/<?php echo $page->id; ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>pages/delete/<?php echo $page->id; ?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
     <?php endforeach; ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'pages/index';?>';
  var total_pages = <?= $model->pagination->total_pages;?>;
  var page = <?= $model->pagination->current_page_index;?>;

</script>

