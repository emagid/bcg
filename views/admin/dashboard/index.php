<div id="page-wrapper">  
	<!-- <div class='latest_media'>
		<img src="https://static.wixstatic.com/media/c11316_9ccc15c96fd04386b552cadf789f303f~mv2.gif">
	</div> -->
</div>

<script>
	var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
	var dataSet = {"previous":{"2014-04":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-05":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-06":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-07":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-08":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-09":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-10":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"}},"current":{"2015-04":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2015-05":{"monthly_sales":"4078.22","gross_margin":0,"monthly_orders":"8"},"2015-06":{"monthly_sales":"9485.6","gross_margin":0,"monthly_orders":"25"},"2015-07":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2015-08":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2015-09":{"monthly_sales":"35","gross_margin":0,"monthly_orders":"1"},"2015-10":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"}}};

	var months = [];
	for (key in dataSet.previous){
		var month = new Date(key.split('-')[0], key.split('-')[1]-1);
		months.push(monthNames[month.getMonth()]);
	}

	var set = [[],[]];
	for (key in dataSet.previous){
		set[0].push(dataSet.previous[key].monthly_sales);
	}
	for (key in dataSet.current){
		set[1].push(dataSet.current[key].monthly_sales);
	}
	var barChartData1 = {
		labels : months,
		datasets : [
			{
				fillColor : "rgba(220,220,220,0.5)",
				strokeColor : "rgba(220,220,220,0.8)",
				highlightFill: "rgba(220,220,220,0.75)",
				highlightStroke: "rgba(220,220,220,1)",
				data : set[0]
			},
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : set[1]
			}
		]

	}

	var lineChartData = {
		labels : months,
		datasets : [
			{
				label: "Previous",
				fillColor : "rgba(220,220,220,0.2)",
				strokeColor : "rgba(220,220,220,1)",
				pointColor : "rgba(220,220,220,1)",
				pointStrokeColor : "#fff",
				pointHighlightFill : "#fff",
				pointHighlightStroke : "rgba(220,220,220,1)",
				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
			},
			{
				label: "Current",
				fillColor : "rgba(151,187,205,0.2)",
				strokeColor : "rgba(151,187,205,1)",
				pointColor : "rgba(151,187,205,1)",
				pointStrokeColor : "#fff",
				pointHighlightFill : "#fff",
				pointHighlightStroke : "rgba(151,187,205,1)",
				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
			}
		]

	}

	set = [[],[]];
	for (key in dataSet.previous){
		set[0].push(dataSet.previous[key].monthly_orders);
	}
	for (key in dataSet.current){
		set[1].push(dataSet.current[key].monthly_orders);
	}
	var barChartData2 = {
		labels : months,
		datasets : [
			{
				fillColor : "rgba(220,220,220,0.5)",
				strokeColor : "rgba(220,220,220,0.8)",
				highlightFill: "rgba(220,220,220,0.75)",
				highlightStroke: "rgba(220,220,220,1)",
				data : set[0]
			},
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : set[1]
			}
		]

	}

	window.onload = function(){
		var ctxB = document.getElementById("B").getContext("2d");
	 	window.myLine = new Chart(ctxB).Line(lineChartData, {
	 		responsive: true
	 	});
		var ctxA = document.getElementById("A").getContext("2d");
		window.myBar = new Chart(ctxA).Bar(barChartData1, {
			responsive : true
		});
		var ctxC = document.getElementById("C").getContext("2d");
		window.myBar = new Chart(ctxC).Bar(barChartData2, {
			responsive : true
		});
	}

</script> 
<div class='cards'>
	<div class="card">
		<a href="/admin/guests">
			<h3><span><?=$model->total_registration_count?></span> Total Registration</h3>
		</a>
	</div>

	<div class="card">
		<a href="/admin/guests">
			<h3><span><?=$model->check_in_count?></span> Checked-In</h3>
		</a>
	</div>
</div>

<!-- <div class="action_cards">
    <div class="card">
        <a href= "/admin/clients">
            <img src="<?=FRONT_ASSETS?>img/phone-book.png">
            <h3>Clients</h3>
        </a>
        
    </div>
    <div class="card">
        <a href="/admin/events">
            <img src="<?=FRONT_ASSETS?>img/checked.png">
            <h3>Events</h3>
        </a>
    </div>
        <div class="card">
        <a href="/admin/administrators">
            <img src="<?=FRONT_ASSETS?>img/admin.png">
            <h3>Admin</h3>
        </a>
        
    </div> -->
<!--
        <div class="card">
        <img src="<?=FRONT_ASSETS?>img/register_icon.png">
        <h3>Title</h3>
        
    </div>
-->
<!-- </div> -->

<div class=" charts-list">
	<div class="col-md-24">
		<div class="panel panel-default" style="min-height: 220px;">
			<div class="panel-heading">
				<h3 class="panel-title">Event Wise Guests Count</h3>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover table-striped tablesorter">
						<thead>
							<tr>
								<th class="header">Event<i class="fa fa-sort"></i></th>
								<th class="header">Invitation Sent<i class="fa fa-sort"></i></th>
								<th class="header">Pre-registered Guests<i class="fa fa-sort"></i></th>
								<th class="header">Volunteers<i class="fa fa-sort"></i></th>
								<th class="header">Total Guests<i class="fa fa-sort"></i></th>
								<th class="header">Total Checked-in<i class="fa fa-sort"></i></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($model->events as $event): 
								$total_guests = \Model\Guest::getCount(['where'=>"event_id=".$event->id]);
								$Prereg_count = \Model\Guest::getCount(['where'=>"event_id=".$event->id." AND type='Pre-Registered Guest'"]);
								$volunteers_count = \Model\Guest::getCount(['where'=>"event_id=".$event->id." AND volunteer=1"]);
								$total_checked_in = \Model\Guest::getCount(['where'=>"event_id=".$event->id." AND status='Checked-In'"]);
								$total_invites = \Model\Invite::getCount(['where'=>"event_id = ".$event->id]);
								?>
							<tr>
								<td><a href="<?=ADMIN_URL?>guests"><?php echo $event->title; ?></a></td>
								<td><a href="<?=ADMIN_URL?>invites"><?php echo $total_invites; ?></a></td>
								<td><a href="<?=ADMIN_URL?>guests"><?php echo $Prereg_count; ?></a></td>
								<td><a href="<?=ADMIN_URL?>guests"><?= $volunteers_count;?></a></td>
								<td><a href="<?=ADMIN_URL?>guests"><?php echo $total_guests; ?></a></td>
								<td><a href="<?=ADMIN_URL?>guests"><?php echo $total_checked_in; ?></a></td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<div class="text-right">
				<a href="<?=ADMIN_URL?>guests">View All Guests <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
    .action_cards {
        width: 100%;
        overflow: hidden;
    }
    .card {
        width: 18%;
        margin-left: 2%;
        margin-right: 2%;
        float: left;
        padding: 15px;
        background: #F9F9F9;
    }
    .card img {
        width: 60%;
        max-width: 68px;
    }
    .card h3 {
        text-transform: uppercase;
        font-size: 16px;
        font-weight: bold;
        color:#696969;
    }
</style>
 
<?php echo footer(); ?>