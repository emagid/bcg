<!--  -->
<?php
 if(count($model->events)>0){ ?>

  <div class="box box-table">
    <table class="table" id="data-list">
      <thead>
        <tr>
            <th width="20%" class="text-center">Event Logo</th>
            <th width="15%" class="text-center">Name</th>
            <th width="15%" class="text-center">Place</th>
            <th width="10%" class="text-center">Date</th>
            <!-- <th width="10%" class="text-center">Time</th> -->
            <!-- <th width="15%" class="text-center">Type</th> -->
            <th width="15%" class="text-center">Client</th>
            <th width="15%" class="text-center">Event Url</th>
            <th width="15%" class="text-center">Check-in URL</th>
            <th width="10%" class="text-center">Registration Status</th>
            <th width="5%" class="text-center">Edit</th>
            <th width="5%" class="text-center">Delete</th>
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->events as $obj){
        $client = \Model\Client::getItem($obj->client_id);
          ?>
        <tr class="allCheckins">
            <td>
              <a href="<?php echo ADMIN_URL; ?>events/update/<?php echo $obj->id; ?>">
              <img src= "<?php echo UPLOAD_URL; ?>events/<?=$obj->logo;?>" height="60px"/> 
              </a>
            </td>
            <td><a href="<?php echo ADMIN_URL; ?>events/update/<?= $obj->id ?>"><?php echo $obj->title; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>events/update/<?= $obj->id ?>"><?php echo $obj->venue; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>events/update/<?= $obj->id ?>"><?php echo $obj->start_date; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>events/update/<?= $obj->id ?>"><?php echo $client->name; ?></a></td>
            <? 
              // $event_url = 'https://'.$_SERVER['SERVER_NAME'].'/home/'.$obj->url;
              //New Register page URL
              $event_url = 'https://'.$_SERVER['SERVER_NAME'].'/register/event/'.$obj->url;
              $check_in_url = 'https://'.$_SERVER['SERVER_NAME'].'/check_in/'.$obj->url;
            ?>
            <td><a href="<?php echo $event_url ?>" target="_blank">Go to Event Page</a></td>
            <td><a href="<?php echo $check_in_url ?>" target="_blank">Go to Check-in Page</a></td>
            <td><a href="<?php echo ADMIN_URL; ?>events/update/<?= $obj->id ?>"><?php echo $obj->registration_status; ?></a></td>
            <td class="text-center">
              <a class="btn-actions" href="<?= ADMIN_URL ?>events/update/<?= $obj->id ?>">
              <i class="icon-pencil"></i> 
              </a>
            </td>
            <td class="text-center">
              <a class="btn-actions" href="<?= ADMIN_URL ?>events/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
                <i class="icon-cancel-circled"></i> 
              </a>
            </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php } else { ?>
    <p>No Events to display</p>
<? } 
?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'events';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>