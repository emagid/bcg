<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#general-tab" aria-controls="general"  role="tab" data-toggle="tab">General</a></li>
        <li role="presentation"><a href="#logos-tab" aria-controls="logos"  role="tab" data-toggle="tab">Logos</a></li>
        <li role="presentation"><a href="#tickets-tab" aria-controls="tickets"  role="tab" data-toggle="tab">Tickets</a></li>
        <li role="presentation"><a href="#images-tab" aria-controls="images"  role="tab" data-toggle="tab">Images</a></li>
        <? if($model->event->id) { ?>
            <li role="presentation"><a href="#guests-tab" aria-controls="guests"  role="tab" data-toggle="tab">Guests</a></li>
            <li role="presentation"><a href="#volunteers-tab" aria-controls="volunteers"  role="tab" data-toggle="tab">Volunteers</a></li>
            <li role="presentation"><a href="#preregister-tab" aria-controls="preregister" role="tab" data-toggle="tab">Pre-Registered Guests</a></li>
        <? } ?>
    </ul>
    
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="general-tab">
            <input type="hidden" name="id" value="<?php echo $model->event->id; ?>" />
            <input name="token" type="hidden" value="<?php echo get_token();?>" />
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <h4>Event Details</h4>
                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Event Title</label>
                                    <?php echo $model->form->editorFor("title"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Event URL</label>
                                    <?php echo $model->form->editorFor("url"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Event description</label>
                                    <?php echo $model->form->textAreaFor("description", ["class"=>"ckeditor"]); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Organizer</label>
                                    <select name="client_id" class="form-control">
                                        <option value="">None</option>
                                        <?php foreach ($model->organizers as $organizer) {
                                            $select = $model->event->client_id == ($organizer->id) ? " selected='selected'" : "";
                                            ?>
                                            <option value="<?php echo $organizer->id; ?>" <?php echo $select; ?> ><?php echo $organizer->name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Registration Status</label>
                                    <select name="registration_status" class="form-control">
                                        <?php foreach (\Model\Event::$reg_status as $status) {
                                            $select = $model->event->registration_status == $status?'selected':''
                                            ?>
                                            <option value="<?php echo $status; ?>" <?php echo $select; ?> ><?php echo $status; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="box">
                        <h4>Event Location and Time Details</h4>
                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Venue</label>
                                    <?php echo $model->form->editorFor("venue"); ?>
                                </div>
                            </div>
                            <div class="col-md-16">
                                <div class="form-group">
                                    <label>Address</label>
                                    <?php echo $model->form->editorFor("address"); ?>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>City</label>
                                    <?php echo $model->form->editorFor("city"); ?>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>State</label>
                                    <?php echo $model->form->editorFor("state"); ?>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Country</label>
                                    <?php echo $model->form->editorFor("country"); ?>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Zip</label>
                                    <?php echo $model->form->editorFor("zip"); ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Start Date & Time</label> 
                                    <input id="start_date" type="text" value="<?php echo $model->event->start_date; ?>" />
                                    <input type="hidden" name="start_date" value="<?php echo $model->event->start_date; ?>" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>End Date & Time</label> 
                                    <input id="end_date" type="text" value="<?php echo $model->event->end_date; ?>" />
                                    <input type="hidden" name="end_date" value="<?php echo $model->event->event_ends; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <h4>General</h4>
                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Theme Color</label>
                                    <?php echo $model->form->textBoxFor("theme_color",['class'=>'jscolor']); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="logos-tab">
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Event Logos & Images</h4>
                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Event background Image</label>
                                    <p><small>(ideal profile photo size is 850 x 850)</small></p>
                                    <p><input type="file" name="background_image" class='image' /></p>
                                    <div style="display:inline-block">
                                        <?php 
                                            $img_path = "";
                                            if($model->event->background_image != "" && file_exists(UPLOAD_PATH.'events'.DS.$model->event->background_image)){ 
                                                $img_path = UPLOAD_URL . 'events/' . $model->event->background_image;
                                        ?>
                                                <div class="well well-sm pull-left" style="max-width:106.25px;max-height:106.25px;">
                                                    <img src="<?php echo $img_path; ?>" />
                                                    <br />
                                                    <a href="<?= ADMIN_URL.'events/delete_image/'.$model->event->id;?>?background_image=1" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>
                                                    <input type="hidden" name="background_image" value="<?=$model->event->background_image?>" />
                                                </div>
                                        <?php } ?>
                                        <div class='preview-container' style="max-width:106.25px;max-height:106.25px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div> 

                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Event Logo</label>
                                    <p><small>(ideal profile photo size is 850 x 850)</small></p>
                                    <p><input type="file" name="logo" class='image' /></p>
                                    <div style="display:inline-block">
                                        <?php 
                                            $img_path = "";
                                            if($model->event->logo != "" && file_exists(UPLOAD_PATH.'events'.DS.$model->event->logo)){ 
                                                $img_path = UPLOAD_URL . 'events/' . $model->event->logo;
                                        ?>
                                                <div class="well well-sm pull-left" style="max-width:106.25px;max-height:106.25px;">
                                                    <img src="<?php echo $img_path; ?>" />
                                                    <br />
                                                    <a href="<?= ADMIN_URL.'events/delete_image/'.$model->event->id;?>?logo=1" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>
                                                    <input type="hidden" name="logo" value="<?=$model->event->logo?>" />
                                                </div>
                                        <?php } ?>
                                        <div class='preview-container' style="max-width:106.25px;max-height:106.25px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>  

                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Instagram URL</label>
                                    <?php echo $model->form->editorFor("insta_url"); ?>
                                </div>
                            </div>
                        </div>                      
                    </div>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="tickets-tab">
            <input type="hidden" name="id" value="<?php echo $model->event->id; ?>" />
            <input name="token" type="hidden" value="<?php echo get_token();?>" />
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Ticket Details</h4>
                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Select a Ticket type</label>
                                    <select>
                                        <option>Free</option>
                                        <option>Paid</option>
                                        <option>Donations</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Ticket Name</label>
                                    <?php echo $model->form->editorFor("name"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Price</label>
                                    <?php echo $model->form->editorFor("price"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Quantity</label>
                                    <?php echo $model->form->editorFor("quantity"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Ticket Sales Start Date</label>
                                    <?php echo $model->form->editorFor("ticket_start"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Total Capacity</label>
                                    <?php echo $model->form->editorFor("capacity"); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="images-tab">
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Event Images</h4>
                        <div class="row">
                            <div class="col-md-24">
                                <div class="form-group">
                                    <label>Featured Image 1 (Top left) </label>
                                    <p><small>(ideal profile photo size is 850 x 850)</small></p>
                                    <p><input type="file" name="featured_image1" class='image' /></p>
                                    <div style="display:inline-block">
                                        <?php 
                                            $img_path = "";
                                            if($model->event->featured_image1 != "" && file_exists(UPLOAD_PATH.'events'.DS.$model->event->featured_image1)){ 
                                                $img_path = UPLOAD_URL . 'events/' . $model->event->featured_image1;
                                        ?>
                                                <div class="well well-sm pull-left" style="max-width:106.25px;max-height:106.25px;">
                                                    <img src="<?php echo $img_path; ?>" />
                                                    <br />
                                                    <a href="<?= ADMIN_URL.'events/delete_image/'.$model->event->id;?>?featured_image1=1" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>
                                                    <input type="hidden" name="featured_image1" value="<?=$model->event->featured_image1?>" />
                                                </div>
                                        <?php } ?>
                                        <div class='preview-container' style="max-width:106.25px;max-height:106.25px;"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Featured Image 2 (Top Right) </label>
                                    <p><small>(ideal profile photo size is 850 x 850)</small></p>
                                    <p><input type="file" name="featured_image2" class='image' /></p>
                                    <div style="display:inline-block">
                                        <?php 
                                            $img_path = "";
                                            if($model->event->featured_image2 != "" && file_exists(UPLOAD_PATH.'events'.DS.$model->event->featured_image2)){ 
                                                $img_path = UPLOAD_URL . 'events/' . $model->event->featured_image2;
                                        ?>
                                                <div class="well well-sm pull-left" style="max-width:106.25px;max-height:106.25px;">
                                                    <img src="<?php echo $img_path; ?>" />
                                                    <br />
                                                    <a href="<?= ADMIN_URL.'events/delete_image/'.$model->event->id;?>?featured_image2=1" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>
                                                    <input type="hidden" name="featured_image2" value="<?=$model->event->featured_image2?>" />
                                                </div>
                                        <?php } ?>
                                        <div class='preview-container' style="max-width:106.25px;max-height:106.25px;"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Featured Image 3 (Bottom Left) </label>
                                    <p><small>(ideal profile photo size is 850 x 850)</small></p>
                                    <p><input type="file" name="featured_image3" class='image' /></p>
                                    <div style="display:inline-block">
                                        <?php 
                                            $img_path = "";
                                            if($model->event->featured_image3 != "" && file_exists(UPLOAD_PATH.'events'.DS.$model->event->featured_image3)){ 
                                                $img_path = UPLOAD_URL . 'events/' . $model->event->featured_image3;
                                        ?>
                                                <div class="well well-sm pull-left" style="max-width:106.25px;max-height:106.25px;">
                                                    <img src="<?php echo $img_path; ?>" />
                                                    <br />
                                                    <a href="<?= ADMIN_URL.'events/delete_image/'.$model->event->id;?>?featured_image3=1" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>
                                                    <input type="hidden" name="featured_image3" value="<?=$model->event->featured_image3?>" />
                                                </div>
                                        <?php } ?>
                                        <div class='preview-container' style="max-width:106.25px;max-height:106.25px;"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Featured Image 4 (Bottom Right) </label>
                                    <p><small>(ideal profile photo size is 850 x 850)</small></p>
                                    <p><input type="file" name="featured_image4" class='image' /></p>
                                    <div style="display:inline-block">
                                        <?php 
                                            $img_path = "";
                                            if($model->event->featured_image4 != "" && file_exists(UPLOAD_PATH.'events'.DS.$model->event->featured_image4)){ 
                                                $img_path = UPLOAD_URL . 'events/' . $model->event->featured_image4;
                                        ?>
                                                <div class="well well-sm pull-left" style="max-width:106.25px;max-height:106.25px;">
                                                    <img src="<?php echo $img_path; ?>" />
                                                    <br />
                                                    <a href="<?= ADMIN_URL.'events/delete_image/'.$model->event->id;?>?featured_image4=1" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>
                                                    <input type="hidden" name="featured_image4" value="<?=$model->event->featured_image4?>" />
                                                </div>
                                        <?php } ?>
                                        <div class='preview-container' style="max-width:106.25px;max-height:106.25px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>                    
                    </div>
                </div>
            </div>
        </div>


        <div role="tabpanel" class="tab-pane" id="guests-tab">
            <input type="hidden" name="id" value="<?php echo $model->event->id; ?>" />
            <input name="token" type="hidden" value="<?php echo get_token();?>" />
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Upload a CSV file of the Invitees</h4>
                        <p> Download the Guest Invite template <a style='color: #a1063f;' href="<?=ADMIN_URL;?>events/importInviteTemplate">here</a></p>
                        <input type="file" name="file" id="uploadFile" placeholder="Upload CSV" />
                        <input style='margin-top: 30px;' type="button" class='btn btn-default btn-upload' value="Upload" disabled />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Guests Details</h4>
                        <? if(count($model->guests) > 0){ ?>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th colspan="2">Name</th>
                                    <th>Email</th>
                                    <th>Company</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody id="guest-list">
                                    <?php foreach($model->guests as $obj){ ?>
                                        <tr class="originalProducts">
                                            <td colspan="2"><a href="<?php echo ADMIN_URL; ?>guests/update/<?php echo $obj->id; ?>"><?php echo $obj->full_name(); ?></td>
                                            <td><a href="<?php echo ADMIN_URL; ?>guests/update/<?php echo $obj->id; ?>"><?php echo $obj->email; ?></td>
                                            <td><a href="<?php echo ADMIN_URL; ?>guests/update/<?php echo $obj->id; ?>"><?php echo $obj->company; ?></td>
                                            <td class='text-center'>
                                                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>guests/update/<?php echo $obj->id; ?>">
                                                    <i class="icon-pencil"></i> 
                                                </a>
                                            </td>
                                            <td>
                                                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>guests/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
                                                    <i class="icon-cancel-circled"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <? } ?>
                                </tbody>
                            </table>
                        <? } else { ?>
                            <p>There are no guests registered for this event yet.</p>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="volunteers-tab">
            <input type="hidden" name="id" value="<?php echo $model->event->id; ?>" />
            <input name="token" type="hidden" value="<?php echo get_token();?>" />
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Upload a CSV file of the Volunteers</h4>
                        <p> Download the Volunteers template <a style='color: #a1063f;' href="<?=ADMIN_URL;?>events/importVolunteersTemplate">here</a></p>
                        <input type="file" name="file" id="uploadVolunteers" placeholder="Upload CSV" />
                        <input style='margin-top: 30px;' type="button" class='btn btn-default btn-uploadVolunteers' value="Upload" disabled />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Volunteers Details</h4>
                        <? if(count($model->volunteers) > 0){ ?>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th colspan="2">Name</th>
                                    <th>Email</th>
                                    <th>Company</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody id="guest-list">
                                    <?php foreach($model->volunteers as $obj){ ?>
                                        <tr class="originalProducts">
                                            <td colspan="2"><a href="<?php echo ADMIN_URL; ?>guests/update/<?php echo $obj->id; ?>"><?php echo $obj->full_name(); ?></td>
                                            <td><a href="<?php echo ADMIN_URL; ?>guests/update/<?php echo $obj->id; ?>"><?php echo $obj->email; ?></td>
                                            <td><a href="<?php echo ADMIN_URL; ?>guests/update/<?php echo $obj->id; ?>"><?php echo $obj->company; ?></td>
                                            <td class='text-center'>
                                                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>guests/update/<?php echo $obj->id; ?>">
                                                    <i class="icon-pencil"></i> 
                                                </a>
                                            </td>
                                            <td>
                                                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>guests/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
                                                    <i class="icon-cancel-circled"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <? } ?>
                                </tbody>
                            </table>
                        <? } else { ?>
                            <p>There are no volunteers registered for this event yet.</p>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>


        <div role="tabpanel" class="tab-pane" id="preregister-tab">
            <input type="hidden" name="id" value="<?php echo $model->event->id; ?>" />
            <input name="token" type="hidden" value="<?php echo get_token();?>" />
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Upload a CSV file of the Pre-registered Guests</h4>
                        <p> Download the Preregistered template <a style='color: #a1063f;' href="<?=ADMIN_URL;?>events/preregisterGuestTemplate">here</a></p>
                        <input type="file" name="file" id="uploadPreRegister" placeholder="Upload CSV" />
                        <input style='margin-top: 30px;' type="button" class='btn btn-default btn-uploadPreregisters' value="Upload" disabled />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Pre-registered Guest Details</h4>
                        <? if(count($model->preregistered) > 0){ ?>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th colspan="2">Name</th>
                                    <th>Email</th>
                                    <th>Company</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody id="guest-list">
                                    <?php foreach($model->preregistered as $obj){ ?>
                                        <tr class="originalProducts">
                                            <td colspan="2"><a href="<?php echo ADMIN_URL; ?>guests/update/<?php echo $obj->id; ?>"><?php echo $obj->full_name(); ?></td>
                                            <td><a href="<?php echo ADMIN_URL; ?>guests/update/<?php echo $obj->id; ?>"><?php echo $obj->email; ?></td>
                                            <td><a href="<?php echo ADMIN_URL; ?>guests/update/<?php echo $obj->id; ?>"><?php echo $obj->company; ?></td>
                                            <td class='text-center'>
                                                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>guests/update/<?php echo $obj->id; ?>">
                                                    <i class="icon-pencil"></i> 
                                                </a>
                                            </td>
                                            <td>
                                                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>guests/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
                                                    <i class="icon-cancel-circled"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <? } ?>
                                </tbody>
                            </table>
                        <? } else { ?>
                            <p>There are no pre-registered guests for this event yet.</p>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>


<div class='popup'>
    <div class='offclick'></div>
    <div class='holder'>
        <img id='loader' src="<?= FRONT_ASSETS ?>img/loader.gif">
        
        <div id='partial_success' style='display: none;'>
            <div class='date date_icon' style='background-color: #2aa1c3'><i class="fas fa-check"></i></div>
            <h2 style='color: #a1063f;'>Upload Complete!</h2>
            <p>There was an error that prevented the following guests from being uploaded! </p>
                <ul id="failed_guests">
                </ul>
             <p>Please check the format of your file and try again!
             Keep an eye on the guests tab for people who sign up.</p>
        </div>

        <div id='success' style='display: none;'>
            <div class='date date_icon' style='background-color: #2aa1c3'><i class="fas fa-check"></i></div>
            <h2 style='color: #a1063f;'>Upload Complete!</h2>
            <p>All Emails have been sent! Keep an eye on the guests tab for people who sign up.</p>
        </div>

        <div id='fail' style='display: none;'>
            <div class='date date_icon' style='background-color: #<?= $model->event->theme_color ?>'><i class="fas fa-times"></i></div>
            <h2 style='color: #a1063f;'>Upload Failed!</h2>
            <p>Please try again, make sure a file is attached and matches the correct format.</p>
        </div>
    </div>
</div>

<script src="<?=ADMIN_JS.'jscolor.min.js'?>"></script>
<?php echo footer(); ?>
<link rel = "stylesheet" type = "text/css" href = "<?=ADMIN_CSS.'jquery.datetimepicker.css'?>">
<script src="<?=ADMIN_JS.'plugins/jquery.datetimepicker.full.min.js'?>"></script>

<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'events/');?>;
    $(document).ready(function () {
        $("input[name='title']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='url']").val(val.toLowerCase());
        });
    });
</script>

<script type="text/javascript">

    $('#start_date').datetimepicker({
        inline:false,
        format: 'Y/m/d H:i',
        formatTime: 'H:i',
        formatDate: 'Y/m/d',
        step: 60,
        monthChangeSpinner: true,
        closeOnDateSelect: false,
        closeOnTimeSelect: true,
        closeOnWithoutClick: true,
        closeOnInputClick: true,
        openOnFocus: true,
        timepicker: true,
        datepicker: true,
        defaultDate: new Date(),
    })

    $('#start_date').change(function(){
       $('[name=start_date]').val($('#start_date').val());
    }).change();

    $('#end_date').datetimepicker({
        inline:false,
        format: 'Y/m/d H:i',
        formatTime: 'H:i',
        formatDate: 'Y/m/d',
        step: 60,
        monthChangeSpinner: true,
        closeOnDateSelect: false,
        closeOnTimeSelect: true,
        closeOnWithoutClick: true,
        closeOnInputClick: true,
        openOnFocus: true,
        timepicker: true,
        datepicker: true,
        defaultDate: new Date(),
    })

    $('#end_date').change(function(){
       $('[name=end_date]').val($('#end_date').val());
    }).change();

    $(function(){
        $("input[name='background_image']").change(function(){
          showBackgroundImage(this);
        });
    });

    function showBackgroundImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var img = $("<img />");
            reader.onload = function (e) {
                img.attr('src',e.target.result);
                img.attr('alt','Uploaded Image');
                img.attr("width",'100%');
                img.attr('height','100%');
            };
            $(input).parent().parent().find('.preview-container').html(img);
            $(input).parent().parent().find('input[type="hidden"]').remove();

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function(){
        $("input[name='logo']").change(function(){
          showLogo(this);
        });
    });

    function showLogo(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var img = $("<img />");
            reader.onload = function (e) {
                img.attr('src',e.target.result);
                img.attr('alt','Uploaded Image');
                img.attr("width",'100%');
                img.attr('height','100%');
            };
            $(input).parent().parent().find('.preview-container').html(img);
            $(input).parent().parent().find('input[type="hidden"]').remove();

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function(){
        $("input[name='featured_image1']").change(function(){
          showFeaturedImage1(this);
        });
    });

    function showFeaturedImage1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var img = $("<img />");
            reader.onload = function (e) {
                img.attr('src',e.target.result);
                img.attr('alt','Uploaded Image');
                img.attr("width",'100%');
                img.attr('height','100%');
            };
            $(input).parent().parent().find('.preview-container').html(img);
            $(input).parent().parent().find('input[type="hidden"]').remove();

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function(){
        $("input[name='featured_image2']").change(function(){
          showFeaturedImage2(this);
        });
    });

    function showFeaturedImage2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var img = $("<img />");
            reader.onload = function (e) {
                img.attr('src',e.target.result);
                img.attr('alt','Uploaded Image');
                img.attr("width",'100%');
                img.attr('height','100%');
            };
            $(input).parent().parent().find('.preview-container').html(img);
            $(input).parent().parent().find('input[type="hidden"]').remove();

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function(){
        $("input[name='featured_image3']").change(function(){
          showFeaturedImage3(this);
        });
    });

    function showFeaturedImage3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var img = $("<img />");
            reader.onload = function (e) {
                img.attr('src',e.target.result);
                img.attr('alt','Uploaded Image');
                img.attr("width",'100%');
                img.attr('height','100%');
            };
            $(input).parent().parent().find('.preview-container').html(img);
            $(input).parent().parent().find('input[type="hidden"]').remove();

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function(){
        $("input[name='featured_image4']").change(function(){
          showFeaturedImage4(this);
        });
    });

    function showFeaturedImage4(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var img = $("<img />");
            reader.onload = function (e) {
                img.attr('src',e.target.result);
                img.attr('alt','Uploaded Image');
                img.attr("width",'100%');
                img.attr('height','100%');
            };
            $(input).parent().parent().find('.preview-container').html(img);
            $(input).parent().parent().find('input[type="hidden"]').remove();

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#uploadFile').change(function(){
       checkValid();
    });
    
    function checkValid(){
        var valid = true;
        if(!$('#uploadFile').get(0).files.length === 0){
            valid = false;
        }
        if(valid){
            // alert(valid);
            $('.btn-upload').prop('disabled',false);
        } else {
            $('.btn-upload').prop('disabled',true);
            $('.btn-upload').addClass('disabled')
        }
    }

    $(function(){
        $('.btn-upload').click(function(){
            $('.popup').fadeIn();
            $('.popup').css('display', 'flex');
            $('.popup').children('.holder').css('display', 'flex');

            $('#loader').fadeIn();
            // debugger;
            // alert("clicked");
            var formData = new FormData();
            // debugger;
            var fileData = $('#uploadFile').prop('files')[0];
            formData.append('file',fileData);
            formData.append('event',<?=$model->event->id?>);
            $.ajax({
                url: '/admin/events/sendInvites',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                success: function(response){
                    console.log(response);
                    if(response.status){
                        $('#loader').fadeOut(500);
                        setTimeout(function(){
                            $('#success').fadeIn();
                        }, 500)
                    } else {
                        $('#loader').fadeOut(500);
                        setTimeout(function(){
                            $('#fail').fadeIn();
                        }, 500)
                    }
                }
            })
        })
    })

    $('#uploadVolunteers').change(function(){
        checkValidImport();
    })

    function checkValidImport(){
        var valid = true;
        if(!$('#uploadVolunteers').get(0).files.length === 0){
            valid = false;
        }
        if(valid){
            // alert(valid);
            $('.btn-uploadVolunteers').prop('disabled',false);
        } else {
            $('.btn-uploadVolunteers').prop('disabled',true);
            $('.btn-uploadVolunteers').addClass('disabled')
        }
    }

    $('#uploadPreRegister').change(function(){
        checkValidPreregImport();
    })

    function checkValidPreregImport(){
        var valid = true;
        if(!$('#uploadPreRegister').get(0).files.length === 0){
            valid = false;
        }
        if(valid){
            // alert(valid);
            $('.btn-uploadPreregisters').prop('disabled',false);
        } else {
            $('.btn-uploadPreregisters').prop('disabled',true);
            $('.btn-uploadPreregisters').addClass('disabled')
        }
    }

    $(function(){
        $('.btn-uploadVolunteers').click(function(){
            $('.popup').fadeIn();
            $('.popup').css('display', 'flex');
            $('.popup').children('.holder').css('display', 'flex');

            $('#loader').fadeIn();

            var formData = new FormData();
            var fileData = $('#uploadVolunteers').prop('files')[0];
            formData.append('file',fileData);
            formData.append('event',<?=$model->event->id?>);
            $.ajax({
                url: '/admin/events/registerVolunteers',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                success: function(response){
                    console.log(response);
                    if(response.status){
                        $('#loader').fadeOut(500);
                        setTimeout(function(){
                            $('#success').fadeIn();
                        }, 500)
                    } else {
                        $('#loader').fadeOut(500);
                        setTimeout(function(){
                            $('#fail').fadeIn();
                        }, 500)
                    }
                }
            })
        })

        $('.btn-uploadPreregisters').click(function(){
            $('.popup').fadeIn();
            $('.popup').css('display', 'flex');
            $('.popup').children('.holder').css('display', 'flex');

            $('#loader').fadeIn();

            var formData = new FormData();
            var fileData = $('#uploadPreRegister').prop('files')[0];
            formData.append('file',fileData);
            formData.append('event',<?=$model->event->id?>);
            $.ajax({
                url: '/admin/events/preregisterGuests',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                success: function(response){
                    console.log(response);
                    if(response.status){
                        $('#loader').fadeOut(500);
                        if(response.failures.length){
                            for(email in response.failures){
                                $('#failed_guests').append($('<td>').text(email));
                            }
                            setTimeout(function(){
                                $('#partial_success').fadeIn();
                            }, 500)
                        } else {
                            setTimeout(function(){
                                $('#success').fadeIn();
                            }, 500)
                        }
                    } else {
                        $('#loader').fadeOut(500);
                        setTimeout(function(){
                            $('#fail').fadeIn();
                        }, 500)
                    }
                }
            })
        })
    })

    // $.datetimepicker.setLocale('en');
    // $('#start_date').datetimepicker({
    //     format: "D., M d - h:i A",
    //     formatTime: "h:i A",
    //     step:15
    // });
    // $('#start_date').val(moment.unix($('[name=start_date]').val()).format('ddd., MMM DD - hh:mm A'));
    // $('#start_date').change(function(){
    //    $('[name=start_date]').val(moment($('#start_date').val(),'ddd., MMM DD - hh:mm A',true).utc().unix());
    // }).change();
    
    // $('#end_date').datetimepicker({
    //     format: "D., M d - h:i A",
    //     formatTime: "h:i A",
    //     step:15
    // });

    // $('#end_date').val(moment.unix($('[name=end_date').val()).format('ddd., MMM DD - hh:mm A'));
    // $('#end_date').change(function(){
    //    $('[name=end_date]').val(moment($('#end_date').val(),'ddd., MMM DD - hh:mm A',true).utc().unix());
    // }).change();
</script>

