

<?php
 if(count($model->contacts)>0): ?>
     <div class="search_bar">
         <div class="topnav">
             <input type="text" id='search' placeholder="Search by name, email, or phone_number..." />
             <button type="button">
                 <img src="<?=ADMIN_IMG?>search.png" />
             </button>

         </div>
     </div>
  <div class="box box-table">
    <table class="table" id="data-list">
      <thead>
        <tr>
            <th width="15%">Name</th>
            <th width="15%">Email</th>
            <th width="15%">Phone Num</th>
            <th width="20%">Last Sign in</th>
          <th width="15%" class="text-center">Edit</th>
          <th width="15%" class="text-center">Delete</th>	
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->contacts as $obj){ ?>
        <tr class="originalContacts">
            <td><a href="<?php echo ADMIN_URL; ?>contacts/update/<?= $obj->id ?>"><?php echo $obj->first_name.' '.$obj->last_name; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>contacts/update/<?= $obj->id ?>"><?php echo $obj->email; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>contacts/update/<?= $obj->id ?>"><?php echo $obj->phone; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>contacts/update/<?= $obj->id ?>"><?php echo $obj->lastSignin(); ?></a></td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>contacts/update/<?= $obj->id ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>contacts/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'contacts';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
    $("#search").keyup(function () {
        var url = "<?php echo ADMIN_URL; ?>contacts/search";
        var keywords = $(this).val();
        if (keywords.length > 0) {
            $.get(url, {keywords: keywords}, function (data) {
                $("#data-list tbody tr").not('.originalContacts').remove();
                $('.paginationContent').hide();
                $('.originalContacts').hide();

                var list = JSON.parse(data);

                for (key in list) {
                    var tr = $('<tr />');
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>contacts/update/' + list[key].id).html(list[key].name));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>contacts/update/' + list[key].id).html(list[key].email));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>contacts/update/' + list[key].id).html(list[key].phone));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>contacts/update/' + list[key].id).html(list[key].last_sign_in));
                    var editTd = $('<td />').addClass('text-center').appendTo(tr);
                    var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>contacts/update/' + list[key].id);
                    var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                    var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                    var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>contacts/delete/' + list[key].id);
                    deleteLink.click(function () {
                        return confirm('Are You Sure?');
                    });
                    var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                    tr.appendTo($("#data-list tbody"));
                }

            });
        } else {
            $("#data-list tbody tr").not('.originalContacts').remove();
            $('.paginationContent').show();
            $('.originalContacts').show();
        }
    });
</script>