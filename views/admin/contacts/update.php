<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a></li>

    </ul>
    
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="general">
        <input type="hidden" name="id" value="<?php echo $model->contact->id; ?>" />
        <input name="token" type="hidden" value="<?php echo get_token();?>" />
        <div class="row">
            <div class="col-md-24">
                <div class="box">
                    <h4>General</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>First Name</label>
                                <?php echo $model->form->editorFor("first_name"); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Last Name</label>
                                <?php echo $model->form->editorFor("last_name"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Email</label>
                                <?php echo $model->form->editorFor("email"); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Phone Number</label>
                                <?php echo $model->form->editorFor("phone"); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Birth Date</label>
                                <?php echo $model->form->editorFor("birthdate"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Visits</label>
                                <?foreach ($model->contact->get_signatures() AS $signin) {?>
                                    <div><?=$signin->toDate();?>&nbsp;&nbsp;<a class="btn sig_btn" data-sig=<?=$signin->id?> >View Signature</a></div>
                                <? } ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Signature</label>
                                <br/>
                                <canvas id="canvas" style="border: 1px solid #061c23;
    outline: none;
    border-radius: 8px;
    position: relative;"></canvas>
                            </div>
                        </div>
                    </div>
                    
                    <div class="" style="margin-top:20px;">
                        <?=$model->form->checkboxFor("has_balance",1,['id'=>"balanceCheck"]);?>
                        <label for="balanceCheck">Outstanding Balance</label>
                    </div>
                </div>
            </div>
 
        </div>
      </div>
    
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>

<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script type="text/javascript">
    var signaturePad;
    $(function () {
        var canvas = document.querySelector("canvas");
        signaturePad = new SignaturePad(canvas);
        $('.sig_btn').click(function () {
          $.post('/contacts/get_sig',{id:$(this).data('sig')},function(data){
            if(data.status){
                signaturePad.fromData(JSON.parse(data.signature));
            }
          });
       });
    });
</script>
 