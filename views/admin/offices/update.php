<div role="tabpanel">
	<ul class="nav nav-tabs" role="tablist">
	<li role="presentation" class="active"><a href="#details-info-tab" aria-controls="categories" role="tab" data-toggle="tab">Details</a></li>
<!--		<li role="presentation"><a href="#patients-info-tab" aria-controls="general" role="tab" data-toggle="tab">Patients</a></li>-->
<!--		<li role="presentation"><a href="#appointment-tab" aria-controls="seo" role="tab" data-toggle="tab">Appointments</a></li>-->
	</ul>
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="details-info-tab">
			<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
				<input type="hidden" name="id" value="<?=$model->_office->O ?>" />
				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<h4>Practice Information - Display</h4>
							<div class="form-group">
								<label>Practice Name:</label>
								<p><?=$model->_office->OFN ?></p>
								<input type="hidden" name="practice_id" value="<?=$model->office->practice_id ?>">
								<input type="text" name="practice_name" value="<?=$model->office->practice()->name?>" class="display-details" data-default="<?=$model->_office->OFN ?>">
							</div>
							<div class="form-group">
								<label>Email:</label>
								<input type="text" name="email" placeholder="email" value="<?=$model->office->email?>" class="display-details" data-default="<?=$model->_office->EM ?>">
							</div>
							<div class="form-group">
								<label>Phone Number:</label>
								<input type="text" name="phone" placeholder="phone" value="<?=$model->office->phone?>" class="display-details" data-default="<?=$model->_office->PH ?>">
							</div>
							<div class="form-group">
								<label>Address:</label>
								<input type="text" name="address" placeholder="address" value="<?=$model->office->address?>" class="display-details" data-default="<?=$model->_office->AD ?>">
								<input type="text" name="address2" placeholder="address2" value="<?=$model->office->address2?>" class="display-details" data-default="<?=$model->_office->AD2 ?>">
								<input type="text" name="city" placeholder="city" value="<?=$model->office->city?>" class="display-details" data-default="<?=$model->_office->CT?>">
								<input type="text" name="state" placeholder="state" value="<?=$model->office->state?>" class="display-details" data-default="<?=$model->_office->ST ?>">
								<input type="text" name="zip" placeholder="zip" value="<?=$model->office->zip?>" class="display-details" data-default="<?=$model->_office->ZP ?>">
							</div>
							<div>
								<label>Display As:</label>
								<input type="text" name="label" placeholder="label" value="<?=$model->office->label?>" class="display-details" data-default="<?=$model->_office->CT?>, <?=$model->_office->ST ?>">
							</div>
							<div class="form-group">
								<label>Added:</label>
								<p><?=$model->_office->CBO?></p>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-save">Save</button>
								<!--<a class="btn btn-save default-btn">Reset to default</a>-->
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
