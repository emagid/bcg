<?php
 if(count($model->ticket_types)>0){ ?>

  <div class="box box-table">
    <table class="table" id="data-list">
      <thead>
        <tr>
            <th width="15%" class="text-center">Id</th>
            <th width="15%" class="text-center">Ticket Type</th>
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->ticket_types as $obj){
          ?>
        <tr class="allCheckins">
            <td><a><?php echo $obj->id; ?></a></td>
            <td><a><?php echo $obj->type; ?></a></td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php } else { ?>
    <p>No Ticket Types to display</p>
<? } 
?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'ticket_types';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>