
<?php
 if(count($model->invites)>0){ ?>

  <div class="box box-table">
    <table class="table" id="data-list">
      <thead>
        <tr>
            <th width="15%" class="text-center">Name</th>
            <th width="10%" class="text-center">Email</th>
            <th width="10%" class="text-center">Phone</th>
            <th width="15%" class="text-center">Event</th>
            <th width="15%" class="text-center">Status</th>
            <th width="5%" class="text-center">Edit</th>
            <th width="5%" class="text-center">Delete</th>
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->invites as $obj){
        $event = \Model\Event::getItem($obj->event_id);
          ?>
        <tr class="allCheckins">
            <td><a href="<?php echo ADMIN_URL; ?>invites/update/<?= $obj->id ?>"><?php echo $obj->name; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>invites/update/<?= $obj->id ?>"><?php echo $obj->email; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>invites/update/<?= $obj->id ?>"><?php echo $obj->phone; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>invites/update/<?= $obj->id ?>"><?php echo $event->title; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>invites/update/<?= $obj->id ?>"><?php echo $obj->status?></a></td>
            <td class="text-center">
              <a class="btn-actions" href="<?= ADMIN_URL ?>invites/update/<?= $obj->id ?>">
              <i class="icon-pencil"></i> 
              </a>
            </td>
            <td class="text-center">
              <a class="btn-actions" href="<?= ADMIN_URL ?>invites/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
                <i class="icon-cancel-circled"></i> 
              </a>
            </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php } else { ?>
    <p>No Invitees to display</p>
<? } 
?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'invites';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>