<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
    <?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#general-tab" aria-controls="general"  role="tab" data-toggle="tab">General</a></li> 
    </ul>
    
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="general-tab">
            <input type="hidden" name="id" value="<?php echo $model->invite->id; ?>" />
            <input name="token" type="hidden" value="<?php echo get_token();?>" />
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Invitee Details</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Name</label>
                                    <?php echo $model->form->editorFor("name"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email</label>
                                    <?php echo $model->form->editorFor("email"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <?php echo $model->form->editorFor("phone"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Event</label>
                                    <select name="event_id" class="form-control">
                                        <option value="">None</option>
                                        <?php foreach ($model->events as $event) {
                                            $select = ($model->invite->event_id == $event->id) ? 'selected' : '';
                                            ?>
                                            <option value="<?php echo $event->id; ?>" <?php echo $select; ?> ><?php echo $event->title; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status" class="form-control">
                                        <?php foreach (\Model\Invite::$status as $stat) {
                                            $select = ($model->invite->status == $stat) ? 'selected' : '';
                                            ?>
                                            <option value="<?php echo $stat; ?>" <?php echo $select; ?> ><?php echo $stat?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
 