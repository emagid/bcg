<?php if(count($model->blogs)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
          <th width="40%">Page</th> 
          <th width="30%">URL</th> 
          <th width="15%" class="text-center">Edit</th>	
          <th width="15%" class="text-center">Delete</th>	
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->blogs as $blog): ?>
        <tr>
         <td><a href="<?php echo ADMIN_URL; ?>blogs/update/<?php echo $blog->id; ?>"><?php echo $blog->title; ?></a></td>
         <td><?php echo $blog->slug; ?></td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>blogs/update/<?php echo $blog->id; ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>blogs/delete/<?php echo $blog->id; ?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
     <?php endforeach; ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'blogs/index';?>';
  var total_pages = <?= $model->pagination->total_pages;?>;
  var page = <?= $model->pagination->current_page_index;?>;

</script>

