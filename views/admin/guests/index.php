<div class="row"> 
  <form id="filters">
      <div class="form-group">
        <label>Filter by Events</label>
          <select name="event">
            <option disabled selected>Select an Event</option>
            <? foreach(\Model\Event::getList() AS $event) { ?>
                <option value="<?=$event->id?>" <?= isset($_GET['event']) && $_GET['event'] == $event->id?'selected':'' ?>>
                    <?=$event->title?>
                </option>
            <? } ?>
          </select>
      </div>

      <div class="form-group">
        <label>Filter by Status</label>
          <select name="status">
            <option disabled selected>Select Status</option>
            <? foreach(\Model\Guest::$status AS $stat) { ?>
                <option value="<?=$stat?>" <?= isset($_GET['status']) && $_GET['status'] == $stat?'selected':'' ?>>
                    <?=$stat?>
                </option>
            <? } ?>
          </select>
      </div> 
  </form>
  <a href="<?= ADMIN_URL?>guests" class="btn btn-primary" >Reset Filters</a>
  <a href="<?= ADMIN_URL?>guests/sendReminder<?=$_SERVER['QUERY_STRING']?'?'.$_SERVER['QUERY_STRING']:''?>" class="btn btn-primary" disabled>Send Reminder</a>
</div>
<?php
 if(count($model->guests)>0){ ?>
    <div class="search_bar">
         <div class="topnav">
             <input type="text" id='search' placeholder="Search by name or email" />
             <button type="button">
                 <img src="<?=ADMIN_IMG?>search.png" />
             </button>

         </div>
     </div>

  <div class="box box-table">
    <table class="table" id="data-list">
      <thead>
        <tr>
            <th width="15%" class="text-center">First Name</th>
            <th width="15%" class="text-center">Last Name</th>
            <th width="10%" class="text-center">Email</th>
            <th width="10%" class="text-center">Company</th>
            <th width="15%" class="text-center">Event</th>
            <th width="5%" class="text-center">Type</th>
            <th width="15%" class="text-center">Status</th>
            <th width="15%" class="text-center">Check-In Time</th>
            <th width="5%" class="text-center">Edit</th>
            <th width="5%" class="text-center">Delete</th>
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->guests as $obj){
        $event = \Model\Event::getItem($obj->event_id);
          ?>
        <tr class="allCheckins">
            <td><a href="<?php echo ADMIN_URL; ?>guests/update/<?= $obj->id ?>"><?php echo $obj->first_name; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>guests/update/<?= $obj->id ?>"><?php echo $obj->last_name; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>guests/update/<?= $obj->id ?>"><?php echo $obj->email; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>guests/update/<?= $obj->id ?>"><?php echo $obj->company; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>events/update/<?= $event->id ?>"><?php echo $event->title?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>events/update/<?= $obj->id ?>"><?php echo $obj->volunteer == 1 ? 'Volunteer': ($obj->type != ''?$obj->type:'Guest');?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>guests/update/<?= $obj->id ?>"><?php echo $obj->status?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>guests/update/<?= $obj->id ?>"><?php echo $obj->check_in_time != '' ? date('M d, Y h:ia',strtotime($obj->check_in_time)) : '-'?></a></td>
            <td class="text-center">
              <a class="btn-actions" href="<?= ADMIN_URL ?>guests/update/<?= $obj->id ?>">
              <i class="icon-pencil"></i> 
              </a>
            </td>
            <td class="text-center">
              <a class="btn-actions" href="<?= ADMIN_URL ?>guests/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
                <i class="icon-cancel-circled"></i> 
              </a>
            </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php } else { ?>
    <p>No Guests to display</p>
<? } 
?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'guests';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

  $('#filters :input').change(function(e){
    $(this).parents('form').submit();
  });
</script>

<script type="text/javascript">
    $("#search").keyup(function () {
        var url = "<?php echo ADMIN_URL; ?>guests/search";
        var keywords = $(this).val();
        if (keywords.length > 2) {
            $.get(url, {keywords: keywords}, function (data) {
                $("#data-list tbody tr").not('.allCheckins').remove();
                $('.paginationContent').hide();
                $('.allCheckins').hide();

                var list = JSON.parse(data);

                for (key in list) {
                    var tr = $('<tr />');
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>guests/update/' + list[key].id).html(list[key].first_name));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>guests/update/' + list[key].id).html(list[key].last_name));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>guests/update/' + list[key].id).html(list[key].email));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>guests/update/' + list[key].id).html(list[key].company));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>guests/update/' + list[key].id).html(list[key].event));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>guests/update/' + list[key].id).html(list[key].type));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>guests/update/' + list[key].id).html(list[key].status));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>guests/update/' + list[key].id).html(list[key].check_in_time));
                    var editTd = $('<td />').addClass('text-center').appendTo(tr);
                    var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>guests/update/' + list[key].id);
                    var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                    var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                    var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>guests/delete/' + list[key].id);
                    deleteLink.click(function () {
                        return confirm('Are You Sure?');
                    });
                    var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                    tr.appendTo($("#data-list tbody"));
                }

            });
        } else {
            $("#data-list tbody tr").not('.allCheckins').remove();
            $('.paginationContent').show();
            $('.allCheckins').show();
        }
    });

    $(document).ready(function(){
        $('select.filter-status').on('change',function(){
            window.location.href = window.location.origin + window.location.pathname + '?status='+this.value;
        });
    });
</script>