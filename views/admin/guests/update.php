<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#general-tab" aria-controls="general"  role="tab" data-toggle="tab">General</a></li> 
    </ul>
    
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="general-tab">
            <input type="hidden" name="id" value="<?php echo $model->guest->id; ?>" />
            <input name="token" type="hidden" value="<?php echo get_token();?>" />
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Guest Details</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <?php echo $model->form->editorFor("first_name"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <?php echo $model->form->editorFor("last_name"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email</label>
                                    <?php echo $model->form->editorFor("email"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <?php echo $model->form->editorFor("phone"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Company</label>
                                    <?php echo $model->form->editorFor("company"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Volunteer</label>
                                    <?php echo $model->form->checkBoxFor("volunteer",1); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Event</label>
                                    <select name="event_id" class="form-control">
                                        <option value="">None</option>
                                        <?php foreach ($model->events as $event) {
                                            $select = ($model->guest->event_id == $event->id) ? 'selected' : '';
                                            ?>
                                            <option value="<?php echo $event->id; ?>" <?php echo $select; ?> ><?php echo $event->title; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status" class="form-control">
                                        <?php foreach (\Model\Guest::$status as $stat) {
                                            $select = ($model->guest->status == $stat) ? 'selected' : '';
                                            ?>
                                            <option value="<?php echo $stat; ?>" <?php echo $select; ?> ><?php echo $stat?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No of Guests</label>
                                    <?php echo $model->form->editorFor("no_of_guests"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Confirmation Number</label>
                                    <?php echo $model->form->textBoxFor("confirmation_no",['readonly'=>true]); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>QR Code</label>
                                    <img src="<?php echo UPLOAD_URL;?>qrcodes/<?=$model->guest->qrcode?>" height="70px">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Address</label>
                                    <?php echo $model->form->editorFor("address"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>City</label>
                                    <?php echo $model->form->editorFor("city"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>State</label>
                                    <?php echo $model->form->editorFor("state"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Country</label>
                                    <?php echo $model->form->editorFor("country"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Zip</label>
                                    <?php echo $model->form->editorFor("zip"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Last Check-In</label>
                                    <!-- <?php echo $model->form->textBoxFor("check_in_time",['readonly'=>true]); ?> -->
                                    <input type="text" name="check_in_time" value="<?php echo date('Y-m-d h:ia',strtotime($model->guest->check_in_time));?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
 