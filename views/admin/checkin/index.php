<div class="row">
  <div class="col-md-8">
    <div class="box">
      <div class="form-group">
        <label>Range</label>
        <div class="input-group">
            <input id="range" type="text" name="range" class="form-control" value="<?= !$model->start || !$model->end ? '': date('Y-m-d', strtotime($model->start)) . ' - ' . date('Y-m-d', strtotime($model->end)) ?>"/>
            <span class="input-group-addon remove-date" style="cursor: pointer;">
                <i class="icon-cancel-circled"></i>
            </span>
        </div>
      </div>

      <div class="form-group">
        <label>Search</label>
        <div class="input-group">
            <input id="search" type="text" name="search" class="form-control" placeholder="Search"/>
            <span class="input-group-addon">
                <i class="icon-search"></i>
            </span>
        </div>
      </div>
    </div> 

  </div>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
</div>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
  $(document).ready(function() {

    function getQueryObj(str) {
      return (str || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
    }

    $('#range').daterangepicker({
      timePicker: false,
      format: 'YYYY-MM-DD',
      timePickerIncrement: 30,
      timePicker12Hour: true,
      timePickerSeconds: false,
      showDropdowns: true,
      startDate: "<?php echo $model->start ? : date('Y-m-d')?>",
      endDate: "<?php echo $model->end ? : date('Y-m-d')?>"
    },function(start,end){
      end.hour(23);end.minute(59);end.second(59);
      var queryString = getQueryObj(location.search);
      queryString.t = start.unix()+','+end.unix();
      $.each(queryString,function(i,e){
          queryString[i] = decodeURIComponent(e);
      });
      window.location.replace('<?=ADMIN_URL.'checkin'?>'+'?'+ $.param(queryString));
    });

    $('.remove-date').on('click',function(){
      var queryString = getQueryObj(location.search);
      if(queryString.t ) {
          delete queryString.t;
      }
      window.location.replace('<?=ADMIN_URL.'checkin'?>'+'?'+ $.param(queryString));
    });

    $('body').on('change', '#range', function () {
      var pay_filter = $(this).val();
      var queryString;
      if(location.search && pay_filter != -1){
          queryString = getQueryObj(location.search);
          $.each(queryString,function(i,e){
              queryString[i] = decodeURIComponent(e);
          });
          queryString.filter = pay_filter;
      } else if(pay_filter == -1){
          queryString = getQueryObj(location.search);
          delete queryString.filter;
      } else {
          queryString = {filter:pay_filter};
      }
      window.location.replace('/admin/checkin?'+ $.param(queryString));
    });
  });
</script>

<?php
 if(count($model->signins)>0){ ?>

  <div class="box box-table">
    <table class="table" id="data-list">
      <thead>
        <tr>
            <th width="15%" class="text-center">Name</th>
            <th width="15%" class="text-center">Email</th>
            <th width="10%" class="text-center">Phone Num</th>
            <th width="10%" class="text-center">Type</th>
            <th width="20%" class="text-center">Checkin Date & time</th>
            <th width="15%" class="text-center">More Info.</th>
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->signins as $obj){
          $signin_contact = \Model\Contact::getItem($obj->contact_id);
          if($signin_contact == null) continue;
          ?>
        <tr class="allCheckins">
            <td><a><?php echo $signin_contact->first_name.' '.$signin_contact->last_name; ?></a></td>
            <td><a><?php echo $signin_contact->email; ?></a></td>
            <td><a><?php echo $signin_contact->phone; ?></a></td>
            <td><a><?php echo \Model\Signature::$type[$obj->type]; ?></a></td>
            <td><a><?php echo $obj->toDate(); ?></a></td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>contacts/update/<?= $signin_contact->id ?>">
           <i class="icon-user"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php } ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'checkin';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

<script type="text/javascript">
  $(function () {
    $("#search").keyup(function () {
        var url = "<?php echo ADMIN_URL; ?>checkin/search";
        var keywords = $(this).val();
        if (keywords.length > 1) {
            $.get(url, {keywords: keywords}, function (data) {
                $("#data-list tbody tr").not('.allCheckins').remove();
                $('.paginationContent').hide();
                $('.allCheckins').hide();
                var list = JSON.parse(data);
                $.each(list,function(i,list){
                    
                    var tr = $('<tr />');
                    
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>checkin/update/' + list.id).html(list.first_name +' '+ list.last_name));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>checkin/update/' + list.id).html(list.email));
                    
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>checkin/update/' + list.id).html(list.phone));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>checkin/update/' + list.id).html(list.insert_time));
                    tr.appendTo($("#data-list tbody"));
                });
                $('input[name=multibox]').prop('checked',false);
                $('#check-all').prop('checked',false);

            });
        } else {
            $("#data-list tbody tr").not('.allCheckins').remove();
            $('.paginationContent').show();
            $('.allCheckins').show();
        }
    });
  });
</script>