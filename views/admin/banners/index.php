<?php
 if(count($model->banners)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
           <th width="30%">Title</th>
            <th width="40%">Image</th> 
            <th width="40%">Banner Map</th>
          <th width="15%" class="text-center">Edit</th>
          <th width="15%" class="text-center">Delete</th> 
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->banners as $obj){ ?>
        <tr>
         <td>
         <a href="<?php echo ADMIN_URL; ?>banners/update/<?php echo $obj->id; ?>">
           <?php echo $obj->title;?>
         </a>
      </td>
      <td>
        <a href="<?php echo ADMIN_URL; ?>banners/update/<?php echo $obj->id; ?>">
        <img src="/content/uploads/banners/<?php echo $obj->image;?>" height="60px"/> 
        </a>
      </td>
      <td>
        <a href="<?php echo ADMIN_URL; ?>banners/update/<?php echo $obj->id; ?>">
            <?=$obj->getBannerMap()?>
        </a>
      </td>
         
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>banners/update/<?= $obj->id ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>banners/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'users';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>