<script src="<?=FRONT_JS?>instascan.min.js"></script>

<main>
	<section class="home checkin_page">
<!--        NOTIFICATION FOR OUTSTANDING BALANCE-->
<!--
        <div class="notify">
            <p>You currently have an outstanding balance. Please see an administrator in order to check-in.</p>
        </div>
-->
		<header>
			<? $client = \Model\Client::getItem($model->event->client_id); 
                $img_path = '';
                if($client != ''){
                    $img_path = UPLOAD_URL.'clients/'.$client->logo; ?>
                    <a href='/home/<?= $model->event->url ?>'><img src="<?php echo $img_path ?>"></a>
                <? } else { ?>
                    <a href='/home/<?= $model->event->url ?>'><img src="<?=UPLOAD_URL?>events/winebow_logo.png"></a>
                <? }
            ?>
		</header>

<!-- 		<a href="/"><aside id='home_click' class='home_click'>
			<img src="<?=FRONT_ASSETS?>img/home.png">
		</aside></a> -->

        <body>
        	<section class='main_hero hero' style="background-image: url('<?=UPLOAD_URL?>events/<?= $model->event->background_image ?>')">
                <div class='dark_overlay'>
                    <div class="registration_box" >
                        <div class="choice_1">
                            <div class="date date_icon" style='background-color: #<?= $model->event->theme_color ?>'>
                                <p><?php echo date('M',strtotime($model->event->start_date))?></p>
                                <p><span><?php echo date('d',strtotime($model->event->start_date))?></span></p>
                            </div>
                            <div class="inner">
                                <div class="time">
                                    <div class="padding event_date">
<!--                                        <h6>Event: <?=$model->event->title;?></h6>-->
                                        <div>
                                            <h6>Date</h6>
                                            <p><?php echo date('F jS, Y',strtotime($model->event->start_date))?></p>
                                        </div>
                                        <div>
                                            <h6>Time</h6>
                                            <p><?php echo date('h:ia',strtotime($model->event->start_date))?> - <?php echo date('h:ia',strtotime($model->event->end_date))?></p>
                                        </div>
                                    </div>
                                </div>


                                <div class="event_options">                                    
                                    <div class="time">
                                        <div class="padding checkin_options" style="text-align:center;">
                                            <h6 class='banner'>Choose an option below to get started:</h6>
                                        </div>
                                    </div>
                                    <div class="options">
                                        <a href="/home/<?= $model->event->url ?>" target="_self">
                                        <a href="<?= 'https://'.$_SERVER['SERVER_NAME'].'/register/event/'.$model->event->url; ?>" target="_self">
                                            <div class="choice_option" style='background-color: #<?= $model->event->theme_color ?>' id='sign_in_option'>
                                                <p>Register</p>
                                            </div>
                                        </a>
                                        <a>
                                        <div class="choice_option" style='background-color: #<?= $model->event->theme_color ?>' id='check_in'>
                                            <p>Check-In</p>
                                        </div>
                                        </a>
                                    </div>
<!--
                                    <div class='choices'>
                                        <a href="/home/vintners-harvest">
                                        <div class="date" style='background-color: #<?= $model->event->theme_color ?>' id='register'>
                                            <p>Register</p>
                                        </div>
                                            </a>
                                        <div class="date" style='background-color: #<?= $model->event->theme_color ?>' id='check_in'>
                                            <p>Check-In</p>
                                        </div>
                                    </div>  
-->
                                </div>
                                <div class="check_in_options" style="display:none;">
                                    <div class="time">
                                        <div class="padding checkin_options" style="text-align:center;">
                                            <p style='margin-top: 27px;' class="back_btn" id="back_1"><span style='color: #<?= $model->event->theme_color ?> !important; border-color: #<?= $model->event->theme_color ?>;' class="pink">&larr; Back</span></p>
                                            <h6 class='banner'>How would you like to check in to <?=$model->event->title;?>?</h6>
                                        </div>
                                    </div>
                                    <div class='choices'>
                                        <div class="date" style='background-color: #<?= $model->event->theme_color ?>' id='confirmation'>
                                            <p>Confirmation #</p>
                                        </div>
                                        <div class="date" style='background-color: #<?= $model->event->theme_color ?>' id='qr'>
                                            <p>QR Code</p>
                                        </div>
                                        <div class="date" style='background-color: #<?= $model->event->theme_color ?>' id='email'>
                                            <p>Email</p>
                                        </div>
                                    </div>
                                </div>
                                    <div class="form confirmation">
                                        <div class="padding">
                                            <p>Check In with Confirmation #</p>
                                            <form id="checkin_by_conf_code">
                                                <input type="hidden" name="event_id" value="<?=$model->event->id?>">
                                                <p class='error' style='color: #<?= $model->event->theme_color ?> !important'>The Confirmation code entered doesn't exist</p>
                                                <input class='jQKeyboard' type="text" name="confirmation_no"  placeholder="Confirmation #"> 
                                                <img class='input_load' src="<?= FRONT_ASSETS ?>img/loader.gif">
                                            </form> 
                                            <a href="#"><button id="next_step_1" style='background-color: #<?= $model->event->theme_color ?>'>Check In</button></a>
                                        </div>
                                    </div>   
                                    <div class="form qr">
                                        <div class="padding">
                                            <p>Place your QR code in front of the camera</p>
                                            <p class='error qr_error' style='color: #<?= $model->event->theme_color ?> !important'>You've already signed in!</p>
                                            <video id="video" width="250" height="187.5" autoplay></video>
                                        </div>
                                    </div>   
                                    <div class="form email">
                                        <div class="padding">
                                            <p>Check In with Email #</p>
                                            <form id="checkin_by_email">
                                                <input type="hidden" name="event_id" value="<?=$model->event->id?>">
                                                <p class='error' style='color: #<?= $model->event->theme_color ?> !important'>The email entered doesn't exist</p>
                                                <input class='jQKeyboard' type="text" name="confirmation_email"  placeholder="Email Address"> 
                                                <img class='input_load' src="<?= FRONT_ASSETS ?>img/loader.gif">
                                            </form> 
                                            <a href="#"><button id="next_step_1" style='background-color: #<?= $model->event->theme_color ?>'>Check In</button></a>
                                        </div>
                                    </div>                                    
                            </div>
                        </div>
                        <div class="choice_2">
                            <div class="date date_icon" style='background-color: #<?= $model->event->theme_color ?>'>
                                <img src="<?=FRONT_ASSETS?>img/tickets.png">
                            </div>
                            
                            <div class="inner">
                                <div class="time">
                                    <div class="padding">
                                        <h6>Confirm Your Information</h6>
                                        <p id="checkedin-name"></p>
                                        <p id="checkedin-email"></p>
                                        <p id="checkedin-company"></p>
                                        <p id="checkedin-total-guest"></p>
                                    </div>
                                </div>
                                <div class="form ticket_select">
                                    <div class="padding">
                                        <h6 style='margin-bottom: 50px;'>If your information above is accurate, please hit the button below to complete check in.</h6>
                                        <a href="#"><button id="next_step_2" style='background-color: #<?= $model->event->theme_color ?>'>CONFIRM</button></a>
                                        <p class="back_btn" id="back_1"><a href="/check_in/vintners-harvest" style="margin:10px auto;text-decoration:none;"><span style='color: #<?= $model->event->theme_color ?>; border-color: #<?= $model->event->theme_color ?>;' class="pink">&larr; Back</span></a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="choice_3">
                            <div class="date date_icon" style='background-color: #<?= $model->event->theme_color ?>'>
                                <img src="<?=FRONT_ASSETS?>img/tick-inside-circle.png">
                            </div>
                            
                            <div class="inner">
                                <div class="time">
                                    <div class="padding">
                                        <h6>Thank You for Checking In</h6>
                                        <p>Hit the button below to print your event badge.</p>
                                    </div>
                                </div>
                                <div class="padding">
                                    <div id="badge" class="form ticket_select" style="margin-top:50px;margin-bottom:0;">
                                        <div class="padding badge">
                                             <!-- <img src="<?=FRONT_ASSETS?>img/logo_alt.png" style="width:250px;">  -->
                                             <img src="<?=FRONT_ASSETS?>img/logo_alt2.png" style="width:250px;"> 
                                            <p id='volunteer'></p>
                                            <p id="badge-name">Sean Cooper</p>
                                            <p id="checkedin-company" style="font-size:20px;color:">Sean Cooper</p>
                                        </div>

                                        <div id="guest_badge" class="padding badge guest" style="margin-top:50px;margin-bottom:0;">
                                            <!-- <img src="<?=FRONT_ASSETS?>img/logo_alt.png" style="width:250px;">  -->
                                             <img src="<?=FRONT_ASSETS?>img/logo_alt2.png" style="width:250px;"> 
                                            <p class="guest_name">Sean Cooper</p>
                                            <p>Guest</p>
                                        </div>
                                    </div>
                                    <div id='badge_img'></div>
                                    <a href="#"><button id="print-badge" style='background-color: #<?= $model->event->theme_color ?>'>Print Badge</button></a>
                                </div>
                            </div>
                        </div>

                        <div class="choice_4"></div>
                    </div>
                </div>
            </section>
        </body>
  </section>
</main>

<script src="<?= FRONT_LIBS ?>divTOcanvas/html2canvas.js"></script>
<script src="<?= FRONT_LIBS ?>divTOcanvas/html2canvas.min.js"></script>
<script>
    // no rights clicks
    document.addEventListener('contextmenu', event => event.preventDefault());

    $(document).click(function(e) {
        var target = e.target;

        if (!$(target).is('.jQKeyboard') && !$(target).parents().is('.jQKeyboardContainer')) {
            $('.jQKeyboardContainer').slideUp();
        }
      });


    $(".location h6 span").click(function( event ){
        event.preventDefault();
        $(".map").slideToggle();
    });

    var guestInfo;
    
    $(".registration_box a button#next_step_1").click(function( event ){
        event.preventDefault();
        var formData = $("#checkin_by_conf_code, #checkin_by_email").serialize();
        $('.input_load').fadeIn();
        $.post("/register/guest_checkin",formData, function(data){
            if(data.status){

                

                guestInfo = data;
                $('.input_load').fadeOut();
                $("p#checkedin-name").text(data.gname);
                $(".guest_name").text(data.gname);
                $("p#checkedin-email").text(data.gemail);
                $("p#checkedin-company").text(data.company);
                if ( data.total_guest != null && data.total_guest != "" ) {
                    console.log(data.total_guest)
                    $("p#checkedin-total-guest").text('# of guests: ' + data.total_guest);
                }
                $("p#badge-name").text(data.gname);
                if(data.volunteer){
                    $("p#checkedin-company").remove();
                    $("p#volunteer").text("Volunteer").show();
                }else {
                    $("p#volunteer").remove();
                }
                $(".choice_1").slideUp();
                $(".choice_2").slideDown();
            } else {
                $('.input_load').hide();
                if(data.msg == 'already checked-in'){
                    $('.error').text("Already Checked-In");
                    $('.error').fadeIn();
                } else {
                    $('.error').fadeIn();
                }
            }
        })
    });

    $("input#conf_no").keypress(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            $(".registration_box a button#next_step_1").click();
        }
    });

    $("input#email_id").keypress(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            $(".registration_box a button#next_step_1").click();
        }
    });    
    
    
    $(".registration_box a button#next_step_3").click(function( event ){
        event.preventDefault();
        // alert("Clicked");
        var dataString = $("#guests_info, #guests_no").serialize();
        console.log(dataString);
        $.post("/register/guest_registration",dataString, function(data){
                $(".choice_3").slideUp();
                $(".choice_4").slideDown();
        })
    });
    $(".choices .date ").click(function( event ){
        event.preventDefault();
        $('.choice_1 .form').slideUp();
        $('.choices .date').css('opacity', '.4');
        $(this).css('opacity', '1');
        $("." + $(this).attr('id') ).slideDown();
        $('.error').fadeOut();
    });
    $(".event_options #check_in").click(function( event ){
        event.preventDefault();
        $('.check_in_options').slideToggle();
            $('.event_options').hide();
            
    });

    $(".pink").click(function( event ){
        event.preventDefault();
        $('.check_in_options').slideToggle();
        $('.event_options').slideDown();
        $('.form').slideUp();
            
    });
    


    var qr = new Instascan.Scanner({
        video: document.getElementById("video")
    });

    var event = <?=$model->event->id?>;
    var guestCount = null

    qr.addListener('scan', function(data){
        if(data != null){
            $.post("/register/scanQR",{data:data,event:event},function(response){
                console.log(response);
                guestCount = response;
                if(response.status){
                    $("p#checkedin-name").text(response.gname);
                    $(".guest_name").text(response.gname);
                    $("p#checkedin-email").text(response.gemail);
                    $("p#checkedin-company").text(response.company);
                    $("p#badge-name").text(response.gname);
                    if(response.volunteer){
                        $("p#checkedin-company").remove();
                        $("p#volunteer").text("Volunteer").show();
                    }else {
                        $("p#volunteer").remove();
                    }
                    // For showing volunteer --> response.company
                    $(".choice_1").slideUp();
                    $(".choice_2").slideDown();
                } else {
                    if(response.msg == 'already checked-in'){
                        $('.qr_error').fadeIn();
                    } else {
                        $('.qr_error').fadeIn();
                    }
                }
            })
        }
    });


    $(".registration_box a button#next_step_2").click(function( event ){
        event.preventDefault();
        $(".choice_2").slideUp();
        $(".choice_3").slideDown();

        if ( guestCount != null ) {
            var guests = parseInt(guestCount.total_guest);
        }else {
            var guests = parseInt(guestInfo.total_guest);
        }
        
        if ( guests > 0 ) {
            for (i = 1; i < guests; i++) { 
                $('#badge').append($('#guest_badge').clone());
            }
        }else {
            $('#guest_badge').remove();
        }

        var badges = $('.badge');
        badges.each(function(i){
            html2canvas(badges[i]).then(canvas => {
                document.querySelector(".choice_4").appendChild(canvas)
            });
            setTimeout(function(){
                $('.choice_4').show(500);
            }, 500)

            var timer;
            function go(e) {
                timer = setTimeout(function () {
                    var src = convertCanvasToImage($('.choice_4 canvas')[i]);
                    $('.choice_4').append(src);
               }, 1000);
            }
            go();
        });

        setTimeout(function(){
            $('button#print-badge').addClass('clear');
        }, 1500);
    });



    Instascan.Camera.getCameras().then(function(cams){
        qr.start(cams[0]);
    }).catch(function(err){
        console.log(err);
    })

    // Get access to the camera!
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
        });
    }



     function convertCanvasToImage(canvas) {
        var image = new Image();
        image.src = canvas.toDataURL("image/png");
        console.log(image)
        return image;
    }


    $("button#print-badge").click(function() { 
        $(this).css('pointer-events', 'none');
        $('.choice_4 canvas').remove();
        VoucherPrint($('.choice_4').html());

        setTimeout(function(){
            location.reload();
        }, 500)
    });


    function VoucherSourcetoPrint(source) {
        return "<html><head><title>Powered by Popshap.com</title><style>" +
        "@page {" +
        "size: 30374 Appointment Card;" +
        "margin: 0;"+
        "scale: 100;"+
        "}"+ 
        "</style><script>function step1(){\n" +
        "setTimeout('step2()', 10);}\n" +
        "function step2(){window.print();window.close()}\n" +
        "</"+
        "script></head><body onload='step1()'>\n" +
        source +
        // "<div>" + $(".badge").html + " </div></body></html>";
        "</body></html>";
    }

    function VoucherPrint(source) {
      Pagelink = "about:blank";
      var pwa = window.open(Pagelink, "_new");
      pwa.document.open();
      pwa.document.write(VoucherSourcetoPrint(source));
      pwa.document.close();
    }
    
</script>


<!--
<script type="text/javascript">
            $(function(){
                var keyboard = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                            [
                    ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                    ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, false],
                    ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                    ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                    ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                    ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                     ['z', 'z', 90, 0, true], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                    ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], 
                     ['Space', '32', 32, 12, true], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                ]
                        ]
                    ]
                }
                $('input.jQKeyboard').initKeypad({'keyboardLayout': keyboard});


                var board = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                        ]
                    ]
                }
                $('input.donate_key').initKeypad({'donateKeyboardLayout': board});
            });
  </script>
-->

<!--  <link rel="stylesheet" type="text/css" href="<?=FRONT_CSS?>jQKeyboard.css">-->
<!--

  <script>
      (function($){

    var keyboardLayout = {
        'layout': [
            // alphanumeric keyboard type
            // text displayed on keyboard button, keyboard value, keycode, column span, new row
            [
                [
                    ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                    ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, false],['-', '-', 189, 0, false],
                    ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                    ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                    ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                    ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                     ['z', 'z', 90, 0, true], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                    ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], 
                     ['Space', '32', 32, 12, true], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                ],
                [
                    ['~', '~', 192, 0, true], ['!', '!', 49, 0, false], ['@', '@', 50, 0, false], ['#', '#', 51, 0, false], ['$', '$', 52, 0, false], ['%', '%', 53, 0, false], ['^', '^', 54, 0, false], 
                    ['&', '&', 55, 0, false], ['*', '*', 56, 0, false], ['(', '(', 57, 0, false], [')', ')', 48, 0, false], ['_', '_', 189, 0, false], ['+', '+', 187, 0, false],
                    ['Q', 'Q', 81, 0, true], ['W', 'W', 87, 0, false], ['E', 'E', 69, 0, false], ['R', 'R', 82, 0, false], ['T', 'T', 84, 0, false], ['Y', 'Y', 89, 0, false], ['U', 'U', 85, 0, false], 
                    ['I', 'I', 73, 0, false], ['O', 'O', 79, 0, false], ['P', 'P', 80, 0, false], ['{', '{', 219, 0, false], ['}', '}', 221, 0, false], ['|', '|', 220, 0, false],
                    ['A', 'A', 65, 0, true], ['S', 'S', 83, 0, false], ['D', 'D', 68, 0, false], ['F', 'F', 70, 0, false], ['G', 'G', 71, 0, false], ['H', 'H', 72, 0, false], ['J', 'J', 74, 0, false], 
                    ['K', 'K', 75, 0, false], ['L', 'L', 76, 0, false], [':', ':', 186, 0, false], ['"', '"', 222, 0, false], ['Enter', '13', 13, 3, false],
                    ['Shift', '16', 16, 2, true], ['Z', 'Z', 90, 0, false], ['X', 'X', 88, 0, false], ['C', 'C', 67, 0, false], ['V', 'V', 86, 0, false], ['B', 'B', 66, 0, false], ['N', 'N', 78, 0, false], 
                    ['M', 'M', 77, 0, false], ['<', '<', 188, 0, false], ['>', '>', 190, 0, false], ['?', '?', 191, 0, false], ['Shift', '16', 16, 2, false],
                    ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]  
                ]
            ]
        ]
    };


        var donateKeyboardLayout = {
        'layout': [
            // alphanumeric keyboard type
            // text displayed on keyboard button, keyboard value, keycode, column span, new row
            [
                [
                    ['1', '1', 49, 0, true], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                    ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                ]
            ]
        ]
    };

    var activeInput = {
        'htmlElem': '',
        'initValue': '',
        'keyboardLayout': keyboardLayout,
        'donateKeyboardLayout': donateKeyboardLayout,
        'keyboardType': '0',
        'keyboardSet': 0,
        'dataType': 'string',
        'isMoney': false,
        'thousandsSep': ',',
        'disableKeyboardKey': false
    };

    /*
     * initialize keyboard
     * @param {type} settings
     */
    $.fn.initKeypad = function(settings){
        //$.extend(activeInput, settings);

        $(this).click(function(e){
            $("#jQKeyboardContainer").remove();
            activateKeypad(e.target);
        });
    };
    
    /*
     * create keyboard container and keyboard button
     * @param {DOM object} targetInput
     */
    function activateKeypad(targetInput){
        if($('div.jQKeyboardContainer').length === 0)
        {
            activeInput.htmlElem = $(targetInput);
            activeInput.initValue = $(targetInput).val();

            $(activeInput.htmlElem).addClass('focus');
            createKeypadContainer();
            createKeypad(0);
        }
    }
    
    /*
     * create keyboard container
     */
    function createKeypadContainer(){
        var container = document.createElement('div');
        container.setAttribute('class', 'jQKeyboardContainer');
        container.setAttribute('id', 'jQKeyboardContainer');
        container.setAttribute('name', 'keyboardContainer' + activeInput.keyboardType);
        
        $('body').append(container);
    }
    
    /*
     * create keyboard
     * @param {Number} set
     */
    function createKeypad(set){
        $('#jQKeyboardContainer').empty();
        if ( $('input.donateinput').is(':focus') ) {
            var layout = activeInput.donateKeyboardLayout.layout[activeInput.keyboardType][set];
        }else {
            var layout = activeInput.keyboardLayout.layout[activeInput.keyboardType][set];
        }
        

        for(var i = 0; i < layout.length; i++){

            if(layout[i][4]){
                var row = document.createElement('div');
                row.setAttribute('class', 'jQKeyboardRow');
                row.setAttribute('name', 'jQKeyboardRow');
                $('#jQKeyboardContainer').append(row);
            }

            var button = document.createElement('button');
            button.setAttribute('type', 'button');
            button.setAttribute('name', 'key' + layout[i][2]);
            button.setAttribute('id', 'key' + layout[i][2]);
            button.setAttribute('class', 'jQKeyboardBtn' + ' ui-button-colspan-' + layout[i][3]);
            button.setAttribute('data-text', layout[i][0]);
            button.setAttribute('data-value', layout[i][1]);
            button.innerHTML = layout[i][0];
            
            $(button).click(function(e){
               getKeyPressedValue(e.target); 
            });

            $(row).append(button);
        }
    }
    /*
     * remove keyboard from kepad container
     */
    function removeKeypad(){
        $('#jQKeyboardContainer').remove();
        $(activeInput.htmlElem).removeClass('focus');
    }
    
    /*
     * handle key pressed
     * @param {DOM object} clickedBtn
     */
    function getKeyPressedValue(clickedBtn){
        var caretPos = getCaretPosition(activeInput.htmlElem);
        var keyCode = parseInt($(clickedBtn).attr('name').replace('key', ''));
        
        var currentValue = $(activeInput.htmlElem).val();
        var newVal = currentValue;
        var closeKeypad = false;
        
        /*
         * TODO
        if(activeInput.isMoney && activeInput.thousandsSep !== ''){
            stripMoney(currentValue, activeInput.thousandsSep);
        }
        */
        
        switch(keyCode){
            case 8:     // backspace key
                newVal = onDeleteKeyPressed(currentValue, caretPos);
              caretPos--;
                break;
            case 13:    // enter key
                closeKeypad = onEnterKeyPressed();
                break;
            case 16:    // shift key
                onShiftKeyPressed();
                break;
            case 27:    // cancel key
                closeKeypad = true;
                newVal = onCancelKeyPressed(activeInput.initValue);
                break;
            case 32:    // space key
                newVal = onSpaceKeyPressed(currentValue, caretPos);
                caretPos++;
    break;
            case 46:    // clear key
                newVal = onClearKeyPressed();
                break;
            case 190:   // dot key
                newVal = onDotKeyPressed(currentValue, $(clickedBtn), caretPos);
                caretPos++;
                break;
            default:    // alpha or numeric key
                newVal = onAlphaNumericKeyPressed(currentValue, $(clickedBtn), caretPos);
                caretPos++;
                break;
        }
        
        // update new value and set caret position
        $(activeInput.htmlElem).val(newVal);
        setCaretPosition(activeInput.htmlElem, caretPos);

        if(closeKeypad){
            removeKeypad();
            $(activeInput.htmlElem).blur();
        }
    }
    
    /*
     * handle delete key pressed
     * @param value 
     * @param inputType
     */
    function onDeleteKeyPressed(value, caretPos){
        var result = value.split('');
        
        if(result.length > 1){
            result.splice((caretPos - 1), 1);
            return result.join('');
        }
    }
    
    /*
     * handle shift key pressed
     * update keyboard layout and shift key color according to current keyboard set
     */
    function onShiftKeyPressed(){
        var keyboardSet = activeInput.keyboardSet === 0 ? 1 : 0;
        activeInput.keyboardSet = keyboardSet;

        createKeypad(keyboardSet);
        
        if(keyboardSet === 1){
            $('button[name="key16"').addClass('shift-active');
        }else{
            $('button[name="key16"').removeClass('shift-active');
        }
    }
    
    /*
     * handle space key pressed
     * add a space to current value
     * @param {String} curVal
     * @returns {String}
     */
    function onSpaceKeyPressed(currentValue, caretPos){
        return insertValueToString(currentValue, ' ', caretPos);
    }
    
    /*
     * handle cancel key pressed
     * revert to original value and close key pad
     * @param {String} initValue
     * @returns {String}
     */
    function onCancelKeyPressed(initValue){
        return initValue;
    }
    
    /*
     * handle enter key pressed value
     * TODO: need to check min max value
     * @returns {Boolean}
     */
    function onEnterKeyPressed(){
        return true;
    }
    
    /*
     * handle clear key pressed
     * clear text field value
     * @returns {String}
     */
    function onClearKeyPressed(){
        return '';
    }
    
    /*
     * handle dot key pressed
     * @param {String} currentVal
     * @param {DOM object} keyObj
     * @returns {String}
     */
    function onDotKeyPressed(currentValue, keyElement, caretPos){
        return insertValueToString(currentValue, keyElement.attr('data-value'), caretPos);
    }
    
    /*
     * handle all alpha numeric keys pressed
     * @param {String} currentVal
     * @param {DOM object} keyObj
     * @returns {String}
     */
    function onAlphaNumericKeyPressed(currentValue, keyElement, caretPos){
        return insertValueToString(currentValue, keyElement.attr('data-value'), caretPos);
    }
    
    /*
     * insert new value to a string at specified position
     * @param {String} currentValue
     * @param {String} newValue
     * @param {Number} pos
     * @returns {String}
     */
    function insertValueToString(currentValue, newValue, pos){
        var result = currentValue.split('');
        result.splice(pos, 0, newValue);
        
        return result.join('');
    }
    
   /*
    * get caret position
    * @param {DOM object} elem
    * @return {Number}
    */
    function getCaretPosition(elem){
        var input = $(elem).get(0);

        if('selectionStart' in input){    // Standard-compliant browsers
            return input.selectionStart;
        } else if(document.selection){    // IE
            input.focus();
            
            var sel = document.selection.createRange();
            var selLen = document.selection.createRange().text.length;
            
            sel.moveStart('character', -input.value.length);
            return sel.text.length - selLen;
        }
    }
    
    /*
     * set caret position
     * @param {DOM object} elem
     * @param {Number} pos
     */
    function setCaretPosition(elem, pos){
        var input = $(elem).get(0);
        
        if(input !== null) {
            if(input.createTextRange){
                var range = elem.createTextRange();
                range.move('character', pos);
                range.select();
            }else{
                input.focus();
                input.setSelectionRange(pos, pos);
            }
        }
    }
})(jQuery);

</script>
-->
    <!-- KEYBOARD PLUGIN END -->