<section class='checkIn_page' style="background-image: url('<?=UPLOAD_URL?>events/<?= $model->event->background_image ?>')">
    <? $client = \Model\Client::getItem($model->event->client_id); 
        $img_path = '';
        $img_path = UPLOAD_URL.'clients/'.$client->logo; ?>
            <a id='reload' href='https://bcg.popshap.net/check_in/bcg-holiday-party'><img src="<?php echo $img_path ?>"></a>
    <!-- <div class='banner'>
        <p class='sm_txt'>Welcome to <?=$model->event->title?></p>
        <p class='lrg_txt'>PLEASE check in below!</p>
    </div> -->

    <div class='checkin_content'>
        <div class='box scanner'>
            <p class='scan_txt'><strong>Check-in</strong> by scanning your QR with the camera</p>
            <video id="video" width="250" height="187.5" autoplay></video>
            <p class='scan_error'>Already signed in</p>
                <device type="media" onchange="update(this.data)"></device>

                <script>
                    function update(stream) {
                        document.querySelector('video').src = stream.url;
                    }
                </script>
        </div>
        <div class='box'>
            <p class='email_txt'>Or <strong>check-in</strong> by typing your first and last name!</p>
            <form id='checkin_by_email'>
                <img id='loader' src="<?= FRONT_ASSETS ?>img/loader.png">
                <input type="hidden" name="event_id" value="<?=$model->event->id?>">
                <input id='f_name' class='jQKeyboard' required type="text" name="f_name" placeholder='First Name'>
                <input id='l_name' class='jQKeyboard' required type="text" name="l_name" placeholder='Last Name'>
                <button id='email_checkin' class='btn checkin_btn'>Check In</button>
                <p class='error'>Already signed in!</p>
            </form>
        </div>
        <button class='btn register_btn'>Didn't sign up? Register here!</button>
    </div>

    <div class='register_popup'>
        <div class='off_click'></div>
        <form id='guests_info'>
            <div id='close'>
                <i class="fas fa-times"></i>
            </div>
            <p class='register_txt'>Please Enter your email below.</p>
            <p class='register_error'>You need to enter a bcg email. @bcg.com, @bcgplatinion.com, @bcgplatinion-maya.com, @bcgdv.com, @expandresearch.com</p>
            <input type="hidden" name="event_id" value="<?=$model->event->id?>">
              <input hidden id='name' required type="text" name="first_name" placeholder="Name">
              <input hidden type="text" name="last_name" placeholder="Last Name" class="input_50_r">
            <input id='email' class='jQKeyboard' type="text" name="email" placeholder="Email Address" class="input_50_l">
                
            <input hidden type="text" name="company"  placeholder="Company" class="input_50_r">
            <button id='_register' class='btn'>Register!</button>
            <!-- <img class='input_load' src="<?= FRONT_ASSETS ?>img/loader.gif"> -->
        </form>

        <form style='display: none;' id="guests_no">
          <select name="no_of_guests">
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
            </select>
            <h4>FREE</h4>
        </form> 

        <form style='display: none;' id="contact_preference">
            <div>
              <input style='display: none;' type="checkbox" checked='true' name="sendemail" value="byEmail">
            </div>
        </form>
    </div>

    <div class='checked_in'>
        <div id='success'><p><span id='guest_name'></span>ENJOY THE EVENT!</p></div>
    </div>

    <p class='close'>X</p>


</section>

<script src="<?=FRONT_JS?>instascan.min.js"></script>

<script src="<?= FRONT_LIBS ?>divTOcanvas/html2canvas.js"></script>
<script src="<?= FRONT_LIBS ?>divTOcanvas/html2canvas.min.js"></script>
<script type="text/javascript">

    // =====================  Interactions  =====================
    $('.checkin_content .register_btn').click(function(){
        $('.register_popup').fadeIn(300);
        $('.register_popup p.register_txt').html('Please Enter your email below.')
    });

    $('.off_click, #close').click(function(){
        $('.register_popup').fadeOut(300);
        $('#name').val('');
    });

    $('.register_popup .fa-times').click(function(){
        $('#email').val('');
    });

    $('#reload').click(function(e){
        e.preventDefault();
        location.reload();
    });



    // =====================  Get access to the camera!  =====================
    // Grab elements, create settings, etc.
    var video = document.getElementById('video');

    // Get access to the camera!
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
            try {
              video.src = window.URL.createObjectURL(stream);
            } catch(error) {
              video.srcObject = stream;
            }
            video.play();
        });
    }

    // =====================  Scanning QR  =====================
   var qr = new Instascan.Scanner({
    video: $('#video')[0]
    });

    var event = <?=$model->event->id?>;
    var guestCount = null

    qr.addListener('scan', function(data){
        if(data != null){
            $.post("/register/scanQR",{data:data,event:event},function(response){
                if ( response.status ) {
                    $('#guest_name').html(response.gname);
                    $('.checked_in').fadeIn();
                    setTimeout(function(){
                        $('.white').fadeIn(500);
                        setTimeout(function(){
                            location.reload();
                        }, 500);
                    }, 2000);
                } else if(response.msg == 'already checked-in'){
                    $('.scan_error').fadeIn();
                } else {
                    $('.register_popup').fadeIn();
                    $('#l_name, #f_name').val('');
                    $('.register_popup p.register_txt').html('YOUR NAME IS NOT REGISTERED. PLEASE REGISTER BELOW.');
                    $('#loader').fadeOut(300);
                }
            })
        }
    });

    Instascan.Camera.getCameras().then(function(cams){
        qr.start(cams[0]);
    }).catch(function(err){
        console.log(err);
    });



    // =====================  Checkin with Email  =====================
    $('#email_checkin').click(function(e){
        e.preventDefault();

            var formData = $("#checkin_by_conf_code, #checkin_by_email").serialize();
                $('#loader').fadeIn(300);
                $('.error').fadeOut();
            $.post("/register/guest_checkin",formData, function(data){
                if(data.status){
                    $('#guest_name').html(data.gname);
                    $('.checked_in').fadeIn();
                    setTimeout(function(){
                        $('.white').fadeIn(500);
                        setTimeout(function(){
                            location.reload();
                        }, 500);
                    }, 3000);
                } else if ( data.msg == 'checked-in' ){
                    $('#loader').fadeOut(300);
                    $('.error').fadeIn();
                }else {
                    $('.register_popup').fadeIn();
                    $('#l_name, #f_name').val('');
                    $('.register_popup p.register_txt').html('YOUR NAME IS NOT REGISTRED. PLEASE REGISTER BELOW.');
                    $('#loader').fadeOut(300);
                }
            });

    });
       

    // =====================  Registering  =====================
    $('#_register').click(function(e){
        e.preventDefault();
        var dataString = $("#guests_info, #guests_no, #contact_preference").serialize();
        $.post("/register/guest_registration",dataString, function(data){
            var email = $('#email').val();
            var reg = /^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\.)?[a-zA-Z]+\.)?(bcg|bcgplatinion|bcgdv|bcgplatinion-maya|expandresearch|)\.com$/g;
            if( reg.test(email) ){
                $('.checked_in').fadeIn();
                setTimeout(function(){
                    $('.white').fadeIn(500);
                    setTimeout(function(){
                        location.reload();
                    }, 500);
                }, 2000);
            } else {
               $('.register_error').fadeIn();
            }
        });
    });
</script>