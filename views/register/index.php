
<style type="text/css">
    div.jQKeyboardContainer {
        padding-top: 252px !important;
    }
</style>
-->

<main>
    <section class="home reg_page">
<!--        NOTIFICATION FOR OUTSTANDING BALANCE-->
<!--
        <div class="notify">
            <p>You currently have an outstanding balance. Please see an administrator in order to check-in.</p>
        </div>
-->     
        <!-- Client Logo(Dynamic) -->
        <header>
            <? $client = \Model\Client::getItem($model->event->client_id); 
                $img_path = '';
                if($client != ''){
                    $img_path = UPLOAD_URL.'clients/'.$client->logo; ?>
                    <a href='/home/<?= $model->event->url ?>'><img src="<?php echo $img_path ?>"></a>
                <? } else { ?>
<!--                    <a href='/home/<?= $model->event->url ?>'><img src="<?=UPLOAD_URL?>events/winebow_logo.png"></a>-->
                <? }
            ?>
        </header>

<!--        <a href="/"><aside id='home_click' class='home_click'>
            <img src="<?=FRONT_ASSETS?>img/home.png">
        </aside></a> -->

        <body>
            <section class='main_hero hero' style="background-image: url('<?=UPLOAD_URL?>events/<?= $model->event->background_image ?>')">
                <div class='dark_overlay'>
<!--                    <h1><span><?= $client->name ?></span><?=$model->event->title?></h1>           -->
<!--                </div>-->
                <!-- Use '$model->event->theme_color' to set dynamic button color  -->
<!--            </section>-->


            <div class='popup'>
                <div class='offclick'></div>
                <div class='holder'>
                    <img id='loader' src="<?= FRONT_ASSETS ?>img/loader.gif">

                    <div id='success' style='display: none;'>
                        <div class='date date_icon' style='background-color: #<?= $model->event->theme_color ?>'><i class="fas fa-check"></i></div>
                        <h2 style='color: #<?= $model->event->theme_color ?>;'>Print your ticket!</h2>
                        <button id='print' style='background-color: #<?= $model->event->theme_color ?>'>PRINT</button>
                    </div>

                    <div id='fail' style='display: none;'>
                        <div class='date date_icon' style='background-color: #<?= $model->event->theme_color ?>'><i class="fas fa-times"></i></div>
                        <p style='color: #a1063f;'>Please enter a valid phone number and make sure you have not already registered.</p>
                        <a style='text-decoration: none;' href="<?= 'https://'.$_SERVER['SERVER_NAME'].'/register/event/'.$model->event->url; ?>">
                        <button style='background-color: #<?= $model->event->theme_color ?>'>TRY AGAIN</button>
                        </a>
                    </div>
                </div>
            </div>


            
<!--            <section class="main_section">-->
                <div class="registration_box">
                    <div class="choice_1">
                    <div class="date date_icon" style='background-color: #<?= $model->event->theme_color ?>'>
                        <p><?php echo date('M',strtotime($model->event->start_date))?></p>
                        <p><span><?php echo date('d',strtotime($model->event->start_date))?></span></p>
                    </div>
                    
                    <div class="inner">
                        <div class="event_title">
                        <h6>Event: <?=$model->event->title;?></h6></div>
                        <div class="time event_date">
                            <div class="padding">
                                <h6>Date</h6>
                                <!-- <p>Fri, Jul 27,2018, 11:00 PM - Sat, Jul 28,2018, 4:00 AM EDT</p> -->
                                <p><?php echo date('F jS, Y',strtotime($model->event->start_date))?></p>
                            </div>
                            <div class="padding">
                                <h6>Time</h6>
                                <!-- <p>Fri, Jul 27,2018, 11:00 PM - Sat, Jul 28,2018, 4:00 AM EDT</p> -->
                                <p><?php echo date('h:ia',strtotime($model->event->start_date))?> - <?php echo date('h:ia',strtotime($model->event->end_date))?></p>
                            </div>
                        </div>
                        <div class="location">
                            <div class="padding">
                                <h6>Location <span class="pink" style='border-bottom: 1px solid #<?= $model->event->theme_color ?>; color: #<?= $model->event->theme_color ?>'>view map</span></h6>
                                <div class="map">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3021.5560938091567!2d-73.98603194873165!3d40.771787179224!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2585f7a9c42ef%3A0x5eb15f9cbf314562!2sDavid+H+Koch+Theater!5e0!3m2!1sen!2sus!4v1536242682240" width="100%" height="250" frameborder="0" style="border:0; margin-bottom: 20px;" allowfullscreen></iframe>
                                </div>
                                <!-- <p>Katra<br>
                                    217 Bowery<br>
                                    New York, NY 10002
                                </p> -->
                                <p><?=$model->event->venue?><br>
                                    <?=$model->event->address?><br>
                                    <?=$model->event->city.', '.$model->event->state.' '.$model->event->zip ?>
                                </p>
                            </div>
                        </div>
<!--
                        <div class="price">
                            <p><span class="pink">Starting at $100</span></p>
                        </div>
--> 
                        <? if( $model->event->registration_status == 'Closed' ) { ?>
                            <a href="#"><button id="next_step_1" style='pointer-events: none; background-color: #<?= $model->event->theme_color ?>'>Event Closed</button></a>
                        <? } else { ?>
                            <a href="#"><button id="next_step_1" style='background-color: #<?= $model->event->theme_color ?>'>Start Registration</button></a>  
                        <a style='color: #<?= $model->event->theme_color ?>' href="<?= 'https://'.$_SERVER['SERVER_NAME'].'/check_in/'.$model->event->url; ?>" target="_self" class="or_check_in"><p style='color: #<?= $model->event->theme_color ?>;'>Or Check-In</p></a>
                        <? } ?>
                    </div>
                    
                </div>
                <div class="choice_2">
                    <div class="date date_icon" style='background-color: #<?= $model->event->theme_color ?>'>
                        <img src="<?=FRONT_ASSETS?>img/application-form.png">
                        
                    </div>
                    
                    <div class="inner">
                        <div class="time">
                            <div class="padding">
                                <p style='color: #<?= $model->event->theme_color ?>;' class="back_btn" id="back_1"><span style='color: #<?= $model->event->theme_color ?>; border-bottom: 1px solid #<?= $model->event->theme_color ?>;' class="pink">&larr; Back</span></p>
                                <h6>Event Registration</h6>
                                <p>Please complete the following fields in order to select tickets.</p>
                            </div>
                        </div>
                        <div class="form">
                            <div class="padding">
                                <form id="guests_info">
                                    <input type="hidden" name="event_id" value="<?=$model->event->id?>">
                                  <input id='f_name' required type="text" name="first_name" placeholder="First Name" class="input_50_l ">
                                  <input id='l_name' required type="text" name="last_name" placeholder="Last Name" class="input_50_r ">
                                    <input required type="text" name="email"  placeholder="Email Address" class="input_50_l ">
                                    
                                    <input id='c_name' required type="text" name="company"  placeholder="Company" class="input_50_r ">
                                    
                                </form> 
                            </div>
                        </div>
                        <p style='color: #a1063f;' class='error' style='color: #<?= $model->event->theme_color ?>'>Please make sure all fields are filled out,<!--  <span style='text-decoration: underline;'>and phone number is valid.</span> --></p>
                        <a href="#"><button id="next_step_2" style='background-color: #<?= $model->event->theme_color ?>'>Select Tickets</button></a>
                    </div>
                    
                </div>
                <div class="choice_3">
                    <div class="date date_icon" style='background-color: #<?= $model->event->theme_color ?>'>
                        <img src="<?=FRONT_ASSETS?>img/tickets.png">
                    </div>
                    
                    <div class="inner">
                        <div class="time">
                            <div class="padding">
                                <p style='color: #<?= $model->event->theme_color ?>;' class="back_btn" id="back_2"><span style='color: #<?= $model->event->theme_color ?>; border-bottom: 1px solid #<?= $model->event->theme_color ?>;' class="pink">&larr; Back</span></p>
                                <h6>Ticket Selection</h6>
                                <p>A confirmation email will be sent to confirm your ticket selection.</p>
                            </div>
                        </div>
                        <div class="form ticket_select">
                            <div class="padding">
                                <p>Please select the amount of guests you would like to bring below.</p>
                                <form id="guests_no">
                                  <select name="no_of_guests">
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                    <h4>FREE</h4>
                                </form> 
                            </div>
                        </div>

                        <div class='checkboxes'>
                            <form id="contact_preference">
                                <!-- <div>
                                  <input type="checkbox" checked='true' name="sendtext" value="byText">Send ticket via phone<br>
                                </div> -->
                                <div>
                                  <input style='display: none;' type="checkbox" checked='true' name="sendemail" value="byEmail">
                                </div>
                            </form>
                          <!-- <input type="submit" value="Submit"> -->
                        </div>
                        <a href="#"><button id="next_step_3" style='background-color: #<?= $model->event->theme_color ?>'>Complete Registration</button></a>
                    </div>
                    
                </div>

                <div class="choice_4">
                    <div class="padding">
                        <div id="badge" class="form ticket_select" style="margin-top:50px;margin-bottom:0;">
                            <div id="main_badge" class="padding badge">
                                <!-- <img src="<?=FRONT_ASSETS?>img/logo_alt.png" style="width:250px;">  -->
                                 <img src="<?=FRONT_ASSETS?>img/logo_alt2.png" style="width:250px;"> 
                                <p id="checkedin-company">Sean Cooper</p>
                                <p class="guest_name">Sean Cooper</p>
                            </div>

                                        <div id="guest_badge" class="padding badge guest" style="margin-top:50px;margin-bottom:0;">
                                            <!-- <img src="<?=FRONT_ASSETS?>img/logo_alt.png" style="width:250px;">  -->
                                             <img src="<?=FRONT_ASSETS?>img/logo_alt2.png" style="width:250px;"> 
                                            <p class="guest_name">Sean Cooper</p>
                                            <p>Guest</p>
                                        </div>
                        </div>
                        <div id='badge_img'></div>
                        <a href="#"><button id="print-badge" style='background-color: #<?= $model->event->theme_color ?>'>Print Badge</button></a>
                    </div>
                </div>

                <div class="choice_5"></div>
                    
                <!-- <div class="choice_4">
                    <div class="date date_icon" style='background-color: #<?= $model->event->theme_color ?>'>
                        <img src="<?=FRONT_ASSETS?>img/tick-inside-circle.png">
                        
                    </div>
                    
                    <div class="inner">
                        <div class="time">
                            <div class="padding">
                                <h6>Your registration for this event has been confirmed.</h6>
                                <p>You will receive an email with your tickets shortly. </p>
                            </div>
                        </div>
                        <div class="form ticket_select">
                            <div class="padding">
                                <form>
                                    <p class='confirmed'>Confirmation #</p>
                                    <h4 id="confirmation_code" class='confirmed_code'></h4>
                                    <img style='margin-top: 30px;' id="qrcode" src="">
                                </form> 
                            </div>
                        </div>
                    </div>
                    
                </div> -->
            </div>
            <script src="<?= FRONT_LIBS ?>divTOcanvas/html2canvas.js"></script>
<script src="<?= FRONT_LIBS ?>divTOcanvas/html2canvas.min.js"></script>
                <script>
                            // $('.offclick, .close').click(function(){
                            //     $('.popup').fadeOut();
                            // });

                            $("input[type='tel']").each(function(){
                                $(this).on("change input keyup paste", function (e) {
                                  var output,
                                    $this = $(this),
                                    input = $this.val();

                                  if(e.keyCode != 8) {
                                    input = input.replace(/[^0-9]/g, '');
                                    var area = input.substr(0, 3);
                                    var pre = input.substr(3, 3);
                                    var tel = input.substr(6, 4);
                                    // var add = input.substr(10, 4);
                                    if (area.length < 3) {
                                      output = "(" + area;
                                    } else if (area.length == 3 && pre.length < 3) {
                                      output = "(" + area + ")" + " " + pre;
                                    } else if (area.length == 3 && pre.length == 3) {
                                      output = "(" + area + ")" + " " + pre + "-" + tel;
                                    }
                                    $this.val(output);
                                  }
                                });
                              });


                            $(document).on("click touch", '.jQKeyboardBtn', function (e) {
                                var output,
                                    $this = $('#phone'),
                                    input = $this.val();

                                  if(e.keyCode != 8) {
                                    input = input.replace(/[^0-9]/g, '');
                                    var area = input.substr(0, 3);
                                    var pre = input.substr(3, 3);
                                    var tel = input.substr(6, 4);
                                    // var add = input.substr(10, 4);
                                    if (area.length < 3) {
                                      output = "(" + area;
                                    } else if (area.length == 3 && pre.length < 3) {
                                      output = "(" + area + ")" + " " + pre;
                                    } else if (area.length == 3 && pre.length == 3) {
                                      output = "(" + area + ")" + " " + pre + "-" + tel;
                                    }
                                    $this.val(output);
                                  }
                            });


                            $(".location h6 span").click(function( event ){
                                event.preventDefault();
                                $(".map").slideToggle();
                            });
                            
                            $(".registration_box a button#next_step_1").click(function( event ){
                                event.preventDefault();
                                $(".choice_1").slideUp();
                                $(".choice_2").slideDown();
                            });
                            $("p.back_btn#back_1").click(function( event ){
                                event.preventDefault();
                                $(".choice_1").slideDown();
                                $(".choice_2").slideUp();
                            });
                                                $("p.back_btn#back_2").click(function( event ){
                                event.preventDefault();
                                    $(".choice_2").slideDown();
                                    $(".choice_3").slideUp();
                            });
                            
                            $(".registration_box a button#next_step_2").click(function( event ){
                                $("p#checkedin-name").text($('#f_name')[0].value);
                                $(".guest_name").text($('#f_name')[0].value);
                                $("p#checkedin-company").text($('#c_name')[0].value);
                                event.preventDefault();
                                if ( $('#guests_info')[0].checkValidity()) {
                                    $(".choice_2").slideUp();
                                    $(".choice_3").slideDown();
                                    $('.error').slideUp();
                                } else {
                                    $('.error').slideDown();
                                }
                            });
                            
                            $(".registration_box a button#next_step_3").click(function( event ){
                                event.preventDefault();

                                $('.popup').fadeIn();
                                $('.popup').css('display', 'flex');
                                $('.popup').children('.holder').css('display', 'flex');
                                $('#loader').fadeIn();



                                // alert("Clicked");
                                var dataString = $("#guests_info, #guests_no, #contact_preference").serialize();
                                // console.log(dataString);
                                $.post("/register/guest_registration",dataString, function(data){
                                    // console.log(data);
                                    if(data.status){
                                    // debugger
                                        // $(".choice_3").slideUp();
                                        $(".choice_4").slideDown();
                                        $("p#checkedin-name").text($('#f_name')[0].value + " " + $('#l_name')[0].value);
                                        $(".guest_name").text($('#f_name')[0].value + " " + $('#l_name')[0].value);
                                        $("p#checkedin-company").text($('#c_name')[0].value);


                                        if ( parseInt(data.guest) <= 1 ) {
                                            var guests = parseInt(data.guest);
                                        }else {
                                            var guests = parseInt(data.guest);
                                        }

                                        if ( parseInt(data.guest) > 0 ) {
                                            for (i = 1; i < guests; i++) { 
                                                $('#badge').append($('#guest_badge').clone());
                                            }
                                        }else {
                                            $('#guest_badge').remove();
                                        }

                                        var badges = $('.badge');

                                        // console.log(badge.html());
                                        badges.each(function(i){
                                            html2canvas(badges[i]).then(canvas => {
                                                document.querySelector(".choice_5").appendChild(canvas)
                                            });
                                        $('.choice_5').show(100);

                                            var timer;
                                            function go(e) {
                                                console.log(i);
                                                timer = setTimeout(function () {
                                                    var src = convertCanvasToImage($('.choice_5 canvas')[i]);
                                                    $('.choice_5').append(src);
                                                    $('#loader').fadeOut(300);
                                                    setTimeout(function(){
                                                        $('#success').fadeIn();
                                                    }, 300);
                                               }, 2000);
                                            }
                                            go();
                                        });

                                        setTimeout(function(){
                                            $('button#print-badge').addClass('clear');
                                        }, 1500);
                                            setTimeout(function(){
                                                // location.reload();
                                            }, 4000)
                                    }else {

                                        $('#loader').fadeOut(500);
                                        setTimeout(function(){
                                            $('#fail p').text("Please make sure you're not already registered");
                                            $('#fail').fadeIn();
                                        }, 500)
                                        // if(data.msg == 'invalid phone number'){
                                        //     console.log(data.msg);
                                        //     $('#loader').fadeOut(500);
                                        //     setTimeout(function(){
                                        //         $('#fail p').text("Please enter valid phone number");
                                        //         $('#fail').fadeIn();
                                        //     }, 500)
                                        // } else {
                                            // $('#loader').fadeOut(500);
                                            // setTimeout(function(){
                                            //     $('#fail').fadeIn();
                                            // }, 500)
                                        // }
                                    }
                                })
                                // $(".choice_3").hide();
                                // $(".choice_4").show();
                            });

                            $("#print").click(function() { 
                                $(this).css('pointer-events', 'none');
                                $('.choice_5 canvas').remove();
                                VoucherPrint($('.choice_5').html());

                                setTimeout(function(){
                                    // location.reload();
                                    window.location.replace("<?= 'https://'.$_SERVER['SERVER_NAME'].'/check_in/'.$model->event->url ?>");
                                }, 500)
                            });
                        </script>
<!--
                <div class="main_container">  
                    <div class="main_content">
                        <? if( $model->event->description ){ ?>
                            <h3>Event Description</h3>
                            <p><?=$model->event->description;?></p>
                            <? } ?>
                    </div>
                    
                    <?  if ( $model->event->featured_image1) { ?>
                        <div class="artboard">
                            <div class="left_board">
                                <? 
                                $img_path1 = UPLOAD_URL.'events/'.$model->event->featured_image1;
                                $img_path2 = UPLOAD_URL.'events/'.$model->event->featured_image2;
                                $img_path3 = UPLOAD_URL.'events/'.$model->event->featured_image3;
                                $img_path4 = UPLOAD_URL.'events/'.$model->event->featured_image4; ?>
                                <img src="<?php echo $img_path1 ?>">
                                <img src="<?php echo $img_path3 ?>">
                            </div>
                            <div class="right_board">
                                <img src="<?php echo $img_path2 ?>">
                                <img src="<?php echo $img_path4 ?>">
                            </div>

                        </div>
                    <? } ?>
                </div>
-->
            </section>
<!--
            <footer style='background-color: #<?= $model->event->theme_color ?>'>
                <img src="<?php echo $img_path ?>"> 
            </footer>
-->

        </body>

  </section>
</main>


<!-- images and gifs from the backend --> 
<!-- ANDREW - For some reason this overrides the keyboard function -->
<script type="text/javascript">
    if ( $(window).width() == 1920 && $(window).height() == 1080 ) {
            $('#guests_info input').each(function(i){
                if ( $(this).attr('id') == 'phone' ) {
                    $(this).addClass('number');
                }else {
//                    $(this).addClass('jQKeyboard');
                }
            });

            document.addEventListener('contextmenu', event => event.preventDefault());
        }

    $(window).resize(function(){
        if ( $(window).width() == 1920 && $(window).height() == 1080 ) {
            $('#guests_info input').each(function(i){
                if ( !$(this).attr('id') == 'phone' ) {
                    $(this).addClass('number');
                }else {
//                    $(this).addClass('jQKeyboard');
                }
            });

            document.addEventListener('contextmenu', event => event.preventDefault());
        }
    });

    $(document).click(function(e) {
        var target = e.target;

        if (!$(target).is('.jQKeyboard, .number') && !$(target).parents().is('.jQKeyboardContainer')) {
            $('.jQKeyboardContainer').slideUp();
        }
    });




    function convertCanvasToImage(canvas) {
        var image = new Image();
        image.src = canvas.toDataURL("image/png");
        console.log(image)
        return image;
    }


    $("button#print-badge").click(function() { 
        $(this).css('pointer-events', 'none');
        $('.choice_4 canvas').remove();
        VoucherPrint($('.choice_4').html());

        setTimeout(function(){
            location.reload();
        }, 500)
    });



    function VoucherSourcetoPrint(source) {
        return "<html><head><title>Powered by Popshap.com</title><style>" +
        "@page {" +
        "size: 30374 Appointment Card;" +
        "margin: 0;"+
        "scale: 66;"+
        "}"+ 
        "</style><script>function step1(){\n" +
        "setTimeout('step2()', 10);}\n" +
        "function step2(){window.print();window.close()}\n" +
        "</"+
        "script></head><body onload='step1()'>\n" +
        source +
        // "<div>" + $(".badge").html + " </div></body></html>";
        "</body></html>";
    }

    function VoucherPrint(source) {
      Pagelink = "about:blank";
      var pwa = window.open(Pagelink, "_new");
      pwa.document.open();
      pwa.document.write(VoucherSourcetoPrint(source));
      pwa.document.close();
    }
</script>

<!--

<script type="text/javascript">
            $(function(){
                var keyboard = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                            [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['-', '-', 189, 0, false], ['=', '=', 187, 0, false],
                                ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                                ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                                ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                                ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                                ['z', 'z', 90, 0, false], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                                ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], 
                                ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                            ]
                        ]
                    ]
                }
                $('input.jQKeyboard').initKeypad({'keyboardLayout': keyboard});


                var board = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                        ]
                    ]
                }
                $('input.number').initKeypad({'numberKeyboardLayout': board});
            });
  </script>

  <link rel="stylesheet" type="text/css" href="<?=FRONT_CSS?>jQKeyboard.css">

  <script>
      (function($){

    var keyboardLayout = {
        'layout': [
            // alphanumeric keyboard type
            // text displayed on keyboard button, keyboard value, keycode, column span, new row
            [
                [
                    ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                    ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, false],['-', '-', 189, 0, false],
                    ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                    ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                    ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                    ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                     ['z', 'z', 90, 0, true], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                    ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], 
                     ['Space', '32', 32, 12, true], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                ],
                [
                    ['~', '~', 192, 0, true], ['!', '!', 49, 0, false], ['@', '@', 50, 0, false], ['#', '#', 51, 0, false], ['$', '$', 52, 0, false], ['%', '%', 53, 0, false], ['^', '^', 54, 0, false], 
                    ['&', '&', 55, 0, false], ['*', '*', 56, 0, false], ['(', '(', 57, 0, false], [')', ')', 48, 0, false], ['_', '_', 189, 0, false], ['+', '+', 187, 0, false],
                    ['Q', 'Q', 81, 0, true], ['W', 'W', 87, 0, false], ['E', 'E', 69, 0, false], ['R', 'R', 82, 0, false], ['T', 'T', 84, 0, false], ['Y', 'Y', 89, 0, false], ['U', 'U', 85, 0, false], 
                    ['I', 'I', 73, 0, false], ['O', 'O', 79, 0, false], ['P', 'P', 80, 0, false], ['{', '{', 219, 0, false], ['}', '}', 221, 0, false], ['|', '|', 220, 0, false],
                    ['A', 'A', 65, 0, true], ['S', 'S', 83, 0, false], ['D', 'D', 68, 0, false], ['F', 'F', 70, 0, false], ['G', 'G', 71, 0, false], ['H', 'H', 72, 0, false], ['J', 'J', 74, 0, false], 
                    ['K', 'K', 75, 0, false], ['L', 'L', 76, 0, false], [':', ':', 186, 0, false], ['"', '"', 222, 0, false], ['Enter', '13', 13, 3, false],
                    ['Shift', '16', 16, 2, true], ['Z', 'Z', 90, 0, false], ['X', 'X', 88, 0, false], ['C', 'C', 67, 0, false], ['V', 'V', 86, 0, false], ['B', 'B', 66, 0, false], ['N', 'N', 78, 0, false], 
                    ['M', 'M', 77, 0, false], ['<', '<', 188, 0, false], ['>', '>', 190, 0, false], ['?', '?', 191, 0, false], ['Shift', '16', 16, 2, false],
                    ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]  
                ]
            ]
        ]
    };


        var numberKeyboardLayout = {
        'layout': [
            // alphanumeric keyboard type
            // text displayed on keyboard button, keyboard value, keycode, column span, new row
            [
                [
                    ['1', '1', 49, 0, true], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                    ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                ]
            ]
        ]
    };

    var activeInput = {
        'htmlElem': '',
        'initValue': '',
        'keyboardLayout': keyboardLayout,
        'numberKeyboardLayout': numberKeyboardLayout,
        'keyboardType': '0',
        'keyboardSet': 0,
        'dataType': 'string',
        'isMoney': false,
        'thousandsSep': ',',
        'disableKeyboardKey': false
    };

    /*
     * initialize keyboard
     * @param {type} settings
     */
    $.fn.initKeypad = function(settings){
        //$.extend(activeInput, settings);

        $(this).click(function(e){
            $("#jQKeyboardContainer").remove();
            activateKeypad(e.target);
        });
    };
    
    /*
     * create keyboard container and keyboard button
     * @param {DOM object} targetInput
     */
    function activateKeypad(targetInput){
        if($('div.jQKeyboardContainer').length === 0)
        {
            activeInput.htmlElem = $(targetInput);
            activeInput.initValue = $(targetInput).val();

            $(activeInput.htmlElem).addClass('focus');
            createKeypadContainer();
            createKeypad(0);
        }
    }
    
    /*
     * create keyboard container
     */
    function createKeypadContainer(){
        var container = document.createElement('div');
        container.setAttribute('class', 'jQKeyboardContainer');
        container.setAttribute('id', 'jQKeyboardContainer');
        container.setAttribute('name', 'keyboardContainer' + activeInput.keyboardType);
        
        $('body').append(container);
    }
    
    /*
     * create keyboard
     * @param {Number} set
     */
    function createKeypad(set){
        $('#jQKeyboardContainer').empty();
        if ( $('input.number').is(':focus') ) {
            var layout = activeInput.numberKeyboardLayout.layout[activeInput.keyboardType][set];
        }else {
            var layout = activeInput.keyboardLayout.layout[activeInput.keyboardType][set];
        }
        

        for(var i = 0; i < layout.length; i++){

            if(layout[i][4]){
                var row = document.createElement('div');
                row.setAttribute('class', 'jQKeyboardRow');
                row.setAttribute('name', 'jQKeyboardRow');
                $('#jQKeyboardContainer').append(row);
            }

            var button = document.createElement('button');
            button.setAttribute('type', 'button');
            button.setAttribute('name', 'key' + layout[i][2]);
            button.setAttribute('id', 'key' + layout[i][2]);
            button.setAttribute('class', 'jQKeyboardBtn' + ' ui-button-colspan-' + layout[i][3]);
            button.setAttribute('data-text', layout[i][0]);
            button.setAttribute('data-value', layout[i][1]);
            button.innerHTML = layout[i][0];
            
            $(button).click(function(e){
               getKeyPressedValue(e.target); 
            });

            $(row).append(button);
        }
    }
    /*
     * remove keyboard from kepad container
     */
    function removeKeypad(){
        $('#jQKeyboardContainer').remove();
        $(activeInput.htmlElem).removeClass('focus');
    }
    
    /*
     * handle key pressed
     * @param {DOM object} clickedBtn
     */
    function getKeyPressedValue(clickedBtn){
        var caretPos = getCaretPosition(activeInput.htmlElem);
        var keyCode = parseInt($(clickedBtn).attr('name').replace('key', ''));
        
        var currentValue = $(activeInput.htmlElem).val();
        var newVal = currentValue;
        var closeKeypad = false;
        
        /*
         * TODO
        if(activeInput.isMoney && activeInput.thousandsSep !== ''){
            stripMoney(currentValue, activeInput.thousandsSep);
        }
        */
        
        switch(keyCode){
            case 8:     // backspace key
                newVal = onDeleteKeyPressed(currentValue, caretPos);
              caretPos--;
                break;
            case 13:    // enter key
                closeKeypad = onEnterKeyPressed();
                break;
            case 16:    // shift key
                onShiftKeyPressed();
                break;
            case 27:    // cancel key
                closeKeypad = true;
                newVal = onCancelKeyPressed(activeInput.initValue);
                break;
            case 32:    // space key
                newVal = onSpaceKeyPressed(currentValue, caretPos);
                caretPos++;
    break;
            case 46:    // clear key
                newVal = onClearKeyPressed();
                break;
            case 190:   // dot key
                newVal = onDotKeyPressed(currentValue, $(clickedBtn), caretPos);
                caretPos++;
                break;
            default:    // alpha or numeric key
                newVal = onAlphaNumericKeyPressed(currentValue, $(clickedBtn), caretPos);
                caretPos++;
                break;
        }
        
        // update new value and set caret position
        $(activeInput.htmlElem).val(newVal);
        setCaretPosition(activeInput.htmlElem, caretPos);

        if(closeKeypad){
            removeKeypad();
            $(activeInput.htmlElem).blur();
        }
    }
    
    /*
     * handle delete key pressed
     * @param value 
     * @param inputType
     */
    function onDeleteKeyPressed(value, caretPos){
        var result = value.split('');
        
        if(result.length > 1){
            result.splice((caretPos - 1), 1);
            return result.join('');
        }
    }
    
    /*
     * handle shift key pressed
     * update keyboard layout and shift key color according to current keyboard set
     */
    function onShiftKeyPressed(){
        var keyboardSet = activeInput.keyboardSet === 0 ? 1 : 0;
        activeInput.keyboardSet = keyboardSet;

        createKeypad(keyboardSet);
        
        if(keyboardSet === 1){
            $('button[name="key16"').addClass('shift-active');
        }else{
            $('button[name="key16"').removeClass('shift-active');
        }
    }
    
    /*
     * handle space key pressed
     * add a space to current value
     * @param {String} curVal
     * @returns {String}
     */
    function onSpaceKeyPressed(currentValue, caretPos){
        return insertValueToString(currentValue, ' ', caretPos);
    }
    
    /*
     * handle cancel key pressed
     * revert to original value and close key pad
     * @param {String} initValue
     * @returns {String}
     */
    function onCancelKeyPressed(initValue){
        return initValue;
    }
    
    /*
     * handle enter key pressed value
     * TODO: need to check min max value
     * @returns {Boolean}
     */
    function onEnterKeyPressed(){
        return true;
    }
    
    /*
     * handle clear key pressed
     * clear text field value
     * @returns {String}
     */
    function onClearKeyPressed(){
        return '';
    }
    
    /*
     * handle dot key pressed
     * @param {String} currentVal
     * @param {DOM object} keyObj
     * @returns {String}
     */
    function onDotKeyPressed(currentValue, keyElement, caretPos){
        return insertValueToString(currentValue, keyElement.attr('data-value'), caretPos);
    }
    
    /*
     * handle all alpha numeric keys pressed
     * @param {String} currentVal
     * @param {DOM object} keyObj
     * @returns {String}
     */
    function onAlphaNumericKeyPressed(currentValue, keyElement, caretPos){
        return insertValueToString(currentValue, keyElement.attr('data-value'), caretPos);
    }
    
    /*
     * insert new value to a string at specified position
     * @param {String} currentValue
     * @param {String} newValue
     * @param {Number} pos
     * @returns {String}
     */
    function insertValueToString(currentValue, newValue, pos){
        var result = currentValue.split('');
        result.splice(pos, 0, newValue);
        
        return result.join('');
    }
    
   /*
    * get caret position
    * @param {DOM object} elem
    * @return {Number}
    */
    function getCaretPosition(elem){
        var input = $(elem).get(0);

        if('selectionStart' in input){    // Standard-compliant browsers
            return input.selectionStart;
        } else if(document.selection){    // IE
            input.focus();
            
            var sel = document.selection.createRange();
            var selLen = document.selection.createRange().text.length;
            
            sel.moveStart('character', -input.value.length);
            return sel.text.length - selLen;
        }
    }
    
    /*
     * set caret position
     * @param {DOM object} elem
     * @param {Number} pos
     */
    function setCaretPosition(elem, pos){
        var input = $(elem).get(0);
        
        if(input !== null) {
            if(input.createTextRange){
                var range = elem.createTextRange();
                range.move('character', pos);
                range.select();
            }else{
                input.focus();
                input.setSelectionRange(pos, pos);
            }
        }
    }
})(jQuery);

</script>
-->
    <!-- KEYBOARD PLUGIN END 